package com.hotan.army.supply.exceptions.propertybook;


import org.parse4j.ParseException;

/**
 * Exceptions that pertain to the Property Book.
 *
 * @author Michael Hotan, michael.hotan@gmail.com.
 */
public class PropertyBookException extends ParseException {

    public PropertyBookException(String message, Throwable cause) {
        super(message, cause);
    }

    public PropertyBookException(Throwable cause) {
        super(cause);
    }
}
