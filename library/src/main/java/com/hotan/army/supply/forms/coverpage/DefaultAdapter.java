package com.hotan.army.supply.forms.coverpage;

import com.hotan.army.supply.forms.FormatException;
import com.hotan.army.supply.util.POIUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;

/**
 * A default adapter meant for the end_item_coverpage.xls in the template directory.
 *
 * @author Michael Hotan, michael.hotan@gmail.com
 */
public class DefaultAdapter implements CoverPageAdapter {

    private static final POIUtil.IndexPair MIN_SN_INDEX = POIUtil.IndexPair.valueOf(3, 1);
    private static final POIUtil.IndexPair MAX_SN_INDEX = POIUtil.IndexPair.valueOf(6, 6);
    private static final POIUtil.IndexPair NAME_INDEX = POIUtil.IndexPair.valueOf(0, 1);
    private static final POIUtil.IndexPair QTY_INDEX = POIUtil.IndexPair.valueOf(0, 6);
    private static final POIUtil.IndexPair LIN_INDEX = POIUtil.IndexPair.valueOf(1, 1);
    private static final POIUtil.IndexPair NSN_INDEX = POIUtil.IndexPair.valueOf(2, 1);
    private static final POIUtil.IndexPair MOS_INDEX = POIUtil.IndexPair.valueOf(1, 5);
    private static final POIUtil.IndexPair LOC_INDEX = POIUtil.IndexPair.valueOf(2, 5);
    private static final POIUtil.IndexPair OVERALL_PIC_INDEX = POIUtil.IndexPair.valueOf(8, 0);
    private static final POIUtil.IndexPair SERIAL_PIC_INDEX = POIUtil.IndexPair.valueOf(8, 0);
    private static final POIUtil.IndexPair BII_PIC_INDEX = POIUtil.IndexPair.valueOf(8, 0);

    @Override
    public Cell getNameCell(Sheet sheet) {
        return POIUtil.getCell(sheet, NAME_INDEX);
    }

    @Override
    public Cell getQuantityCell(Sheet sheet) {
        return POIUtil.getCell(sheet, QTY_INDEX);
    }

    @Override
    public Cell getLINCell(Sheet sheet) {
        return POIUtil.getCell(sheet, LIN_INDEX);
    }

    @Override
    public Cell getNSNCell(Sheet sheet) {
        return POIUtil.getCell(sheet, NSN_INDEX);
    }

    @Override
    public Cell getMOSCell(Sheet sheet) {
        return POIUtil.getCell(sheet, MOS_INDEX);
    }

    @Override
    public Cell getLocationCell(Sheet sheet) {
        return POIUtil.getCell(sheet, LOC_INDEX);
    }

    @Override
    public CellRangeAddress getSNRangeAddress(Sheet sheet) {
        return new CellRangeAddress(
                MIN_SN_INDEX.row, MAX_SN_INDEX.row, MIN_SN_INDEX.col, MAX_SN_INDEX.col);
    }

    @Override
    public Cell getOverallPicCell(Sheet sheet) {
        return POIUtil.getCell(sheet, OVERALL_PIC_INDEX);
    }

    @Override
    public Cell getSNPicCell(Sheet sheet) {
        return POIUtil.getCell(sheet, SERIAL_PIC_INDEX);
    }

    @Override
    public Cell getBIIPicCell(Sheet sheet) {
        return POIUtil.getCell(sheet, BII_PIC_INDEX);
    }

    @Override
    public void validate(Workbook workbook) throws FormatException {
        // Should always work but it is good to check anyways.
        // Get the first sheet with all the entire cover page.
        Sheet sheet = workbook.getSheetAt(0);

        StringBuffer errBuf = new StringBuffer();
        CellRangeAddress snRange = getSNRangeAddress(sheet);
        if (snRange == null)
            errBuf.append("Can't locate Serial Number range.");
        Cell nameCell = getNameCell(sheet);
        if (nameCell == null)
            errBuf.append("Can't locate Name cell\n");
        Cell qtyCell = getQuantityCell(sheet);
        if (qtyCell == null)
            errBuf.append("Can't locate Quantity cell\n");
        Cell linCell = getLINCell(sheet);
        if (linCell == null)
            errBuf.append("Can't locate LIN cell\n");
        Cell nsnCell = getNSNCell(sheet);
        if (nsnCell == null)
            errBuf.append("Can't locate NSN cell\n");
        Cell mosCell = getMOSCell(sheet);
        if (mosCell == null)
            errBuf.append("Can't locate MOS cell\n");
        Cell locCell = getLocationCell(sheet);
        if (locCell == null)
            errBuf.append("Can't locate Location cell\n");

        // If there was an error stop everything.
        if (errBuf.length() > 0) {
            throw new FormatException(getClass().getSimpleName() + "() " + errBuf.toString());
        }
    }

}
