package com.hotan.army.supply.exceptions.propertybook;

/**
 * Exception that represents a property book already exists in the cloud.
 *
 * @author Michael Hotan, michael.hotan@gmail.com
 */
public class PropertyBookAlreadyExistsException extends PropertyBookException {

    private final String nameNotFound;

    public PropertyBookAlreadyExistsException(String name, String message) {
        super("Duplicate name \"" + name + "\" Encountered: " + message, new Exception("Can't have duplicate property names."));
        nameNotFound = name;
    }

    public PropertyBookAlreadyExistsException(String name) {
        this(name, "");
    }

    public String getNameNotFound() {
        return nameNotFound;
    }
}
