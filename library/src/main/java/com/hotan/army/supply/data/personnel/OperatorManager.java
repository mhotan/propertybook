package com.hotan.army.supply.data.personnel;

import com.hotan.army.supply.data.parse.DataManager;
import org.apache.commons.lang3.text.WordUtils;
import org.parse4j.ParseException;
import org.parse4j.ParseQuery;

import java.util.List;
import java.util.logging.Logger;

/**
 * Manager class that handles the creation and
 *
 * @author Michael Hotan, michael.hotan@gmail.com.
 */
public class OperatorManager extends DataManager<Operator> {

    private static final Logger LOG = Logger.getLogger(OperatorManager.class.getSimpleName());

    /*
    Representation Invariant:

    Abstract Function:
    firstName, middleName, lastName ==> is the unique key for the Parse cloud.
    TODO: Use social security numbers for specific keys.

    */

    /**
     * Cannot instantiate outside this class.
     */
    private OperatorManager() {
        super();

        // Pull all the current Operators down.
        ParseQuery<Operator> query = ParseQuery.getQuery(Operator.class);
        try {
            List<Operator> operators = query.find();
            if (operators == null) return;
            operators.stream().forEach(op -> this.objects.add(op));
        } catch (ParseException e) {
            LOG.warning(getClass().getSimpleName() + "() Unable to intially fetch all the operators");
        }
    }

    /**
     * Singleton instance.
     */
    private static OperatorManager instance;

    /**
     * @return Singleton instance.
     */
    public static OperatorManager getInstance() {
        if (instance == null)
            instance = new OperatorManager();
        return instance;
    }

    @Override
    protected Class<Operator> getDataClass() {
        return Operator.class;
    }

    /**
     * Adds an operator to this or returns the existing Operator that represents the argument name.
     * Initializes the Operators Rank and MOS to null.
     *
     * @param firstName First name of the Operator to add, non null or empty String
     * @param middleName Middle name of the Operator to add, non null or empty String
     * @param lastName Last name of the Operator to add, non null or empty String
     * @return The operator that represents the name of this operator.
     */
    public Operator add(String firstName, String middleName, String lastName) throws ParseException {
        return add(firstName, middleName, lastName, null, null);
    }

    /**
     * Adds an operator to this or returns the existing Operator that represents the argument name.
     * If the argument rank is non null then the operator will be set to that value.
     * If the argument MOS is non null then the operator will be set to that value.
     *
     * @param firstName First name of the Operator to add, non null or empty String
     * @param middleName Middle name of the Operator to add, non null or empty String
     * @param lastName Last name of the Operator to add, non null or empty String
     * @param rank Rank of the operator or null if no Rank is currently assigned.
     * @param mos MOS of the operator or null if no MOS is currently assigned to this operator.
     * @return The operator with the appropiate properties.
     */
    public Operator add(
            String firstName, String middleName, String lastName, Rank rank, MOS mos) throws ParseException {
        firstName = processName(firstName);
        middleName = processMiddleName(middleName);
        lastName = processName(lastName);

        Operator operator = get(firstName, middleName, lastName);
        // If we already have the operator then update its values.
        if (operator != null) {
            // Update the values of the Operator (if applicable).
            if (rank != null)
                operator.setRank(rank);
            if (mos != null)
                operator.setMOS(mos);
        } else { // Create a new operator and then save it in the cloud and locally.
            operator = new Operator(firstName, middleName, lastName, rank, mos);
            // Save the operator in the
            operator.save();

            // Finally store it locally.
            add(operator);
        }
        return operator;
    }

    /**
     * Returns the operator with the name that matches the arguments.
     *
     * @param firstName First name of the Operator to add, non null or empty String
     * @param middleName Middle name of the Operator to add, non null or empty String
     * @param lastName Last name of the Operator to add, non null or empty String
     * @return The operator with argument name or null if no operator is found.
     */
    public Operator get(String firstName, String middleName, String lastName) throws ParseException {
        final String first = processName(firstName);
        final String middle = processMiddleName(middleName);
        final String last = processName(lastName);

        // Check if we have an operator by that name locally.
        Operator foundOperator = this.objects.stream().filter(
                curOp -> curOp.getFirstName().equals(first) &&
                        curOp.getMiddleName().equals(middle) &&
                        curOp.getLastName().equals(last)).findAny().orElse(null);
        if (foundOperator != null)
            return foundOperator;

        // Check if the cloud has the operator but we do not have it locally.
        ParseQuery<Operator> query = ParseQuery.getQuery(Operator.class);
        query.whereEqualTo(Operator.FIRST_NAME, firstName);
        query.whereEqualTo(Operator.MIDDLE_NAME, middleName);
        query.whereEqualTo(Operator.LAST_NAME, lastName);
        List<Operator> objects = query.find();

        // If there are no objects matching this description.
        if (objects == null || objects.isEmpty()) {
            return null;
        } // There is an operator matching that description.
        else {
            // currently the names are a primary key
//            objects.get(0).fetchIfNeeded();
            Operator operator = objects.get(0);
            // Update our local list.
            add(operator);
            return operator;
        }
    }

    /**
     * Process the Middle name of this string.
     *
     * @param name Middle Name of
     * @return Processed Middle Name
     */
    private static String processMiddleName(String name) {
        if (name == null)
            name = "";
        return WordUtils.capitalizeFully(name.trim());
    }

    /**
     * Processes the string.
     *
     * @param name Name to process.
     * @return The Processed string of the argument.
     */
    private static String processName(String name) {
        if (name == null || name.trim().isEmpty()) {
            throw new IllegalArgumentException("Illegal name: " + name);
        }
        return WordUtils.capitalizeFully(name.trim());
    }
}
