package com.hotan.army.supply.forms;

import com.hotan.army.supply.data.personnel.Operator;
import com.hotan.army.supply.data.personnel.OperatorManager;
import com.hotan.army.supply.data.personnel.Rank;
import org.apache.poi.ss.usermodel.Cell;
import org.parse4j.ParseException;

/**
 * Helper class for interpretting supply forms
 *
 * Created by mhotan_dev on 1/10/14.
 */
public final class FormUtil {

    private FormUtil() {
        // Cannot Insantiate
    }

    /**
     * Extracts the value from the cell without the prefix.
     *
     * @param cell Cell that contains the value.
     * @param prefix String that precedes the value.
     * @return The value excluding the prefix.
     */
    public static String getValue(Cell cell, String prefix) {
        if (cell == null)
            throw new IllegalArgumentException("getValue(), Cell cannot be null");

        String val = cell.getStringCellValue().trim();
        if (prefix == null || prefix.isEmpty())
            return val.trim();
        return val.replace(prefix, "").trim();
    }

    /**
     * Extracts the Operator from a cell.
     * <b>The cell must be in the form of 'prefix''last name', 'first name'/'rank'</b>
     *
     * @param cell Cell that contains the value.
     * @param prefix Prefix String that precedes the operator.
     * @return The Operator at the specified cell.
     */
    public static Operator getOperator(Cell cell, String prefix) throws ParseException {
        if (cell == null)
            throw new IllegalArgumentException("getValue(), Cell cannot be null");

        String val = cell.getStringCellValue().trim();
        String[] details = val.replace(prefix, "").split("/");
        String rank = details[1];
        String[] fullName = details[0].split(", ");
        String firstName = fullName[1];
        String lastName = fullName[0];
        return OperatorManager.getInstance().add(firstName, "", lastName, Rank.valueOf(rank), null);
    }

    /**
     * Pulls value of String. Values are deliminated with '/'
     * <b>Therefore a cell with a/b/c with have valid indexes of 0,1, or 2</b>
     *
     * @param cell Cell that contains the value.
     * @param prefix Prefix to exclude from content. Can be null
     * @param index Index to extract after the split.
     * @return The String value at the index of
     */
    public static String getValueWithSplit(Cell cell, String prefix, int index) {
        if (cell == null)
            throw new IllegalArgumentException("getValueWithSplit(), Cell cannot be null");
        if (index < 0)
            throw new IllegalArgumentException("getValueWithSplit(), Index cannot be negative!");

        String val = cell.getStringCellValue().trim();

        // Check if there is a prefix to remove.
        if (prefix != null)
            val = val.replace(prefix, "");

        // Split up the values.
        String[] values = val.split("/");
        // Check if the index is within bounds
        if (index > values.length - 1)
            throw new IllegalArgumentException("getValueWithSplit(), Invalid index: " + index + "" +
                    " for cell value: " + val + ".  Maximum index = " + (values.length - 1));

        val = values[index];
        return val;
    }

}
