package com.hotan.army.supply.forms;

/**
 * Exception for misformated documents or files.
 *
 * @author Michael Hotan, michael.hotan@gmail.com
 */
public class FormatException extends Exception {

    /**
     * Creates and exception with a message.
     *
     * @param msg Message for Format Exception.
     */
    public FormatException(String msg) {
        super(msg);
    }

    /**
     * Serialization ID
     */
    private static final long serialVersionUID = 3663534401660293695L;

}
