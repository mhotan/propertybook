package com.hotan.army.supply.data.location;

import com.hotan.army.supply.data.parse.FXParseObject;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.parse4j.ParseClassName;
import org.parse4j.ParseObject;

import java.util.logging.Logger;

/**
 * Created by mhotan on 3/23/14.
 */
@ParseClassName("LocatedAt")
public class LocatedAt extends FXParseObject {

    private static final Logger LOG = Logger.getLogger(LocatedAt.class.getName());

    private static final String LOCATION = "location";
    private static final String DESCRIPTION = "description";

    private final ObjectProperty<Location> location;

    private final StringProperty description;

    /**
     * Parse Constructor/
     */
    public LocatedAt() {
        location = new SimpleObjectProperty<>();
        description = new SimpleStringProperty();

        description.addListener((ov, oldVal, newVal) -> {
            assert newVal != null: "Description cannot be null";
            put(DESCRIPTION, newVal);
        });
    }

    /**
     * Located at relation.
     * @param location Location for this relation.
     */
    public LocatedAt(Location location) {
        this(location, "");
    }

    /**
     * Creates a located at relation.
     * @param location Location for this relation.
     * @param description Description fo the location.
     */
    public LocatedAt(Location location, String description) {
        this();
        if (location == null)
            throw new NullPointerException(getClass().getSimpleName() + "() Cannot have null location");
        description = description != null ? description.trim() : "";
        this.location.set(location);
        this.description.set(description);
    }

    public void setLocation(Location location) {
        if (location == null)
            throw new NullPointerException(getClass().getSimpleName() + "setLocation() " +
                    "location cannot be null");
        this.location.set(location);
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //// Setters
    //////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Set the description
     * @param description Desciption for this located at instance.
     */
    public void setDescription(String description) {
        if (description == null) description = "";
        description = description.trim();
        this.description.set(description);
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //// Getters
    //////////////////////////////////////////////////////////////////////////////////////////

    public Location getLocation() {
        return locationProperty().get();
    }

    public String getDescription() {
        return descriptionProperty().get();
    }


    //////////////////////////////////////////////////////////////////////////////////////////
    //// Properties
    //////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @return location property.
     */
    public ObjectProperty<Location> locationProperty() {
        if (location.isNull().get()) {
            ParseObject po = getParseObject(LOCATION);
            if (po != null && po.getObjectId() != null) {
                try {
                    location.set(LocationManager.getInstance().get(po.getObjectId()));
                } catch (org.parse4j.ParseException e) {
                    LOG.warning("Unable to to load location " + e.getMessage());
                }
            }
        }
        return location;
    }

    /**
     * @return Description Property.
     */
    public StringProperty descriptionProperty() {
        if (description.isNull().get()) {
            description.set(getString(DESCRIPTION));
        }
        return description;
    }
}
