package com.hotan.army.supply.forms.coverpage;

import com.hotan.army.supply.forms.FormAdapter;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;

/**
 * Interface that defines how to assign the cells for the inputed sheet.
 * This class allows variation in use different kind of cover pages XLS templates.
 * 
 * @author Michael Hotan, michael.hotan@gmail.com
 */
public interface CoverPageAdapter extends FormAdapter {

    /**
     * Attempts to get the Cell that represents the Name
     * <br>Cell cannot be null
     * 
     * @param sheet Sheet to get Cell from.
     * @return Cell to place the value
     */
    public Cell getNameCell(Sheet sheet);
    
    /**
     * Attempts to get the Cell that represents the Quantity
     * <br>Cell cannot be null
     * 
     * @param sheet Sheet to get Cell from.
     * @return Cell to place the value
     */
    public Cell getQuantityCell(Sheet sheet);
    
    /**
     * Attempts to get the Cell that represents the LIN number
     * <br>Cell cannot be null
     * 
     * @param sheet Sheet to get Cell from.
     * @return Cell to place the value
     */
    public Cell getLINCell(Sheet sheet);
    
    /**
     * Attempts to get the Cell that represents the LIN number
     * <br>Cell cannot be null
     * 
     * @param sheet Sheet to get Cell from.
     * @return Cell to place the value
     */
    public Cell getNSNCell(Sheet sheet);
    
    /**
     * Attempts to get the Cell that represents the assigned MOS
     * <br>Cell cannot be null
     * 
     * @param sheet Sheet to get Cell from.
     * @return Cell to place the value
     */
    public Cell getMOSCell(Sheet sheet);
    
    /**
     * Attempts to get the Cell that represents the Location of this item
     * <br>Cell cannot be null
     * 
     * @param sheet Sheet to get Cell from.
     * @return Cell to place the value
     */
    public Cell getLocationCell(Sheet sheet);
    
    /**
     * Attempts to get the Cell range of all the serial numbers boxes
     * <br>Range cannot be null
     * 
     * @param sheet Sheet to get Cell from.
     * @return Cell to place the value
     */
    public CellRangeAddress getSNRangeAddress(Sheet sheet);
    
    /**
     * Attempts to get the Cell that represents the Overall picture of this item
     * 
     * @param sheet Sheet to get Cell from.
     * @return Cell to place the value
     */
    public Cell getOverallPicCell(Sheet sheet);

    /**
     * Attempts to get the Cell that represents the Serial Number picture of this item
     * 
     * @param sheet Sheet to get Cell from.
     * @return Cell to place the value
     */
    public Cell getSNPicCell(Sheet sheet);

    /**
     * Attempts to get the Cell that represents the Overall BII picture of this item
     * 
     * @param sheet Sheet to get Cell from.
     * @return Cell to place the value
     */
    public Cell getBIIPicCell(Sheet sheet);

    
}
