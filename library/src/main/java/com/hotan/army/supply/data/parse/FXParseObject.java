package com.hotan.army.supply.data.parse;

import javafx.concurrent.Service;
import javafx.concurrent.Task;
import org.parse4j.ParseException;
import org.parse4j.ParseObject;
import org.parse4j.callback.DeleteCallback;
import org.parse4j.callback.SaveCallback;

import java.lang.reflect.Constructor;

/**
 * The Parse cloud uses ParseObjects to store information in
 * the database. For any java object that we want to store
 * in the cloud, there needs to be a convenient way to
 * convert between ParseObjects and java objects.
 *
 * This abstract class provides the method definitions that are
 * required to convert ParseObjects and java objects.
 *
 * @author Mike Hotan
 */
public abstract class FXParseObject extends ParseObject {

    /**
     * Tag for logging information.
     */
    protected final String TAG = this.getClass().getSimpleName();

    public FXParseObject() {}

    public static <T extends FXParseObject> T createWithoutData(Class<T> clazz, String objectID) {
        if (objectID == null)
            throw new NullPointerException(FXParseObject.class.getSimpleName() +
                    ".createWithoutData() Object ID cannot be null");
        try {
            Constructor<T> constructor = clazz.getDeclaredConstructor(new Class[0]);
            T instance = constructor.newInstance();
            instance.setObjectId(objectID);
            return instance;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Creates a pointer to this class.  If there is no object ID then
     * will attempt to save on the current thread.  Then used the Object ID after the
     * save to return the pointer.
     *
     * @return Pointer to this instance.
     * @throws ParseException Exception when attempting to save this.
     */
    public ParseObject getPointer() throws ParseException {
        if (getObjectId() == null)
            this.save();
        return createWithoutData(getClass(), getObjectId());
    }

    @Override
    public void put(String key, Object value) {
        // Ignore null values.
        if (value == null) return;

        // Check if the existing element is already at the location with the key.
        if (value.equals(get(key))) return;

        // If the current and new value is a parse object
        // and have the same object ID then we can ignore adding it.
        Object currentValue = get(key);
        if (currentValue != null && currentValue instanceof ParseObject
                && value instanceof ParseObject) {
            // The order is important in this case.
            if (((ParseObject) currentValue).getObjectId().equals(((ParseObject) value).getObjectId())) {
                return;
            }
        }

        // Finally the regular behavior.
        super.put(key, value);
    }



    @Override
    public void saveInBackground(SaveCallback saveCallback) {
        // Create a service instance
        final SaveService service = new SaveService();

        if (saveCallback != null) {
            // Set what to do when the save succeeds
            service.setOnSucceeded(event -> saveCallback.done(null));

            // Set what to do when the save fails.
            service.setOnFailed(e -> {
                Throwable t = e.getSource().getException();
                assert t != null : "There was a failure but no exception thrown.";

                if (t instanceof ParseException)
                    saveCallback.done((ParseException) t);
                saveCallback.done(new ParseException(t));
            });
        }
        service.start();
    }

    @Override
    public void deleteInBackground(DeleteCallback deleteCallback) {
        final DeleteService service = new DeleteService();

        if (deleteCallback != null) {
            service.setOnSucceeded(e -> deleteCallback.done(null));

            // Set what to do when the save fails.
            service.setOnFailed(e -> {
                Throwable t = e.getSource().getException();
                assert t != null : "There was a failure but no exception thrown.";

                if (t instanceof ParseException)
                    deleteCallback.done((ParseException) t);
                deleteCallback.done(new ParseException(t));
            });
        }
        service.start();
    }

    private class FetchService extends Service<Void> {

        @Override
        protected Task<Void> createTask() {
            return new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    fetchIfNeeded();
                    return null;
                }
            };
        }
    }

    private class DeleteService extends Service<Void> {

        @Override
        protected Task<Void> createTask() {
            return new Task<Void>() {
                @Override
                protected Void call() throws Exception {
//                    TODO Lock the data after collection.
                    delete();
                    return null;
                }
            };
        }
    }

    private class SaveService extends Service<Void> {

        @Override
        protected Task<Void> createTask() {
            return new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    save();
                    return null;
                }
            };
        }
    }


}
