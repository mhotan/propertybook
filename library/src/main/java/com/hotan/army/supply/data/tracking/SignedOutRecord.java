package com.hotan.army.supply.data.tracking;

import com.hotan.army.supply.data.location.LocatedAt;
import com.hotan.army.supply.data.parse.FXParseObject;
import com.hotan.army.supply.data.personnel.Operator;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import org.parse4j.ParseClassName;
import org.parse4j.ParseException;
import org.parse4j.ParseObject;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * Created by mhotan on 3/21/14.
 */
@ParseClassName("SignedOutRecord")
public class SignedOutRecord extends FXParseObject {

    private static final String INDIVIDUAL = "signedOutTo";
    private static final String LOCATION = "locatedAt";
    public static final String DATE_SIGNED_OUT = "dateSignedOut";
    private static final String DATE_SIGNED_IN = "dateSignedIn";

    private final ObjectProperty<Operator> individual;

    private final ObjectProperty<LocatedAt> location;

    private final ObjectProperty<LocalDate> dateSignedOut, dateSignedIn;

    /**
     * Parse Object Constructor.
     */
    public SignedOutRecord() {
        super();
        individual = new SimpleObjectProperty<>();
        location = new SimpleObjectProperty<>();
        dateSignedOut = new SimpleObjectProperty<>();
        dateSignedIn = new SimpleObjectProperty<>();

        // Signed Out Date cannot be null.
        dateSignedOut.addListener((ov, oldVal, newVal) -> {
            assert newVal != null: "Signed out date cannot be null";
            put(DATE_SIGNED_OUT, Date.from(newVal.atStartOfDay().
                    atZone(ZoneId.systemDefault()).toInstant()).getTime());
        });
        //
        dateSignedIn.addListener((ov, oldVal, newVal) -> {
            if (newVal == null)
                remove(DATE_SIGNED_IN);
            else
                put(DATE_SIGNED_IN, Date.from(newVal.atStartOfDay().
                        atZone(ZoneId.systemDefault()).toInstant()).getTime());
        });
        this.location.addListener((ov, oldVal, newVal) -> {
            // if the new value is null then remove the key.
            if (newVal == null)
                remove(LOCATION);

            // Because Located At is specific to this Signed Out Record.
            if (oldVal != null) {
                // Delete if exists in the cloud.
                oldVal.deleteInBackground();
            }
        });
    }

    /**
     * Creates a sign out record using argument operator and current data stamp.
     *
     * @param operator Operator who is signing out the item.
     */
    public SignedOutRecord(Operator operator) {
        this(operator, LocalDate.now());
    }

    /**
     * Creates a sign out record using the argument operator and sign out.
     *
     * @param operator Operator that is signing out this item
     * @param signedOut Date signed out.
     */
    public SignedOutRecord(Operator operator, LocalDate signedOut) {
        this();
        if (operator == null)
            throw new NullPointerException(getClass().getSimpleName() + "() " +
                    "The Operator cannot be null");
        if (signedOut == null)
            throw new NullPointerException(getClass().getSimpleName() + "() " +
                    "The Date signed out cannot be null");

        this.dateSignedOut.set(signedOut);
        this.individual.set(operator);
        this.dateSignedIn.set(null);
        this.location.set(null);
    }

    @Override
    public void save() throws ParseException {
        super.save();
    }

    @Override
    public void delete() throws ParseException {
        super.delete();
    }

    /**
     * Sets the location of the item.  Can be null if location is not known.
     *
     * @param location Location to set the object to.
     */
    public void setLocation(LocatedAt location) {
        this.location.set(location);
    }

    /**
     * Set the date the object was signed in.
     *
     * @param dateSignedIn The date signed in.
     */
    public void setDateSignedIn(LocalDate dateSignedIn) {
        this.dateSignedIn.set(dateSignedIn);
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Properties
    ////////////////////////////////////////////////////////////////////////////////


    public LocalDate getDateSignedIn() {
        return dateSignedInProperty().get();
    }

    public LocalDate getDateSignedOut() {
        return dateSignedOutProperty().get();
    }

    public Operator getIndividual() {
        return individualProperty().get();
    }

    public LocatedAt getLocation() {
        return locationProperty().get();
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Properties
    ////////////////////////////////////////////////////////////////////////////////

    public ReadOnlyObjectProperty<LocalDate> dateSignedOutProperty() {
        if (this.dateSignedOut.isNull().get()) {
            long signedOut = getLong(DATE_SIGNED_OUT);
            if (signedOut != 0) {
                Date date = new Date(signedOut);
                dateSignedOut.set(LocalDateTime.ofInstant(Instant.ofEpochMilli(date.getTime()),
                        ZoneId.systemDefault()).toLocalDate());
            }
        }
        return dateSignedOut;
    }

    public ObjectProperty<LocalDate> dateSignedInProperty() {
        if (this.dateSignedIn.isNull().get()) {
            long signedIn = getLong(DATE_SIGNED_IN);
            if (signedIn != 0) {
                Date date = new Date(signedIn);
                dateSignedIn.set(LocalDateTime.ofInstant(Instant.ofEpochMilli(date.getTime()),
                        ZoneId.systemDefault()).toLocalDate());
            }
        }
        return dateSignedIn;
    }

    public ObjectProperty<LocatedAt> locationProperty() {
        if (this.location.isNull().get()) {
            ParseObject po = getParseObject(LOCATION);
            if (po != null)
                this.location.set(FXParseObject.createWithoutData(LocatedAt.class, po.getObjectId()));
        }

        return location;
    }

    public ReadOnlyObjectProperty<Operator> individualProperty() {
        if (individual.isNull().get()) {
            ParseObject po = getParseObject(INDIVIDUAL);
            if (po != null)
                individual.set(FXParseObject.createWithoutData(Operator.class, po.getObjectId()));
        }
        return individual;
    }


}
