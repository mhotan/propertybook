package com.hotan.army.supply.forms.handreceipt.subcomponent;

import com.hotan.army.supply.data.items.EndItem;
import com.hotan.army.supply.data.items.itemdata.EndItemData;
import com.hotan.army.supply.data.items.NonSerializedEndItem;
import com.hotan.army.supply.data.items.SerializedEndItem;
import com.hotan.army.supply.data.items.components.EndItemBasicIssueComponent;
import com.hotan.army.supply.data.items.components.EndItemComponent;
import com.hotan.army.supply.data.personnel.Operator;
import com.hotan.army.supply.forms.FormUtil;
import com.hotan.army.supply.forms.FormatException;
import com.hotan.army.supply.util.Alphabet;
import com.hotan.army.supply.util.POIUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.parse4j.ParseException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

/**
 * Default adapter for subcomponent hand receipt.
 *
 * @author Michael Hotan, michael.hotan@gmail.com
 */
class DefaultAdapter implements ComponentHandReceiptAdapter {

    private static final Logger LOG = Logger.getLogger(ComponentHandReceipt.class.getSimpleName());

    // General Components of the End Item.
    private static final String COIE_LABEL = "COMPONENTS OF END ITEM (COEI)";
    // BII Components of the End Item.
    private static final String BII_LABEL = "BASIC ISSUE ITEMS (BII)";

    // Index locations for certain aspects of the sheet.
    private static final POIUtil.IndexPair UICDESC_LOCATION = POIUtil.IndexPair.valueOf(2, 0);
    private static final POIUtil.IndexPair FROM_LOCATION = POIUtil.IndexPair.valueOf(3, 0);
    private static final POIUtil.IndexPair TO_LOCATION = POIUtil.IndexPair.valueOf(3, 6);
    private static final POIUtil.IndexPair NSN_LOCATION = POIUtil.IndexPair.valueOf(5, 0);
    private static final POIUtil.IndexPair LIN_LOCATION = POIUtil.IndexPair.valueOf(6, 0);
    private static final POIUtil.IndexPair SN_LOCATION = POIUtil.IndexPair.valueOf(7, 0);
    private static final POIUtil.IndexPair NAME_LOCATION = POIUtil.IndexPair.valueOf(5, 2);
    private static final POIUtil.IndexPair PUBNUM_LOCATION = POIUtil.IndexPair.valueOf(6, 2);
    private static final POIUtil.IndexPair PUBDATE_LOCATION = POIUtil.IndexPair.valueOf(6, 6);
    private static final POIUtil.IndexPair CLNUMBER_LOCATION = POIUtil.IndexPair.valueOf(5, 6);


    /**
     * The minimum row
     */
    private static final int MIN_COMPONENT_ROW = 13;
    private static final int NSN_COLUMN = Alphabet.A.index();
    private static final int ITEM_DESCRIPTION_COLUMN = Alphabet.C.index();
    private static final int AUTH_QTY_COLUMN = Alphabet.H.index();

    ////////////////////////////////////////////////////////////////////////////////
    //// Cell Getters.
    ////////////////////////////////////////////////////////////////////////////////

    @Override
    public Cell getUicCell(Sheet sheet) {
        return POIUtil.getCell(sheet, UICDESC_LOCATION);
    }

    @Override
    public Cell getDescCell(Sheet sheet) {
        return POIUtil.getCell(sheet, UICDESC_LOCATION);
    }

    @Override
    public Cell getWhoFromCell(Sheet sheet) {
        return POIUtil.getCell(sheet, FROM_LOCATION);
    }

    @Override
    public Cell getWhoToCell(Sheet sheet) {
        return POIUtil.getCell(sheet, TO_LOCATION);
    }

    @Override
    public Cell getSerialNumberCell(Sheet sheet) {
        return POIUtil.getCell(sheet, SN_LOCATION);
    }

    @Override
    public Cell getNameCell(Sheet sheet) {
        return POIUtil.getCell(sheet, NAME_LOCATION);
    }

    @Override
    public Cell getPublicationNumberCell(Sheet sheet) {
        return POIUtil.getCell(sheet, PUBNUM_LOCATION);
    }

    @Override
    public Cell getPublicationDateCell(Sheet sheet) {
        return POIUtil.getCell(sheet, PUBDATE_LOCATION);
    }

    @Override
    public Cell getNSNCell(Sheet sheet) {
        return POIUtil.getCell(sheet, NSN_LOCATION);
    }

    @Override
    public Cell getLINCell(Sheet sheet) {
        return POIUtil.getCell(sheet, LIN_LOCATION);
    }

    @Override
    public Cell getCLNumberCell(Sheet sheet) {
        return POIUtil.getCell(sheet, CLNUMBER_LOCATION);
    }

    ////////////////////////////////////////////////////////////////////////////////
    //// Getters
    ////////////////////////////////////////////////////////////////////////////////

    @Override
    public String getNSN(Sheet sheet) {
        Cell cell = getNSNCell(sheet);
        return FormUtil.getValue(cell, "END ITEM NSN:");
    }

    @Override
    public String getLIN(Sheet sheet) {
        Cell cell = getLINCell(sheet);
        return FormUtil.getValue(cell, "LIN:");
    }

    @Override
    public String getSerialNumber(Sheet sheet) {
        Cell cell = getSerialNumberCell(sheet);
        return FormUtil.getValue(cell, "SERIAL NO:");
    }

    @Override
    public String getName(Sheet sheet) {
        Cell cell = getNameCell(sheet);
        return FormUtil.getValue(cell, "ITEM DESC:");
    }

    @Override
    public String getPublicationDate(Sheet sheet) {
        Cell cell = getPublicationDateCell(sheet);
        return FormUtil.getValue(cell, "PUB DATE:");
    }

    @Override
    public String getPublicationNumber(Sheet sheet) {
        Cell cell = getPublicationNumberCell(sheet);
        return FormUtil.getValue(cell, "PUB NUM:");
    }

    @Override
    public Operator getWhoFrom(Sheet sheet) {
        Cell cell = getWhoFromCell(sheet);
        try {
            return FormUtil.getOperator(cell, "FROM: ");
        } catch (ParseException e) {
            LOG.warning("Unable to obtain a Operator from cell " + e);
            return null;
        }
    }

    @Override
    public Operator getWhoTo(Sheet sheet) {
        Cell cell = getWhoToCell(sheet);
        try {
            return FormUtil.getOperator(cell, "TO: ");
        } catch (ParseException e) {
            LOG.warning("Unable to obtain a Operator from cell " + e);
            return null;
        }
    }

    @Override
    public String getUIC(Sheet sheet) {
        return FormUtil.getValueWithSplit(getUicCell(sheet), "UIC/DESC: ", 0);
    }

    @Override
    public String getDESC(Sheet sheet) {
        return FormUtil.getValueWithSplit(getUicCell(sheet), "UIC/DESC: ", 1);
    }

    @Override
    public String getCLNumber(Sheet sheet) {
        return FormUtil.getValue(getCLNumberCell(sheet), "CL NUMBER:");
    }

    @Override
    public ObservableMap<EndItemData, ObservableList<EndItem>> processHandReceipt(Workbook workbook) {
        // Mapping of the all the itemdata.
        ObservableMap<EndItemData, ObservableList<EndItem>> mapping = FXCollections.observableHashMap();
        POIUtil.getSheets(workbook).stream().forEach(sheet -> {
            // Process the current sheet for the end item
           processSheet(sheet, mapping);
        });
        return mapping;
    }

    private void processSheet(Sheet sheet, ObservableMap<EndItemData, ObservableList<EndItem>> mapping) {
        try {
            String nsn = getNSN(sheet);
            String lin = getLIN(sheet);
            String serialNum = getSerialNumber(sheet);
            serialNum = serialNum.isEmpty() ? null: serialNum;
            String name = getName(sheet);
            String pubDate = getPublicationDate(sheet);
            String pubNum = getPublicationNumber(sheet);
            String clNumber = getCLNumber(sheet);

            // If there is not any lin or nsn match then add a new Data group.
            if (!mapping.keySet().stream().anyMatch(data -> data.getLIN().equals(lin) && data.getNSN().equals(nsn))) {
                // Create a new entry.
                EndItemData data = new EndItemData(nsn, lin);
                data.setPublicationDate(pubDate);
                data.setPublicationNumber(pubNum);
                data.addName(name);
                mapping.put(data, FXCollections.observableArrayList());
            }

            // Find the EndItem itemdata object that corresponds to the lin and nsn
            // This should always add a non null instance.
            EndItemData data = mapping.keySet().stream().filter(
                    tmp -> tmp.getLIN().equals(lin) && tmp.getNSN().equals(nsn)).findFirst().get();

            // Add the End Item that corresponds to the sheet.
            ObservableList<EndItem> items = mapping.get(data);
            EndItem item;
            if (serialNum == null)
                item = new NonSerializedEndItem("" + (items.size() + 1), clNumber, nsn, lin);
            else
                item = new SerializedEndItem(serialNum, clNumber, nsn, lin);
            items.add(item);

            Components components = collectComponentRows(sheet);
            // Find Components for this End Item.
            List<EndItemComponent> comps = components.getCoieComponents();
            for (EndItemComponent comp: comps) {
                item.addCOEI(comp);
            }

            List<EndItemBasicIssueComponent> biiComps = components.getBiiComponents();
            for (EndItemBasicIssueComponent comp: biiComps) {
                item.addBII(comp);
            }
        } catch (FormatException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns the List of valid rows.
     *
     * @return A collection of valid rows.
     * @throws com.hotan.army.supply.forms.FormatException a row that represents a component or title does not have the appropiate values.
     */
    private static Components collectComponentRows(Sheet sheet) throws FormatException {
        List<Row> biiRows = new ArrayList<Row>();
        List<Row> coieRows = new ArrayList<Row>();

        boolean addBII = false, addCOIE = false;
        for (Row row : sheet) {
            // Ignore the administrative block of itemdata.
            if (row.getRowNum() < MIN_COMPONENT_ROW) continue;
            // If we reach the signature block then we are done with
            // all the component rows.
            if (isBlankRow(row)) break;

            // We have a row that is either a title block or a component.
            Cell cell = row.getCell(Alphabet.C.index());
            String cellVal = cell == null ? null : cell.getStringCellValue();
            if (cellVal == null)
                throw new FormatException("Illegal Row that represents a component or title.");

            if (cellVal.equals(BII_LABEL)) {
                addBII = true;
                continue;
            } else if (cellVal.equals(COIE_LABEL)) {
                addCOIE = true;
                continue;
            }

            if (addBII) {
                biiRows.add(row);
            } else if (addCOIE) {
                coieRows.add(row);
            }
        }

        return new Components(biiRows, coieRows);
    }

    private static boolean isBlankRow(Row row) {
        for (Cell cell: row) {
            if (POIUtil.hasStringValue(cell)) return false;
        }
        return true;
    }

    /**
     * Gets the End Item Component component at the specified row.
     *
     * @param row Row to extract end item component from.
     * @return Accountable Component from the row. Null if none can be found.
     */
    private static EndItemComponent getCOE(Row row) {
        if (isBlankRow(row))
            throw new IllegalArgumentException("Row: " + row + " cannot be blank to be a component");

        final String name = row.getCell(ITEM_DESCRIPTION_COLUMN).getStringCellValue().trim();
        final String nsn = row.getCell(NSN_COLUMN).getStringCellValue().trim();
        final int authQty = Integer.valueOf(row.getCell(AUTH_QTY_COLUMN).getStringCellValue().trim());
        return new EndItemComponent(name, nsn, authQty);
    }

    /**
     * Gets the Basic Item Issue component at the specified row.
     *
     * @param row Row to extract end item component from.
     * @return BII Component or Null if non exist.
     */
    private static EndItemBasicIssueComponent getBII(Row row) {
        if (isBlankRow(row))
            throw new IllegalArgumentException("Row: " + row + " cannot be blank to be a component");

        final String name = row.getCell(ITEM_DESCRIPTION_COLUMN).getStringCellValue().trim();
        final String nsn = row.getCell(NSN_COLUMN).getStringCellValue().trim();
        final int authQty = Integer.valueOf(row.getCell(AUTH_QTY_COLUMN).getStringCellValue().trim());
        return new EndItemBasicIssueComponent(name, nsn, authQty);
    }

    @Override
    public void validate(Workbook workbook) throws FormatException {
        // Should always work but it is good to check anyways.
        // Get the first sheet with all the entire cover page.
        Collection<Sheet> sheets = POIUtil.getSheets(workbook);
        if (sheets.isEmpty())
            throw new FormatException("There must be atleast one sheet in the workbook.");

        for (Sheet sheet : POIUtil.getSheets(workbook)) {
            // Ignore Checking the template sheet.
            if (sheet.getSheetName().equals("template")) continue;

            sheet = workbook.getSheetAt(0);
            StringBuffer errBuf = new StringBuffer();
            Cell UIC = getUicCell(sheet);
            if (UIC == null)
                errBuf.append("Can't locate UIC Cell.");
            Cell DESC = getDescCell(sheet);
            if (DESC == null)
                errBuf.append("Can't locate DESC Cell.");
            Cell whoFromCell = getWhoFromCell(sheet);
            if (whoFromCell == null)
                errBuf.append("Can't locate Who From cell\n");
            Cell whoToCell = getWhoToCell(sheet);
            if (whoToCell == null)
                errBuf.append("Can't locate Who To cell\n");
            Cell linCell = POIUtil.getCell(sheet, LIN_LOCATION);
            if (linCell == null)
                errBuf.append("Can't locate LIN cell\n");
            Cell nsnCell = POIUtil.getCell(sheet, NSN_LOCATION);
            if (nsnCell == null)
                errBuf.append("Can't locate NSN cell\n");
            Cell serialNumCell = getSerialNumberCell(sheet);
            if (serialNumCell == null)
                errBuf.append("Can't locate Serial Number cell\n");
            Cell nameCell = getNameCell(sheet);
            if (nameCell == null)
                errBuf.append("Can't locate Name cell\n");
            Cell pubNum = getPublicationNumberCell(sheet);
            if (pubNum == null)
                errBuf.append("Can't locate Publication Number cell\n");
            Cell pubDate = getPublicationDateCell(sheet);
            if (pubDate == null)
                errBuf.append("Can't locate Publication Date cell\n");
            Cell clNumber = getCLNumberCell(sheet);
            if (clNumber == null)
                errBuf.append("Can't locate CL Number cell\n");

            // If there was an error stop everything.
            if (errBuf.length() > 0) {
                throw new FormatException(getClass().getSimpleName() + "() " + errBuf.toString());
            }

            // There should always be atleast 3 valid rows for the components area.
            collectComponentRows(sheet);
        }
    }

    private static class Components {

        private final List<EndItemBasicIssueComponent> biiComponents;
        private final List<EndItemComponent> coieComponents;

        Components(List<Row> biiComponents, List<Row> coieComponents) {
            if (biiComponents == null)
                throw new IllegalArgumentException("BII Components cannot be null");
            if (coieComponents == null)
                throw new IllegalArgumentException("COIE Components cannot be null");
            this.biiComponents = new ArrayList<EndItemBasicIssueComponent>();
            this.coieComponents = new ArrayList<EndItemComponent>();

            for (Row row : biiComponents) {
                this.biiComponents.add(getBII(row));
            }
            for (Row row : coieComponents) {
                this.coieComponents.add(getCOE(row));
            }
        }

        List<EndItemBasicIssueComponent> getBiiComponents() {
            return new ArrayList<EndItemBasicIssueComponent>(biiComponents);
        }

        List<EndItemComponent> getCoieComponents() {
            return new ArrayList<EndItemComponent>(coieComponents);
        }
    }

}


//    /**
//     * Attempts to find all the Components of this enditem on the sheet.
//     *
//     * @return list of components
//     */
//    private static List<EndItemComponent> getCOEIs(HSSFSheet  sheet) {
//        final List<EndItemComponent> components = new ArrayList<EndItemComponent>();
//
//        // Get all the valid rows.
//        List<Row> validRows = collectValidRows(sheet);
//        // Iterate through all the valid rows
//        boolean collect = false; // Flag to notify collection of components.
//        for (Row validRow : validRows) {
//            // Ignore the blank row.
//            if (isBlankRow(validRow)) {
//                collect = false;
//                continue;
//            } else if (isTitleRow(validRow)) {
//                String title = validRow.getCell(ITEM_DESCRIPTION_COLUMN).getStringCellValue().trim();
//                if (title.equals(COIE_LABEL))
//                    collect = true;
//                else
//                    collect = false;
//                continue;
//            } else { // Actual Enditem
//                if (collect) {
//                    EndItemComponent component = getCOE(validRow);
//                    assert component != null;
//                    components.add(component);
//                }
//            }
//        }
//        return components;
//    }
//
//    /**
//     * Extracts all the BII Components from the sheet.
//     *
//     * @param sheet Sheet to find BII components from.
//     * @return List of BII Components
//     */
//    private static List<EndItemBasicIssueComponent> getBIIs(HSSFSheet sheet) {
//        final List<EndItemBasicIssueComponent> components = new ArrayList<EndItemBasicIssueComponent>();
//
//        // Get all the valid rows.
//        List<Row> validRows = collectValidRows(sheet);
//        // Iterate through all the valid rows
//        boolean collect = false; // Flag to notify collection of components.
//        for (Row validRow : validRows) {
//            // Ignore the blank row.
//            if (isBlankRow(validRow)) {
//                collect = false;
//                continue;
//            } else if (isTitleRow(validRow)) {
//                String title = validRow.getCell(ITEM_DESCRIPTION_COLUMN).getStringCellValue().trim();
//                if (title.equals(BII_LABEL))
//                    collect = true;
//                else
//                    collect = false;
//                continue;
//            } else { // Actual Enditem
//                if (collect) {
//                    EndItemBasicIssueComponent component = getBII(validRow);
//                    assert component != null;
//                    components.add(component);
//                }
//            }
//        }
//        return components;
//    }