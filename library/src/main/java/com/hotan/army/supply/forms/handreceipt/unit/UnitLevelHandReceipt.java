package com.hotan.army.supply.forms.handreceipt.unit;

import com.hotan.army.supply.data.items.EndItem;
import com.hotan.army.supply.data.items.itemdata.EndItemData;
import com.hotan.army.supply.data.personnel.Operator;
import com.hotan.army.supply.forms.FormatException;
import com.hotan.army.supply.forms.Template;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.util.Date;
import java.util.logging.Logger;

/**
 *
 *
 * <b>This document is usually generated as an xls file and provided by company S4.</b>
 *
 * @author Michael Hotan, michael.hotan@gmail.com
 */
public class UnitLevelHandReceipt extends Template {

    private static final Logger LOG = Logger.getLogger(UnitLevelHandReceipt.class.getSimpleName());

    private final HSSFWorkbook workbook;

    private final Date date;
    
    private final String UIC, DESC, team;
    
    private final Operator whoFrom, toFrom;

    private final ObservableMap<EndItemData, ObservableList<EndItem>> items;

    /**
     * Creates a Unit level hand receipt from a XLS workbook.
     * 
     * @param wb Workbook to use to build this UnitLevelHandReceipt
     */
    public UnitLevelHandReceipt(HSSFWorkbook wb) throws FormatException {
        super(wb, new DefaultAdapter());
        workbook = wb;
        UnitLevelHandReceiptAdapter adapter = (UnitLevelHandReceiptAdapter) getAdapter();

        HSSFSheet sheet = wb.getSheetAt(0);
        // Find the Date.
        date = adapter.getDate(sheet);
        UIC = adapter.getUIC(sheet);
        DESC = adapter.getDESC(sheet);
        team = adapter.getTeam(sheet);
        whoFrom = adapter.getWhoFrom(sheet);
        toFrom = adapter.getWhoTo(sheet);
        items = adapter.processHandReceipt(wb);
    }

    public Operator getWhoFrom() {
        return whoFrom;
    }
    
    public Operator getWhoTo() {
        return toFrom;
    }

    public String getUIC() {
        return UIC;
    }
    
    public String getDESC() {
        return DESC;
    }
    
    public String getTeam() {
        return team;
    }
    
    public Date getDatePrepared() {
        return date;
    }
    
    /**
     * Returns the EndItems found on this hand receipt.
     * 
     * @return Unmodifiable list of groups in the enditem
     */
    public ObservableMap<EndItemData, ObservableList<EndItem>> getItems() {
        return FXCollections.unmodifiableObservableMap(items);
    }
    
    /**
     * Checks if there is an EndItem Group with matching nsn and lin.
     * 
     * @param nsn NSN of the EndItem
     * @param lin Lin number of the EndItem
     * @return Whether or not there exist an EndItem group with the same nsn and lin
     */
    public boolean hasGroup(String nsn, String lin) {
        return getGroup(nsn, lin) != null;
    }
    
    /**
     * Returns EndItem group that has the same nsn and lin number. 
     * 
     * @param nsn NSN to find.
     * @param lin LIN to find.
     * @return EndItemGroup that represents the the nsn and lin inputted, or null if non are found.
     */
    public EndItemData getGroup(String nsn, String lin) {
        return items.keySet().stream().filter(
                data -> data.getNSN().equals(nsn) && data.getLIN().equals(lin)).
                findFirst().orElse(null);
    }

    public ObservableList<EndItem> getItems(String nsn, String lin) {
        if (!hasGroup(nsn, lin)) return null;
        return this.items.get(getGroup(nsn, lin));
    }

}
