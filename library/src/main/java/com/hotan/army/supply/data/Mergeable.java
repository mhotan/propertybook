package com.hotan.army.supply.data;

/**
 * Provides the ability to merge itemdata from one item into the class instance.
 *
 * @author Michael Hotan, michael.hotan@gmail.com.
 */
public interface Mergeable<T> {

    /**
     * Merge argument's itemdata into this.
     *
     * @param item to merge into this.
     */
    public void merge(T item);

}
