package com.hotan.army.supply.util;

import com.hotan.army.supply.data.Mergeable;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;

import java.util.Collection;

/**
 * Created by mhotan on 3/7/14.
 */
public class PropertyMerger {

    /**
     * Merges the property from source to destination if destination does not have a proper value and
     * source does.
     *
     * @param destination Destination StringProperty to set.
     * @param source Source property to use to update.
     */
    public static void merge(StringProperty destination, StringProperty source) {
        if (destination.isNotNull().get() && !destination.get().isEmpty()) return;
        if (source.isNull().get() || source.get().isEmpty()) return;
        destination.set(source.get());
    }

    public static <T> void merge(ObjectProperty<T> destination, ObjectProperty<T> source) {
        if (destination.isNull().get())
            destination.set(source.get());
    }

    /**
     * Merges any item in the source list that is not currently in the destination list.
     *
     * @param destination Destination list.
     * @param source Source list
     * @param <T> Type of both list.
     */
    public static <T extends Mergeable<T>> void merge(Collection<T> destination, Collection<T> source) {

        // For every item that is in the destination check
        // whether the source list has the same item.
        destination.stream().forEach(destObj -> {
            // Obtain the source object taht matches the destObjects.
            T sourceObj = source.stream().filter(tmp ->
                    destObj.equals(tmp)).findFirst().orElse(null);
            // Merge the source object with the destination object.
            if (sourceObj != null) {
                destObj.merge(sourceObj);
            }
        });

        // For any item that is not in the destination but in the source add it.
        source.stream().filter(sourceItem -> !destination.contains(sourceItem)).forEach(item ->{
             destination.add(item);
        });
    }

    /**
     * Merges any item in the source list that is not currently in the destination list.
     *
     * @param destination Destination list.
     * @param source Source list
     * @param <T> Type of both list.
     */
    public static <T> void simpleMerge(Collection<T> destination, Collection<T> source) {
        // For any item that is not in the destination but in the source add it.
        source.stream().filter(sourceItem -> !destination.contains(sourceItem)).forEach(item ->{
            destination.add(item);
        });
    }

}
