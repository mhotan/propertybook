package com.hotan.army.supply.exceptions.propertybook;

/**
 * Illegal PropertBook name exception.
 *
 * @author Michael Hotan, michael.hotan@gmail.com
 */
public class IllegalPropertyBookNameException extends PropertyBookException {

    private final String name;

    public IllegalPropertyBookNameException(String argumentName) {
        super(new Exception("Illegal Property Book name \"" + argumentName + "\""));
        this.name = argumentName;
    }

    public String getName() {
        return name;
    }
}
