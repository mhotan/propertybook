package com.hotan.army.supply.data.items.itemdata;

import com.hotan.army.supply.data.Mergeable;
import com.hotan.army.supply.data.parse.FXParseObject;
import com.hotan.army.supply.data.personnel.MOS;
import com.hotan.army.supply.util.ImageConverter;
import com.hotan.army.supply.util.PropertyMerger;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import org.parse4j.ParseClassName;
import org.parse4j.ParseException;
import org.parse4j.ParseFile;
import org.parse4j.ParseObject;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

/**
 * Specific itemdata
 *
 * @author Michael Hotan, michael.hotan@gmail.com
 */
@ParseClassName("EndItemData")
public class EndItemData extends FXParseObject implements Mergeable<EndItemData> {

    public static final String LIN_KEY = "lin";
    public static final String NSN_KEY = "nsn";
    private static final String MOS_KEY = "mos";
    private static final String PUB_NUM = "publicationNumber";
    private static final String PUB_DATE = "publicationDate";
    private static final String NAMES = "names";
    private static final String OVERVIEW_IMAGE = "overview";
    private static final String SERIAL_NUMBER_IMAGE = "serialNumber";
    private static final String COMPONENTS_IMAGE = "components";
    private static final String LOCATION_IMAGE = "location";

    private static final Logger LOG = Logger.getLogger(EndItemData.class.getName());


    /*
    Representation Invariant:
    NSN cannot be null or empty
    LIN cannot be null or empty
    names cannot be null nor Empty

    */

    /**
     * Aliases that this item goes by.
     */
    private final ObservableList<String> names;

    /**
     * Publication itemdata.
     */
    private final StringProperty publicationNumber, publicationDate, NSN, LIN;

    /**
     * The default name of the EndItem.
     */
    private final StringBinding defaultName;

    /**
     * Assigned MOS of the end item group.
     */
    private final ObjectProperty<MOS> assignedMOS;

    /**
     * The images that pertain to this.
     */
    private final ObjectProperty<Image>
            overviewImage, serialNumberImage, componentsImage, locationImage;

    public EndItemData() {
        this.names = FXCollections.observableArrayList();
        this.publicationNumber = new SimpleStringProperty();
        this.publicationDate = new SimpleStringProperty();
        this.defaultName = Bindings.stringValueAt(names, 0);
        this.assignedMOS = new SimpleObjectProperty<MOS>();
        this.overviewImage = new SimpleObjectProperty<>();
        this.serialNumberImage = new SimpleObjectProperty<>();
        this.componentsImage = new SimpleObjectProperty<>();
        this.locationImage = new SimpleObjectProperty<>();
        this.NSN = new SimpleStringProperty();
        this.LIN = new SimpleStringProperty();

        // Handle all the values that can never be null.
        NSN.addListener((ov, oldVal, newVal) -> put(NSN_KEY, newVal));
        LIN.addListener((ov, oldVal, newVal) -> put(LIN_KEY, newVal));

        // Place a handler for all the primitive and String types
        publicationNumber.addListener((ov, oldVal, newVal) ->  {
            if (newVal != null)
                put(PUB_NUM, newVal);
            else
                remove(PUB_NUM);
        });
        publicationDate.addListener((ov, oldVal, newVal) -> {
            if (newVal != null)
                put(PUB_DATE, newVal);
            else
                remove(PUB_DATE);
        });
        assignedMOS.addListener((ov, oldVal, newVal) -> {
            if (newVal != null)
                put(MOS_KEY, newVal.toString());
            else
                remove(MOS_KEY);
        });

    }

    /**
     * Creates and instance of the EndItem itemdata with the associated NSN and LIN.
     *
     * @param nsn NSN of the End Item
     * @param lin LIN of the End Item
     */
    public EndItemData(String nsn, String lin) {
        this();
        this.NSN.set(nsn);
        this.LIN.set(lin);

        // Put the name of the list
        put(NAMES, names);
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //// Overriden Parse itemdata
    //////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void save() throws ParseException {
        // The overview image
        saveImage(overviewImage, OVERVIEW_IMAGE);
        saveImage(serialNumberImage, SERIAL_NUMBER_IMAGE);
        saveImage(componentsImage, COMPONENTS_IMAGE);
        saveImage(locationImage, LOCATION_IMAGE);

        // Saves the list of names.
        List<String> storedNames = getList(NAMES);
        if (storedNames != null && !this.names.equals(storedNames)) {
            put(NAMES, this.names);
        }

        super.save();
    }

    private void saveImage(ObjectProperty<Image> imageProp, String name) throws ParseException {
        if (imageProp.isNotNull().get()) {
            ParseFile currentFile = getParseFile(name);
            byte[] bytes = ImageConverter.toByteArray(imageProp.get());

            // If the current value is already stored then we can ignore the saving process.
            if (currentFile != null) {
                byte[] data = currentFile.getData();
                if (data != null && Arrays.equals(bytes, data)) {
                    return;
                }
            }

            // Create and save a new Parse File as image.
            ParseFile pImage = new ParseFile(bytes);
            pImage.save();
            put(name, pImage);
        }
    }

    @Override
    public void delete() throws ParseException {
        super.delete();
    }

    @Override
    public <T extends ParseObject> T fetchIfNeeded() throws ParseException {
        return super.fetchIfNeeded();
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //// Setters
    //////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the Publication Number of this End Item
     *
     * @param publicationNumber Publication Number.
     */
    public void setPublicationNumber(String publicationNumber) {
        if (publicationNumber == null) return;
        this.publicationNumber.set(publicationNumber);
    }

    /**
     * Set the date of publication.
     *
     * @param publicationDate Date
     */
    public void setPublicationDate(String publicationDate) {
        if (publicationDate == null) return;
        this.publicationDate.set(publicationDate);
    }

    /**
     * Set the assigned mos.
     * @param assignedMOS MOS to set
     */
    public void setAssignedMOS(MOS assignedMOS) {
        if (assignedMOS == null) return;
        this.assignedMOS.set(assignedMOS);
    }

    public void setOverviewImage(Image overviewImage) {
        this.overviewImage.set(overviewImage);
    }

    public void setSerialNumberImage(Image serialNumberImage) {
        this.serialNumberImage.set(serialNumberImage);
    }

    public void setComponentsImage(Image componentsImage) {
        this.componentsImage.set(componentsImage);
    }

    public void setLocationImage(Image locationImage) {
        this.locationImage.set(locationImage);
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //// Mutators
    //////////////////////////////////////////////////////////////////////////////////////////

    public void addName(String name) {
        if (name == null || name.trim().isEmpty()) return;
        names.add(name.trim());
    }

    public boolean removeName(String name) {
        return names.remove(name);
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //// Getters
    //////////////////////////////////////////////////////////////////////////////////////////

    public ObservableList<String> getNames() {
        if (names.isEmpty()) {
            List<String> storedNames = getList(NAMES);
            if (storedNames != null) {
                this.names.addAll(storedNames);
            }
        }
        return names;
    }

    public String getPublicationNumber() {
        return publicationNumberProperty().get();
    }

    public String getPublicationDate() {
        return publicationDateProperty().get();
    }

    public String getNSN() {
        return NSNProperty().get();
    }

    public String getLIN() {
        return LINProperty().get();
    }

    public String getDefaultName() {
        return defaultNameProperty().get();
    }

    public MOS getAssignedMOS() {
        return assignedMOSProperty().get();
    }

    public Image getOverviewImage() {
        return overviewImageProperty().get();
    }

    public Image getSerialNumberImage() {
        return serialNumberImageProperty().get();
    }

    public Image getComponentsImage() {
        return componentsImageProperty().get();
    }

    public Image getLocationImage() {
        return locationImageProperty().get();
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //// Properties
    //////////////////////////////////////////////////////////////////////////////////////////

    public StringProperty publicationNumberProperty() {
        if (publicationNumber.isNull().get()) {
            publicationNumber.set(getString(PUB_NUM));
        }
        return publicationNumber;
    }

    public StringProperty publicationDateProperty() {
        if (publicationDate.isNull().get()) {
            publicationDate.set(getString(PUB_DATE));
        }
        return publicationDate;
    }

    public StringProperty NSNProperty() {
        if (NSN.isNull().get())
            NSN.set(getString(NSN_KEY));
        return NSN;
    }

    public StringProperty LINProperty() {
        if (LIN.isNull().get())
            LIN.set(getString(LIN_KEY));
        return LIN;
    }

    public ObjectProperty<MOS> assignedMOSProperty() {
        if (assignedMOS.isNull().get())
            assignedMOS.set(MOS.getMOS(getString(MOS_KEY)));
        return assignedMOS;
    }

    public StringBinding defaultNameProperty() {
        return defaultName;
    }

    /**
     * The image stored at key.
     *
     * @param name Parse Key of the Image.
     * @return The image if it is stored in the cloud.
     */
    private Image getImage(String name) {
        try {
            ParseFile file = getParseFile(OVERVIEW_IMAGE);
            if (file == null) return null;
            byte[] data = file.getData();
            if (data == null) return null;
            return ImageConverter.toImage(data);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * @return Overview Image Property.
     */
    public ObjectProperty<Image> overviewImageProperty() {
        if (overviewImage.isNull().get())
            overviewImage.set(getImage(OVERVIEW_IMAGE));
        return overviewImage;
    }

    /**
     * @return Serial Number Image Property.
     */
    public ObjectProperty<Image> serialNumberImageProperty() {
        if (serialNumberImage.isNull().get())
            serialNumberImage.set(getImage(SERIAL_NUMBER_IMAGE));
        return serialNumberImage;
    }

    /**
     * @return Components Image Property.
     */
    public ObjectProperty<Image> componentsImageProperty() {
        if (componentsImage.isNull().get())
            componentsImage.set(getImage(COMPONENTS_IMAGE));
        return componentsImage;
    }

    /**
     * @return Location Image Property.
     */
    public ObjectProperty<Image> locationImageProperty() {
        if (locationImage.isNull().get())
            locationImage.set(getImage(LOCATION_IMAGE));
        return locationImage;
    }

    @Override
    public void merge(EndItemData item) {
        if (item == null || !this.equals(item)) return;
        PropertyMerger.simpleMerge(this.names, item.names);
        PropertyMerger.merge(this.publicationNumber, item.publicationNumber);
        PropertyMerger.merge(this.publicationDate, item.publicationDate);
        PropertyMerger.merge(this.assignedMOS, item.assignedMOS);
        PropertyMerger.merge(this.overviewImage, item.overviewImage);
        PropertyMerger.merge(this.serialNumberImage, item.serialNumberImage);
        PropertyMerger.merge(this.componentsImage, item.componentsImage);
        PropertyMerger.merge(this.locationImage, item.locationImage);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EndItemData)) return false;

        EndItemData that = (EndItemData) o;
        if (!LIN.isEqualTo(that.LIN).get()) return false;
        if (!NSN.isEqualTo(that.NSN).get()) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = NSN.get().hashCode();
        result = 31 * result + LIN.get().hashCode();
        return result;
    }
}
