package com.hotan.army.supply.forms.handreceipt.subcomponent;

import com.hotan.army.supply.data.items.EndItem;
import com.hotan.army.supply.data.items.itemdata.EndItemData;
import com.hotan.army.supply.data.personnel.Operator;
import com.hotan.army.supply.forms.FormAdapter;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * An adapter used for interacting with a Component Hand Receipt Workbook.
 *
 * @author Michael Hotan, michael.hotan@gmail.com
 */
public interface ComponentHandReceiptAdapter extends FormAdapter {

    ////////////////////////////////////////////////////////////////////////////////
    //// Cell Getters
    ////////////////////////////////////////////////////////////////////////////////

    Cell getUicCell(Sheet sheet);
    Cell getDescCell(Sheet sheet);
    Cell getWhoFromCell(Sheet sheet);
    Cell getWhoToCell(Sheet sheet);
    Cell getNSNCell(Sheet sheet);
    Cell getLINCell(Sheet sheet);
    Cell getSerialNumberCell(Sheet sheet);
    Cell getNameCell(Sheet sheet);
    Cell getPublicationNumberCell(Sheet sheet);
    Cell getPublicationDateCell(Sheet sheet);
    Cell getCLNumberCell(Sheet sheet);

    ////////////////////////////////////////////////////////////////////////////////
    //// Regular Getters
    ////////////////////////////////////////////////////////////////////////////////

    Operator getWhoFrom(Sheet sheet);
    Operator getWhoTo(Sheet sheet);
    String getUIC(Sheet sheet);
    String getDESC(Sheet sheet);
    String getNSN(Sheet sheet);
    String getLIN(Sheet sheet);
    String getSerialNumber(Sheet sheet);
    String getName(Sheet sheet);
    String getPublicationDate(Sheet sheet);
    String getPublicationNumber(Sheet sheet);
    String getCLNumber(Sheet sheet);

    /**
     * Process the workbook and extract all the EndItemGroups contained.
     *
     * @param workbook Workbook to process.
     * @return All the EndItem groups.
     */
    ObservableMap<EndItemData, ObservableList<EndItem>> processHandReceipt(Workbook workbook);
}
