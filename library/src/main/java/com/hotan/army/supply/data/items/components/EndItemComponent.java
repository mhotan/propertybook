package com.hotan.army.supply.data.items.components;

import com.hotan.army.supply.data.Mergeable;
import org.parse4j.ParseClassName;

/**
 * Creates a regular End Item Component.
 *
 * @author Michael Hotan, michael.hotan@gmail.com
 */
@ParseClassName("COIEComponent")
public class EndItemComponent extends AccountableComponent implements Mergeable<EndItemComponent>  {

    public EndItemComponent() {
        super();
    }

    public EndItemComponent(String name, String nsn) {
        super(name, nsn);
    }

    public EndItemComponent(String name, String nsn, int qty) {
        super(name, nsn, qty);
    }

    // Potentially add specific COIE functionality.


    @Override
    public void merge(EndItemComponent item) {

    }
}
