package com.hotan.army.supply.data;

import com.hotan.army.supply.LibraryConstants;
import com.hotan.army.supply.data.parse.DataManager;
import com.hotan.army.supply.exceptions.propertybook.IllegalPropertyBookNameException;
import com.hotan.army.supply.exceptions.propertybook.PropertyBookAlreadyExistsException;
import com.hotan.army.supply.forms.handreceipt.subcomponent.ComponentHandReceipt;
import com.hotan.army.supply.forms.handreceipt.unit.UnitLevelHandReceipt;
import org.parse4j.ParseException;
import org.parse4j.ParseQuery;
import org.parse4j.callback.GetCallback;
import org.parse4j.callback.SaveCallback;

import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author Michael Hotan,
 */
public class PropertyBookManager extends DataManager<PropertyBook> {

    private static final Logger LOG = Logger.getLogger(PropertyBookManager.class.getName());

    /*
    Representation Invariant:
    instance == null ==> No call to get any property books have been called.
    instance != null ==> books != null && name != null;
    books.keySet() is the names of all the current Property Books.
    For String name where books.contains(name) then books.get(name) == The Parse Object of

    Abstract Function:
    instance is the single instance that provides access to PropertyBook objects in the cloud.
    books mapping to either ParseObject or actual property book.
    names current list of all the property books by name.
    * */

    private static PropertyBookManager instance;

    private PropertyBookManager() {
        try {
            ParseQuery<PropertyBook> query = ParseQuery.getQuery(PropertyBook.class);
            List<PropertyBook> books = query.find();
            if (books == null) return;
            books.stream().forEach(book -> this.objects.add(book));
        } catch (ParseException e) {
            LOG.warning("Unable to find property books");
        }
    }

    /**
     * Returns the singleton instance of this.
     *
     * @return Singleton instance
     */
    public static PropertyBookManager getInstance() {
        if (instance == null) {
            instance = new PropertyBookManager();
        }
        return instance;
    }

    @Override
    protected Class<PropertyBook> getDataClass() {
        return PropertyBook.class;
    }

    public synchronized void getBookByName(String bookName, GetCallback<PropertyBook> cb) {
        if (bookName == null || bookName.trim().isEmpty())
            throw new IllegalArgumentException(getClass().getSimpleName() + "get(), " +
                    "Illegal Property Book Name: \"" + bookName + "\"");


        final String name = bookName.trim();
        PropertyBook book = this.objects.stream().filter(b ->
                b.getName().equalsIgnoreCase(name)).findAny().orElse(null);
        if (book != null) {
            cb.done(book, null);
            return;
        }

        // Check the cloud on current thread.
        ParseQuery<PropertyBook> query = ParseQuery.getQuery(PropertyBook.class);
        query.whereEqualTo(PropertyBook.NAME, bookName);
        findFirstInBackgroud(query, cb);
    }

    /**
     * Returns the Property Book associated with argument name.  Names can
     * be found using the getExistingBooks() call.
     *
     * @param bookName name of the property book to find.
     * @return the property book with the name bookName
     */
    public synchronized PropertyBook getBookByName(String bookName) throws ParseException {
        if (bookName == null || bookName.trim().isEmpty())
            throw new IllegalArgumentException(getClass().getSimpleName() + "get(), " +
                    "Illegal Property Book Name: \"" + bookName + "\"");
        final String name = bookName.trim();
        PropertyBook book = this.objects.stream().filter(b ->
                b.getName().equalsIgnoreCase(name)).findAny().orElse(null);
        if (book != null) return book;

        // Check the cloud on current thread.
        ParseQuery<PropertyBook> query = ParseQuery.getQuery(PropertyBook.class);
        query.whereEqualTo(PropertyBook.NAME, bookName);
        List<PropertyBook> bookPOs = query.find();
        if (bookPOs == null || bookPOs.isEmpty()) return null;

        book = bookPOs.get(0);
        add(book);
        return book;
    }

//    /**
//     *
//     * @param bookName
//     * @param cb Callback that notifies
//     */
//    public void getBook(String bookName, final PropertyBookFoundCallback cb) {
//        // Check our local mapping
//        PropertyBook book = find(bookName);
//        if (book != null) {
//            cb.done(book, null);
//            return;
//        }
//
//        ParseQuery<PropertyBook> query = ParseQuery.getQuery(PropertyBook.class);
//        query.whereEqualTo(PropertyBook.NAME, bookName);
//        query.include(PropertyBook.SERIALIZED_END_ITEMS);
//        query.include(PropertyBook.NONSERIALIZED_END_ITEMS);
//
//        // Find in the background.
//        ParseUtil.query(query, new FindCallback<PropertyBook>() {
//            @Override
//            public void done(List<PropertyBook> list, ParseException parseException) {
//
//                // Notify error.
//                if (parseException != null) {
//                    cb.done(null, parseException);
//                    return;
//                }
//                // Notify nothing found.
//                if (list == null || list.isEmpty()) {
//                    cb.done(null, null);
//                    return;
//                }
//
//                // Notify something has been found
//                PropertyBook book = list.get(0);
//                books.put(book.getName(), book);
//                cb.done(book, null);
//            }
//        });
//
//    }

    /**
     * Checks whether the property book already exists in the cloud.
     *
     * @param bookName Name of the book to check for
     * @return Whether or not the property book has
     * @throws ParseException
     */
    private boolean hasBook(final String bookName) {
        // Do a invariant check to make sure we don't have to make a network call.
        return this.objects.stream().anyMatch(book -> book.getName().equalsIgnoreCase(bookName));
    }

    public void addBook(String bookName,
                        UnitLevelHandReceipt receipt,
                        ComponentHandReceipt components,
                        final GetCallback<PropertyBook> cb) {
        // The name cannot be null.
        if (bookName == null || bookName.trim().isEmpty()) {
            cb.done(null, new IllegalPropertyBookNameException(getClass().getSimpleName() + "get(), " +
                    "Illegal Property Book Name: \"" + bookName + "\""));
        }
        // Remove any leading or trailing white space
        bookName = bookName.trim();

        if (LibraryConstants.DEBUG) { // For debugging purposes lets just create a new book each time.
            cb.done(new PropertyBook(bookName, receipt, components), null);
            return;
        }

        // If there is a book with the name already ignore it.
        if (hasBook(bookName)) {
            cb.done(null, new PropertyBookAlreadyExistsException(bookName + " already exists"));
        }

        final PropertyBook book = new PropertyBook(bookName, receipt, components);
        book.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e != null) {
                    cb.done(null, e);
                    return;
                }
                add(book);
                cb.done(book, null);
            }
        });

    }

    /**
     * Adds the property book with an associated name.
     *
     * @param bookName Name of the property book to add, cannot be null
     * @param receipt The Unit level hand receipt of this property book.
     * @param components SubComponent hand receipt of this team.
     * @return The added property book.
     * @throws org.parse4j.ParseException Unable to create a new property book
     */
    public synchronized PropertyBook addBook(String bookName,
                                UnitLevelHandReceipt receipt,
                                ComponentHandReceipt components)
            throws ParseException {

        // The name cannot be null.
        if (bookName == null || bookName.trim().isEmpty())
            throw new IllegalPropertyBookNameException(getClass().getSimpleName() + "get(), " +
                    "Illegal Property Book Name: \"" + bookName + "\"");

        // Remove any leading or trailing white space
        bookName = bookName.trim();

        if (LibraryConstants.DEBUG) { // For debugging purposes lets just create a new book each time.
            return new PropertyBook(bookName, receipt, components);
        }

        // If there is a book with the name already ignore it.
        if (hasBook(bookName))
            throw new PropertyBookAlreadyExistsException(bookName + " already exists");

        PropertyBook book = new PropertyBook(bookName, receipt, components);
        book.save();
        add(book);
        return book;
    }
//
//    public interface PropertyBookCreatedCallback {
//
//        /**
//         * Returns the property book that was created and saved.  Returns error
//         * as non null exception.
//         *
//         *
//         * @param book Book that was found,
//         * @param exception Non null exception.
//         */
//        public void done(PropertyBook book, ParseException exception);
//
//    }
//
//    public interface PropertyBookFoundCallback {
//
//        /**
//         * Returns the property book that was found.  Returns null book if no book was found.
//         * If there was an error the exception argument is not null.
//         *
//         *
//         * @param book Book that was found,
//         * @param exception Non null exception.
//         */
//        public void done(PropertyBook book, ParseException exception);
//
//    }
//
//    public interface NamesFoundCallback {
//
//        /**
//         * Notified when the list of names is found or exception found.
//         *
//         * @param names Current list of Property Book names
//         * @param exception Non null exception if an error occured.
//         */
//        public void done(ObservableList<String> names, ParseException exception);
//
//    }



}
