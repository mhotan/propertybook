package com.hotan.army.supply.data.location;

import com.hotan.army.supply.data.parse.FXParseObject;
import javafx.beans.property.*;
import org.parse4j.ParseClassName;
import org.parse4j.ParseGeoPoint;

/**
 * Created by Michael Hotan on 3/6/14.
 */
@ParseClassName("Location")
public class Location extends FXParseObject {

    static final String NAME = "name";
    private static final String GEO_POINT = "geoPoint";

    /*
    Representation Invariant:
    name != null and !name.isEmpty()

    Abstract Function:
    name is the name of the location.
    Longitude or Latitude is null then the location is unknown.
    * */

    public static final double MIN_LATITUDE = -90.0;
    public static final double MAX_LATITUDE = 90.0;
    public static final double MIN_LONGITUDE = -180.0;
    public static final double MAX_LONGITUDE = 180.0;
    public static final double EPSILON = .1;

    private final StringProperty name;

    private final DoubleProperty latitude, longitude;

    public Location() {
        name = new SimpleStringProperty();
        latitude = new SimpleDoubleProperty(Double.MIN_VALUE);
        longitude = new SimpleDoubleProperty(Double.MIN_VALUE);

        name.addListener((ov, oldVal, newVal) -> {
            if (newVal == null || newVal.trim().isEmpty()) return;
            put(NAME, newVal);
        });
        latitude.addListener((ov, oldVal, newVal) -> {
            put(GEO_POINT, new ParseGeoPoint(newVal.doubleValue(), longitude.get()));
        });
        longitude.addListener((ov, oldVal, newVal) -> {
            put(GEO_POINT, new ParseGeoPoint(latitude.get(), newVal.doubleValue()));
        });
    }

    /**
     * Creates a new location instance with a single name.
     *
     * @param name Name of the location.
     */
    Location(String name) {
        this(name, 0.0, 0.0);
    }

    Location(String name, double latitude, double longitude) {
        this();
        // Check if the name is legal.
        if (name == null || name.trim().isEmpty()) {
            throw new IllegalArgumentException(getClass().getSimpleName() + "() " +
                    "Illegal name \"" + name + "\"");
        }

        // Check if the latitude is legal.
        if (latitude <= MIN_LATITUDE || latitude >= MAX_LATITUDE) {
            throw new IllegalArgumentException(getClass().getSimpleName() + "() " +
                    "Illegal latitude:" + latitude + " must be within (" + MIN_LATITUDE + ", "
                    + MAX_LATITUDE + ")");
        }
        // Check if the longitude is legal.
        if (longitude <= MIN_LATITUDE || longitude >= MAX_LATITUDE) {
            throw new IllegalArgumentException(getClass().getSimpleName() + "() " +
                    "Illegal longitude:" + longitude + " must be within (" + MIN_LONGITUDE + ", "
                    + MAX_LONGITUDE + ")");
        }

        this.name.set(name);
        this.latitude.set(latitude);
        this.longitude.set(longitude);
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //// Setters
    //////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Set the latitude of this location.
     *
     * @param latitude Latitude for this location.
     */
    public void setLatitude(double latitude) {
        this.latitude.set(Math.max(Math.min(latitude, MAX_LATITUDE), MIN_LATITUDE));
    }

    /**
     * Set the longitude of this location.
     *
     * @param longitude Longitude for this location.
     */
    public void setLongitude(double longitude) {
        this.longitude.set(Math.max(Math.min(longitude, MAX_LONGITUDE), MIN_LONGITUDE));
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //// Getters
    //////////////////////////////////////////////////////////////////////////////////////////

    public String getName() {
        return nameProperty().get();
    }

    public double getLatitude() {
        return latitudeProperty().get();
    }

    public double getLongitude() {
        return longitudeProperty().get();
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //// Properties
    //////////////////////////////////////////////////////////////////////////////////////////

    public DoubleProperty latitudeProperty() {
        if (latitude.isEqualTo(Double.MIN_VALUE, EPSILON).get()) {
            ParseGeoPoint point = getParseGeoPoint(GEO_POINT);
            if (point == null)
                latitude.set(0);
            else {
                latitude.set(point.getLatitude());
                longitude.set(point.getLongitude());
            }
        }
        return latitude;
    }

    public DoubleProperty longitudeProperty() {
        if (longitude.isEqualTo(Double.MIN_VALUE, EPSILON).get()) {
            ParseGeoPoint point = getParseGeoPoint(GEO_POINT);
            if (point == null)
                longitude.set(0);
            else {
                latitude.set(point.getLatitude());
                longitude.set(point.getLongitude());
            }
        }
        return longitude;
    }

    public ReadOnlyStringProperty nameProperty() {
        if (this.name.isNull().get()) {
            name.set(getString(NAME));
        }
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Location)) return false;

        Location location = (Location) o;
        if (!name.isEqualTo(location.name).get()) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return nameProperty().get();
    }
}
