package com.hotan.army.supply.data.personnel;

import com.hotan.army.supply.data.parse.FXParseObject;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.parse4j.ParseClassName;

/**
 * Class that represents a single operator 
 * @author Michael Hotan, michael.hotan@gmail.com
 */
@ParseClassName("Operator")
public class Operator extends FXParseObject {

    /*
    Representation Invariant
    firstName and lastName cannot be null nor empty
    middleName cannot be null

    Abstract Function

     */

    static final String RANK = "rank";
    static final String MOS_KEY = "mos";
    static final String FIRST_NAME = "firstName";
    static final String MIDDLE_NAME = "middleName";
    static final String LAST_NAME = "lastName";
    static final String PHONE_NUMBER = "phoneNumber";

    /**
     * Rank of this operator.
     */
    private final ObjectProperty<Rank> rank;

    /**
     * First and Last name.
     */
    private final StringProperty firstName, middleName, lastName, phoneNumber;

    /**
     * MOS of this End Item.
     */
    private final ObjectProperty<MOS> MOS;

    public Operator() {
        rank = new SimpleObjectProperty<>();
        firstName = new SimpleStringProperty();
        middleName = new SimpleStringProperty();
        lastName = new SimpleStringProperty();
        phoneNumber = new SimpleStringProperty();
        MOS = new SimpleObjectProperty<>();

        rank.addListener((ov, oldVal, newVal) -> {
            if (newVal == null)
                remove(RANK);
            else
                put(RANK, newVal.toString());
        });
        firstName.addListener((ov, oldVal, newVal) -> {
            // New val can never be null
            put(FIRST_NAME, newVal);
        });
        middleName.addListener((ov, oldVal, newVal) -> {
            // New val can never be null
            put(MIDDLE_NAME, newVal);
        });
        lastName.addListener((ov, oldVal, newVal) -> {
            // New val can never be null
            put(LAST_NAME, newVal);
        });
        phoneNumber.addListener((ov, oldVal, newVal) -> {
            if (newVal == null)
                remove(PHONE_NUMBER);
            else
                put(PHONE_NUMBER, newVal);
        });
        MOS.addListener((ov, oldVal, newVal) -> {
            if (newVal == null)
                remove(MOS_KEY);
            else
                put(MOS_KEY, newVal.toString());
        });
    }

    /**
     * Creates a new Operator.
     *
     * @param firstName First name for this operator.
     * @param middleName Middle name for this operator.
     * @param lastName Last name for this operator.
     */
    Operator(String firstName, String middleName, String lastName) {
        this(firstName, middleName, lastName, null, null);
    }

    /**
     * Creates a new operator.
     *
     * @param firstName First name for this operator.
     * @param middleName Middle name for this operator.
     * @param lastName Last name for this operator.
     * @param rank Rank of this operator.
     * @param mos MOS of this operator.
     */
    Operator(String firstName, String middleName, String lastName, Rank rank, MOS mos) {
        this();
        if (firstName == null)
            throw new IllegalArgumentException(Operator.class.getSimpleName()
                    + "(), Can't have null FirstName");
        if (lastName == null)
            throw new IllegalArgumentException(Operator.class.getSimpleName()
                    + "(), Can't have null LastName");
        if (firstName.trim().isEmpty())
            throw new IllegalArgumentException(Operator.class.getSimpleName()
                    + "(), Can't have empty first name");
        if (lastName.trim().isEmpty())
            throw new IllegalArgumentException(Operator.class.getSimpleName()
                    + "(), Can't have empty last name");
        if (middleName == null)
            middleName = "";

        // Set immutable fields
        this.firstName.set(firstName);
        this.middleName.set(middleName);
        this.lastName.set(lastName);
        setRank(rank);
        setMOS(mos);
    }

    /**
     * Creates an Operator.
     *
     * @param firstName First name of the operator.
     * @param lastName Last name of the operator.
     * @param rank Rank of the Operator.
     * @param mos MOS of the Operator.
     */
    Operator(String firstName, String lastName, Rank rank, MOS mos) {
        this(firstName, "", lastName, rank, mos);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////    Mutators and Setters
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void setMOS(MOS MOS) {
        this.MOS.set(MOS);
    }

    public void setRank(Rank rank) {
        this.rank.set(rank);
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber.set(phoneNumber);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////    Getters
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    public Rank getRank() {return rankProperty().get();}

    public String getFirstName() { return firstNameProperty().get();}

    public String getMiddleName() {
        return middleNameProperty().get();
    }

    public String getLastName() {
        return lastNameProperty().get();
    }

    public MOS getMOS() {
        return MOSProperty().get();
    }

    public String getPhoneNumber() {
        return phoneNumberProperty().get();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////    Getters
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    public StringProperty firstNameProperty() {
        if (firstName.isNull().get()) {
            firstName.set(getString(FIRST_NAME));
        }
        return firstName;
    }

    public StringProperty middleNameProperty() {
        if (middleName.isNull().get()) {
            middleName.set(getString(MIDDLE_NAME));
        }
        return middleName;
    }

    public StringProperty lastNameProperty() {
        if (lastName.isNull().get()) {
            lastName.set(getString(LAST_NAME));
        }
        return lastName;
    }

    public ObjectProperty<Rank> rankProperty() {
        if (rank.isNull().get()) {
            rank.set(Rank.getRank(getString(RANK)));
        }
        return rank;
    }

    public ObjectProperty<MOS> MOSProperty() {
        if (MOS.isNull().get()) {
            MOS.set(com.hotan.army.supply.data.personnel.MOS.getMOS(getString(MOS_KEY)));
        }
        return MOS;
    }

    public StringProperty phoneNumberProperty() {
        if (phoneNumber.isNull().get()) {
            phoneNumber.set(getString(PHONE_NUMBER));
        }
        return phoneNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Operator)) return false;
        if (!super.equals(o)) return false;

        Operator operator = (Operator) o;
        if (!firstNameProperty().isEqualTo(operator.firstNameProperty()).get()) return false;
        if (!lastNameProperty().isEqualTo(operator.lastNameProperty()).get()) return false;
        if (!middleNameProperty().isEqualTo(operator.middleNameProperty()).get()) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + firstNameProperty().get().hashCode();
        result = 31 * result + middleNameProperty().get().hashCode();
        result = 31 * result + lastNameProperty().get().hashCode();
        return result;
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        if (getRank() != null) {
            b.append(getRank().toString() + " ");
        }
        b.append(getLastName() + ", " + getFirstName());
        if (getMOS() != null) {
            b.append(" (" + getMOS().toString() + ")");
        }
        return b.toString();
    }


}
