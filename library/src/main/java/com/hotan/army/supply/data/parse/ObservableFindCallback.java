package com.hotan.army.supply.data.parse;

import javafx.collections.ObservableList;
import org.parse4j.ParseException;
import org.parse4j.ParseObject;
import org.parse4j.callback.FindCallback;

/**
 * Created by mhotan on 3/21/14.
 */
public abstract class ObservableFindCallback<T extends ParseObject> extends FindCallback<T> {

    public abstract void done(ObservableList<T> list, ParseException parseException);

}
