package com.hotan.army.supply.util.loader;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.IOException;
import java.io.InputStream;

public class POITemplateLoader {

    private POITemplateLoader() {}
    private static final String DIRECTORY = "templates";
    
    public static HSSFWorkbook getXLSWorkbook(String name) throws IOException {
        // Validate the name.
        if (name == null || !name.endsWith(".xls"))
            throw new IllegalArgumentException("Illegal name of template to load: " + name);

        // Use the baseloader to extract the file
        InputStream is = BaseLoader.stream(DIRECTORY, name);
        // Attempt to construct the workbook via the input stream     
        return POILoader.getXLSWorkbook(is);
    }

    private static HSSFWorkbook getKnownXLSWorkbook(String name) {
        try {
            return getXLSWorkbook(name);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    public static HSSFWorkbook getXLSCoverPage() {
        return getKnownXLSWorkbook("end_item_coverpage.xls");
    }
    
    public static HSSFWorkbook getXLS2404() {
        return getKnownXLSWorkbook("DA_2404.xls");
    }

}
