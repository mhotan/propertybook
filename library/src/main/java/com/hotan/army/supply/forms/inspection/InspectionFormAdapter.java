package com.hotan.army.supply.forms.inspection;

import com.hotan.army.supply.forms.FormAdapter;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;

/**
 * An interface that defines the ability to interact with a DA 2404
 * sheet.  This allows future developers to adjust and alter XLS forms
 * with little or no changes to the functionality.
 * 
 * @author Michael Hotan, michael.hotan@gmail.com
 */
interface InspectionFormAdapter extends FormAdapter {

    Cell getOwningUnitCell(Sheet sheet);

    Cell getNameAndModelNumberCell(Sheet sheet);

    Cell getSerialNumberCell(Sheet sheet);

    Cell getMilesCell(Sheet sheet);

    Cell getHoursCell(Sheet sheet);

    Cell getDateCell(Sheet sheet);

    Cell getTMNumCell(Sheet sheet);

    Cell getTMDateCell(Sheet sheet);

    Cell getInspectionTypeCell(Sheet sheet);

    Cell getInspectorSignatureCell(Sheet sheet);

    Cell getInspectorTimeCell(Sheet sheet);
    
    CellRangeAddress getDeficiencyBlock();
}
