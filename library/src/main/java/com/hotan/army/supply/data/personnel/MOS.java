package com.hotan.army.supply.data.personnel;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Enum type that represents different types 
 * of MOS with a team.
 * 
 * @author Michael Hotan, michael.hotan@gmail.com
 *
 */
public enum MOS {

    ALPHA("18A"),
    WARRENT("18A0"),
    BRAVO("18B"),
    CHARLIE("18C"),
    DELTA("18D"),
    ECHO("18E"),
    FOX("18F"),
    ZULU("18Z")
    ;

    // Moses in the most common ordering.
    public static final ObservableList<MOS> MOSES =
            FXCollections.unmodifiableObservableList(FXCollections.observableArrayList(
                    ALPHA, WARRENT, ZULU, BRAVO, CHARLIE, DELTA, ECHO, FOX));

    private final String mName;
    
    MOS(String name) {
        mName = name;
    }
    
    @Override
    public String toString() {
        return mName;
    }

    /**
     * Returns the MOS for the string variable.
     *
     * @param mosString String representation of MOS.
     * @return The MOS with the corresponding mos string value.
     */
    public static MOS getMOS(String mosString) {
        for (MOS mos : MOSES)
            if (mos.toString().equalsIgnoreCase(mosString))
                return mos;
        return null;
    }
}
