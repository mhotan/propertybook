package com.hotan.army.supply.forms.handreceipt.subcomponent;

import com.hotan.army.supply.data.items.EndItem;
import com.hotan.army.supply.data.items.itemdata.EndItemData;
import com.hotan.army.supply.data.personnel.Operator;
import com.hotan.army.supply.forms.FormatException;
import com.hotan.army.supply.forms.Template;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.util.logging.Logger;

/**
 * A Excel Document composing of all the Unit level Sub Component hand receipt for
 * every end item in a team property book.  This document is a reference for all other sub components for
 * accountable end items.
 *
 * <b>This document is usually generated as an xls file and provided by company S4.</b>
 *
 * @author Michael Hotan, michael.hotan@gmail.com
 */
public class ComponentHandReceipt extends Template {

    private static final Logger LOG = Logger.getLogger(ComponentHandReceipt.class.getSimpleName());

    // Component labels within ComponentHandReceipt

    /**
     * Adapter for component hand receipt.
     */
    private final ComponentHandReceiptAdapter adapter;

    /**
     * A Complete list of all End Items.
     * <b>All End Items are broken down by group.</b>
     */
    private final ObservableMap<EndItemData, ObservableList<EndItem>> items;

    /**
     * Who from and to.
     */
    private final Operator whoFrom, whoTo;

    /**
     * The UIC and DESC.
     */
    private final String UIC, DESC;

    /**
     * Creates a Component Hand Receipt.
     *
     * @param wb Workbook that contains component handreceipt.
     * @throws FormatException The Workbook is not compatible with {@link com.hotan.army.supply.forms.handreceipt.subcomponent.DefaultAdapter}
     */
    public ComponentHandReceipt(HSSFWorkbook wb) throws FormatException {
        this(wb, new DefaultAdapter());
    }

    /**
     * Creates a Component Hand Receipt from an Excel Workbook.
     *
     * @param wb Workbook to use.
     * @throws FormatException Could not interpret the XLS file as Component Hand receipt.
     */
    public ComponentHandReceipt(HSSFWorkbook wb, ComponentHandReceiptAdapter adapter) throws FormatException {
        super(wb, adapter);
        this.adapter = (ComponentHandReceiptAdapter) getAdapter();

        // Create a list of items.
        items = this.adapter.processHandReceipt(wb);

        HSSFSheet sheetOne = wb.getSheetAt(0);
        if (sheetOne == null)
            throw new FormatException(getClass().getSimpleName() + "() Workbook has no sheets");

        whoFrom = this.adapter.getWhoFrom(sheetOne);
        whoTo = this.adapter.getWhoTo(sheetOne);
        UIC = this.adapter.getUIC(sheetOne);
        DESC = this.adapter.getDESC(sheetOne);
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    ////    Getters
    ////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Get who the Sub Component Hand reciept is signed from.
     * @return Who the Hand Receipt is signed from.
     */
    public Operator getWhoFrom() {
        return whoFrom;
    }

    /**
     * Get who the sub component hand receipt is signed to.
     * @return Operator who the hand receipt is signed to.
     */
    public Operator getWhoTo() {
        return whoTo;
    }

    /**
     * Get the Unit Identification Code.
     *
     * @return the UIC as a String
     */
    public String getUIC() {
        return UIC;
    }

    /**
     * Get the Unit description.
     *
     * @return the Description of the unit.
     */
    public String getDESC() {
        return DESC;
    }

    /**
     * Items that are contained in this group.  The list returned is not modifiable.
     *
     * @return A list of all the End Item Groups
     */
    public ObservableMap<EndItemData, ObservableList<EndItem>> getItems() {
        return FXCollections.unmodifiableObservableMap(items);
    }

    /**
     * Checks if there is an EndItem Group with matcheing nsn and lin.
     *
     * @param nsn NSN of the EndItem
     * @param lin Lin number of the EndItem
     * @return Whether or not there exist an EndItem group with the same nsn and lin
     */
    public boolean hasGroup(String nsn, String lin) {
        return getGroup(nsn, lin) != null;
    }

    /**
     * Returns EndItem group that has the same nsn and lin number. 
     *
     * @param nsn NSN to find.
     * @param lin LIN to find.
     * @return EndItemGroup that represents the the nsn and lin inputted, or null if non are found.
     */
    public EndItemData getGroup(String nsn, String lin) {
        if (nsn == null)
            throw new NullPointerException("Null NSN");
        if (lin == null)
            throw new NullPointerException("Null LIN");

        return items.keySet().stream().filter(
                tmp -> tmp.getLIN().equals(lin) &&
                        tmp.getNSN().equals(nsn)).findFirst().orElse(null);
    }

}
