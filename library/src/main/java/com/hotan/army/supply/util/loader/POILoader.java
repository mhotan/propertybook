package com.hotan.army.supply.util.loader;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.*;

/**
 * Class that manages the general loading of XLS and XLSX files.
 * <br> This loads files from itemdata subdirectory from the res directory.
 * 
 * @author Michael Hotan, michael.hotan@gmail.com
 */
public class POILoader {

    private static final String DIRECTORY = "data";

    private POILoader() {}

    /**
     * Makes sure that input file is the correct format for an HSSFWorkbook.
     * <b>Returns the workbook on success.</b>
     *
     * @param xlsFile The Workbook to use.
     * @return The Workbook in the correct format.
     * @throws java.io.IOException An error occured accessing the file to create the workbook.
     */
    public static HSSFWorkbook getXLSWorkbook(File xlsFile) throws IOException {
        if (!xlsFile.getName().endsWith(".xls"))
            throw new IOException("File " + xlsFile + " is not a excel file.  Look for files " +
                    "ending with \'.xls\'.");
        return new HSSFWorkbook(new FileInputStream(xlsFile));
    }

    /**
     * Returns workbook from stream.
     *
     * @param stream Stream containing workbook.
     * @return Workbook found from stream
     */
    public static HSSFWorkbook getXLSWorkbook(InputStream stream) throws IOException {
        if (stream == null)
            throw new IllegalArgumentException("Invalid Stream " + stream);
        return new HSSFWorkbook(stream);
    }

    /**
     * Returns the XLS Workbook with the associated name.  The file is already
     *
     * @param name Name of the file that is in
     * @return The Workbook to load
     * @throws java.io.IOException The Exception
     */
    public static HSSFWorkbook getXLSWorkbook(String name) throws IOException {
        // Validate the name.
        if (name == null)
            throw new IllegalArgumentException("Illegal name of XLS to load: " + name);

        // Creates an input stream for this
        InputStream stream = BaseLoader.stream(DIRECTORY, name);
        return getXLSWorkbook(stream);
    }

}
