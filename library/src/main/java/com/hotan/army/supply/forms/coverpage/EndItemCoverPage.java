package com.hotan.army.supply.forms.coverpage;

import com.hotan.army.supply.data.items.EndItem;
import com.hotan.army.supply.data.items.itemdata.EndItemData;
import com.hotan.army.supply.data.location.LocatedAt;
import com.hotan.army.supply.data.personnel.MOS;
import com.hotan.army.supply.forms.FormatException;
import com.hotan.army.supply.forms.Template;
import com.hotan.army.supply.util.POIUtil;
import com.hotan.army.supply.util.POIUtil.IndexPair;
import com.hotan.army.supply.util.loader.POITemplateLoader;
import javafx.collections.ObservableList;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

/**
 * A class that provides access to a general cover page for a particular end item.
 *
 * @author Michael Hotan, michael.hotan@gmail.com
 */
public class EndItemCoverPage extends Template {

    /**
     * Logging tag.
     */
    private static final Logger LOG = Logger.getLogger(EndItemCoverPage.class.getSimpleName());

    private static final CoverPageAdapter DEFAULT_ADAPTER = new DefaultAdapter();
    
    // NOTE: The cover page currently only supports maximum
    // of 24 items per page.

    /**
     * Reference to internal worksheet.
     */
    private final HSSFSheet mSheet;

    /**
     * Adapter that correctly extracts the cell.
     */
    private final CoverPageAdapter adapter;
    
    /**
     * Current cell to place
     */
    private IndexPair mCurrIndex;

    /**
     * Creates a cover page from the default template.
     * 
     * @throws FormatException Default Adapter is not compatible with Cover Page.
     */
    public EndItemCoverPage() throws FormatException {
        this(POITemplateLoader.getXLSCoverPage(), DEFAULT_ADAPTER);
    }

    /**
     * Loads a template for the cover page to store as XLS
     * defined by outputFile.
     * 
     * @param form Current Cover Page Form.
     * @param adapter Adapter used to access cells for the form.
     * @throws FormatException If the adapter is misformated for the template.
     */
    public EndItemCoverPage(HSSFWorkbook form, CoverPageAdapter adapter) throws FormatException {
        super(form, adapter);
        if (adapter == null)
            throw new NullPointerException(getClass().getSimpleName() + "(), CoverPageAdapter cannot be null");
        this.adapter = (CoverPageAdapter) getAdapter();

        // The first sheet is the primary sheet.
        // No other sheet is used.
        mSheet = form.getSheetAt(0);

        // Set the initial index for the serial number block.
        CellRangeAddress range = adapter.getSNRangeAddress(mSheet);
        mCurrIndex = IndexPair.valueOf(range.getFirstRow(), range.getFirstColumn());
        checkRep();
    }

    /**
     * Sets the group of EndItems to Display.
     * 
     * @param group Group of EndItems to present.
     */
    public void setData(EndItemData group, ObservableList<EndItem> items) {

        // Set all the reciprocated itemdata points.
        setName(group.getDefaultName());
        setLIN(group.getLIN());
        setNSN(group.getNSN());
        setMOS(group.getAssignedMOS());

        // Extract a complete list of end items.
        List<EndItem> itemLst = items;
        setQuantity(itemLst.size());

        Set<String> locations = new HashSet<String>();
        // Attempt to set the location of all the items.
        for (EndItem item : itemLst) {
            LocatedAt location = item.getLocation();
            if (location == null) continue;
            locations.add(location.getLocation().getName());
        }
        StringBuffer buf = new StringBuffer();
        List<String> locList = new ArrayList<String>(locations);
        for (int i = 0; i < locList.size(); ++i) {
            buf.append(locList.get(i));
            if (i != locList.size() - 1) {
                buf.append("; ");
            }
        }
        setLocation(buf.toString());

        for (EndItem item : itemLst) {
            try {
                addSerialNumber(item.getName());
            } catch (FormatException e) {
                LOG.warning("Error while adding serial number");
            }
        }
    }

    /**
     * Sets the name of this cover page
     * 
     * @param name Name of the End Item.
     */
    private void setName(String name) {
        if (name == null) return;
        adapter.getNameCell(mSheet).setCellValue(name);
    }

    /**
     * Set the authorized quanttiy of this end item.
     * @param qty Authorized Quantity.
     */
    private void setQuantity(int qty) {
        adapter.getQuantityCell(mSheet).setCellValue(qty);
    }

    /**
     * Sets the LIN number.
     * @param lin Lin number
     */
    private void setLIN(String lin) {
        if (lin == null) return;
        adapter.getLINCell(mSheet).setCellValue(lin);
    }

    /**
     * Sets the NSN number of this number.
     * @param nsn National Stock Number.
     */
    private void setNSN(String nsn) {
        if (nsn == null) return;
        adapter.getNSNCell(mSheet).setCellValue(nsn);
    }

    /**
     * Sets the MOS.
     * @param mos MOS of this End Item.
     */
    private void setMOS(MOS mos) {
        if (mos == null) return;
        adapter.getMOSCell(mSheet).setCellValue(mos.toString());
    }

    /**
     * Sets the location description.
     * @param location Location of the End Item.
     */
    private void setLocation(String location) {
        if (location == null) return;
        adapter.getLocationCell(mSheet).setCellValue(location);
    }

    /**
     *
     * @param serialNumber
     * @throws FormatException
     */
    private void addSerialNumber(String serialNumber) throws FormatException {
        if (serialNumber == null) return;
        Cell currCell = POIUtil.getCell(mSheet, mCurrIndex);
        if (currCell == null) 
            throw new FormatException("Unable to find cell at " + mCurrIndex);
        currCell.setCellValue(serialNumber);
        
        try {
            incrementSerialCell();
        } catch (FormatException e) {
            throw new FormatException("addSerialNumber() " + e.getMessage());
        }
    }

    /**
     * Sets the overall picture to the path found in the argument.
     * 
     * @param image Image file
     * @throws java.io.IOException Error occurred while loading the image.
     */
    public void setOverallPic(File image) throws IOException {
        Cell cell = adapter.getOverallPicCell(mSheet);
        if (cell == null) return;
        addImageToCell(image, cell);
    }
    
    /**
     * Sets the serial number picture to the path found in the argument.
     * 
     * @param image Image file
     * @throws java.io.IOException Error occurred while loading the image.
     */
    public void setSNImage(File image) throws IOException {
        Cell cell = adapter.getSNPicCell(mSheet);
        if (cell == null) return;
        addImageToCell(image, cell);
    }
    
    /**
     * Sets the BII picture to the path found in the argument.
     * 
     * @param imagePath Image path of the picture
     * @throws java.io.IOException Error occurred while loading the image.
     */
    public void setBIIImage(File imagePath) throws IOException {
        Cell cell = adapter.getBIIPicCell(mSheet);
        if (cell == null) return;
        addImageToCell(imagePath, cell);
    }
    
    /**
     * Returns the current sheet with the itemdata presented
     */
    public HSSFSheet getSheet() {
        return mSheet;
    }

    /**
     *
     * @param image
     * @param cell
     * @throws IOException
     */
    private void addImageToCell(File image, Cell cell) throws IOException {
        assert cell != null: "Cell cannot be null";
        if (image == null)
            throw new NullPointerException("Null image path");
        Workbook wb = getWorkbook();
        
        int type = -1;
        String nameCpy = image.getName().toLowerCase().trim();
        if (nameCpy.endsWith(".jpg") || nameCpy.endsWith(".jpeg"))
            type = Workbook.PICTURE_TYPE_JPEG;
        if (nameCpy.endsWith(".bmp") || nameCpy.endsWith(".dib"))
            type = Workbook.PICTURE_TYPE_DIB;
        if (nameCpy.endsWith(".png"))
            type = Workbook.PICTURE_TYPE_PNG;
        if (type == -1) {
            throw new IllegalArgumentException("Unsurpported type " + image.getAbsolutePath());
        }
        
        // Load the image and store it into the workbook.
        InputStream is = new FileInputStream(image);
        byte[] bytes = IOUtils.toByteArray(is);
        int pictureIdx = wb.addPicture(bytes, type);
        is.close();
        
        // Place the image in the cell provided.
        CreationHelper helper = wb.getCreationHelper();
        
        // Create the drawing patriarch.  This is the top level container for all shapes. 
        Drawing drawing = mSheet.createDrawingPatriarch();
        
        ClientAnchor anchor = helper.createClientAnchor();
        //set top-left corner of the picture,
        //subsequent call of Picture#resize() will operate relative to it
        anchor.setCol1(cell.getColumnIndex());
        anchor.setRow1(cell.getRowIndex());
        Picture pict = drawing.createPicture(anchor, pictureIdx);

        //auto-size picture relative to its top-left corner
        pict.resize();
    }

    /**
     * Increments to the next cell for serial number.
     * @throws FormatException There is no available cell.
     */
    private void incrementSerialCell() throws FormatException {
        IndexPair pair = mCurrIndex.indexOnRight();
        CellRangeAddress range = adapter.getSNRangeAddress(mSheet);
        if (!range.isInRange(pair.row, pair.col)) {
            // Go to the next element on the first row.
            pair = IndexPair.valueOf(pair.row + 1, range.getFirstColumn());
            if (!range.isInRange(pair.row, pair.col))
                throw new FormatException("Ran out of cells");
        }
        mCurrIndex = pair;
    }

    /**
     * Representation checker.
     */
    private void checkRep() {
        assert mSheet != null: "Workbook not found";
        assert adapter != null: "Adapter Cannot be null.";
    }
    
}
