package com.hotan.army.supply.data.parse;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import org.parse4j.ParseException;
import org.parse4j.ParseQuery;
import org.parse4j.callback.CountCallback;
import org.parse4j.callback.FindCallback;
import org.parse4j.callback.GetCallback;

import java.util.List;

/**
 * Query that uses javafx save background threads.
 *
 * Created by mhotan on 3/21/14.
 */
public class FXParseQuery<T extends FXParseObject> extends ParseQuery<T> {

    private final FXParseQuery<T> This;

    public FXParseQuery(Class<T> subclass) {
        super(subclass);
        This = this;
    }

    public FXParseQuery(String theClassName) {
        super(theClassName);
        This = this;
    }

    @Override
    public void getInBackground(String objectId, GetCallback<T> callback) {
        final GetInBackgroundService service = new GetInBackgroundService(objectId);

        if (callback != null) {
            service.setOnSucceeded(e -> callback.done((T) e.getSource().getValue(), null));
            service.setOnFailed(e -> {
                Throwable t = e.getSource().getException();
                assert t != null : "There was a failure but no exception thrown.";

                // Notify via callback.
                if (t instanceof ParseException)
                    callback.done(null, (ParseException) t);
                callback.done(null, new ParseException(t));
            });
        }

        service.start();
    }

    @Override
    public void findInBackground(FindCallback<T> callback) {
        final FindInBackgroundService service = new FindInBackgroundService();

        if (callback != null) {
            service.setOnSucceeded(e -> callback.done((List<T>) e.getSource().getValue(), null));
            service.setOnFailed(e -> {
                Throwable t = e.getSource().getException();
                assert t != null : "There was a failure but no exception thrown.";

                // Notify via callback.
                if (t instanceof ParseException)
                    callback.done(null, (ParseException) t);
                callback.done(null, new ParseException(t));
            });
        }

        service.start();
    }

    @Override
    public void countInBackground(CountCallback callback) {
        final CountService service = new CountService();

        if (callback != null) {
            service.setOnSucceeded(e -> callback.done((Integer) e.getSource().getValue(), null));
            service.setOnFailed(e -> {
                Throwable t = e.getSource().getException();
                assert t != null : "There was a failure but no exception thrown.";

                // Notify via callback.
                if (t instanceof ParseException)
                    callback.done(null, (ParseException) t);
                callback.done(null, new ParseException(t));
            });
        }

        service.start();
    }

    private class CountService extends Service<Integer> {

        @Override
        protected Task<Integer> createTask() {
            return new Task<Integer>() {
                @Override
                protected Integer call() throws Exception {
                    return This.count();
                }
            };
        }
    }

    private class GetInBackgroundService extends Service<T> {

        private final StringProperty objectId;

        private GetInBackgroundService(String objectId) {
            this.objectId = new SimpleStringProperty();
        }

        public String getObjectId() {
            return objectId.get();
        }

        public StringProperty objectIdProperty() {
            return objectId;
        }

        @Override
        protected Task<T> createTask() {
            final String objectId = getObjectId();
            return new Task<T>() {
                @Override
                protected T call() throws Exception {
                    return This.get(objectId);
                }
            };
        }
    }

    private class FindInBackgroundService extends Service<List<T>> {

        @Override
        protected Task<List<T>> createTask() {
            return new Task<List<T>>() {
                @Override
                protected List<T> call() throws Exception {
                    return This.find();
                }
            };
        }
    }
}
