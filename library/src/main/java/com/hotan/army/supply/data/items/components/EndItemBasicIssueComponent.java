package com.hotan.army.supply.data.items.components;


import com.hotan.army.supply.data.Mergeable;
import org.parse4j.ParseClassName;

/**
 * BII Component.
 * 
 * @author Michael Hotan, michael.hotan@gmail.com
 */
@ParseClassName("BIIComponent")
public class EndItemBasicIssueComponent extends AccountableComponent implements Mergeable<EndItemBasicIssueComponent> {

    public EndItemBasicIssueComponent() {
        super();
    }

    public EndItemBasicIssueComponent(String name, String nsn) {
        super(name, nsn);
    }
    
    public EndItemBasicIssueComponent(String name, String nsn, int qty) {
        super(name, nsn, qty);
    }


    // Potentially add specific BII functionality.


    @Override
    public void merge(EndItemBasicIssueComponent item) {

    }
}
