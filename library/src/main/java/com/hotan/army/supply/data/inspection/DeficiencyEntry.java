package com.hotan.army.supply.data.inspection;

import com.hotan.army.supply.data.parse.FXParseObject;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.util.StringConverter;
import org.parse4j.ParseClassName;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * A mutable class that represents a DA 2404 Deficiency entry.  A deficiency entry is defined
 * by the actual string representation of the deficiency, Date it was logged, and the
 * TM Item number.
 *  
 * @author Michael Hotan, michael.hotan@gmail.com
 */
@ParseClassName("DeficiencyEntry")
public class DeficiencyEntry extends FXParseObject {

    /*
    Representation Invariant:
    tmItemNum != null
    deficiency != null && deficiency is not empty
    dateTime != null
    correctiveAction != null
    status != null

    Abstract Function:
    tmItemNum the TM Number of the item that has a deficiency.
    deficiency What exactly is wrong with the item.
    correctiveAction what actions were taken to fix the item.
    status current status of the end item.
    */

    /**
     * Status of the deficiency.
     *
     * @author Michael Hotan, michael.hotan@gmail.com
     */
    public enum STATUS {
        OK("", "OK"),
        INOP("X", "Inoperable"),
        REQUIRES_INSPECTION("-", "Requires Inspection"),
        MATERIAL_DEFECT("/", "Material Defect"),
        UNKNOWN("?", "Unknown");

        private final String symbol, readable;

        /**
         * Creates a Status enumerated type for deficiency.
         *
         * @param canonical Canonical form the of the status.
         */
        private STATUS(String canonical, String readableStr) {
            this.symbol = canonical;
            this.readable = readableStr;
        }

        @Override
        public String toString() {
            return getReadable();
        }

        public String getSymbol() {
            return symbol;
        }

        public String getReadable() {
            return readable;
        }

        // List of all available statuses.
        public static final ObservableList<STATUS> STATUSES =
                FXCollections.unmodifiableObservableList(
                        FXCollections.observableArrayList(OK, INOP, REQUIRES_INSPECTION, MATERIAL_DEFECT));

        /**
         * Returns the correct Enum status that is represented by argument string.  That is
         * for STATUS s, s.toString().equals(statusStr) then s is returned else return null.
         *
         * @param symbol The string representation of the status
         * @return Status that is represented by input string.
         */
        public static STATUS getStatus(String symbol) {
            for (STATUS status: STATUSES) {
                if (status.getSymbol().equalsIgnoreCase(symbol))
                    return status;
            }
            return UNKNOWN;
        }

        public static StringConverter<STATUS> STRING_CONVERTER = new StatusStringConverter();

        private static class StatusStringConverter extends StringConverter<STATUS> {

            @Override
            public String toString(STATUS status) {
                return status.getSymbol();
            }

            @Override
            public STATUS fromString(String symbol) {
                return getStatus(symbol);
            }
        }
    }

    /**
     * Necessary blocks for deficiency entry for a 2404.
     */
    private final StringProperty tmItemNum, deficiency, correctiveAction;

    /**
     * The status block for the 2404 entry.
     */
    private final ObjectProperty<STATUS> status;

    /**
     * The dateTime the deficiency was entered.
     */
    private final ObjectProperty<LocalDateTime> dateTime;

    /**
     * The date property of this that is bound to Local Date Time.
     */
    private final ObjectProperty<LocalDate> date;

    /**
     * The string representation of the Date the deficiency occured.
     */
    private final StringBinding dateString;

    /**
     * Do not use.  This is just implemented for Parse purposes.
     */
    public DeficiencyEntry() {
        tmItemNum = new SimpleStringProperty();
        deficiency = new SimpleStringProperty();
        correctiveAction = new SimpleStringProperty();
        status = new SimpleObjectProperty<>();
        dateTime = new SimpleObjectProperty<>();
        date = new SimpleObjectProperty<>();

        // Create the helper binding
        dateString = Bindings.createStringBinding(() -> {
            if (dateTime.isNull().get()) {
                return "";
            } else
                return dateTime.get().format(DateTimeFormatter.ISO_LOCAL_DATE);
        } , dateTime);

        // Add listeners to update the Parse Cloud itemdata.
        // NOTE: Data is not pushed to the cloud it is just set locally.

        // Add the listeners for the all the Primitive type components
        tmItemNum.addListener((ov, oldVal, newVal) -> {
            assert newVal != null: "TM Item Number cannot be null";
            put(TM_ITEM_NUMBER, newVal);
            // Cannot be null TM Item number.
        });
        deficiency.addListener((ov, oldVal, newVal) -> {
            assert newVal != null: "Deficiency cannot be null";
            put(DEFICIENCY, newVal);
        });
        correctiveAction.addListener((ov, oldVal, newVal) -> {
            if (newVal != null)
                put(CORRECTIVE_ACTION, newVal);
            else
                remove(CORRECTIVE_ACTION);
        });
        // Add a listener for items that can be null.
        // if they are null then
        status.addListener((ov, oldVal, newVal) -> {
            // If the new value is equal to ok then place make sure
            // the deficiency is bound to the dateTime as a string.
            if (newVal.equals(STATUS.OK))
                this.deficiency.bind(dateString);
            else
                this.deficiency.unbind();

            put(STATUS_OF_ITEM, newVal.getSymbol());
            // Status can never be null
        });
        dateTime.addListener((ov, oldVal, newVal) -> {
            assert newVal != null : "Date cannot be null";
            // Add a listener for this
            date.set(newVal.toLocalDate());

            put(DATE_LOGGED, Date.from(newVal.atZone(
                    ZoneId.systemDefault()).toInstant()).getTime());
        });
    }

    /**
     * Creates a deficiency for an end item part.
     *
     * @param tmNum TM Number of EndItem part.
     * @param deficiency Written text deficiency.
     * @param corActions Corrective action.
     * @param status Status of the deficiency. Cannot be Null
     */
    public DeficiencyEntry(String tmNum, String deficiency, String corActions, STATUS status) {
        this(tmNum, deficiency, corActions, status, LocalDateTime.now());
    }

    /**
     * Creates a deficiency for an end item part with an associated dateTime.
     *
     * @param tmNum TM Number of EndItem part Cannot be null.
     * @param deficiency Written text deficiency. Cannot be null or empty.
     * @param corActions Corrective Actions
     * @param status Status of the entry.
     * @param dateTime Date of the action, Cannot be null.
     */
    public DeficiencyEntry(
            String tmNum,
            String deficiency,
            String corActions,
            STATUS status,
            LocalDateTime dateTime) {
        this();
        // Do Primary Key Pre condition checks.

        if (status == null)
            throw new IllegalArgumentException(getClass().getSimpleName() + "() Illegal Status \"null\"");
        if (deficiency == null || deficiency.trim().isEmpty() && !status.equals(STATUS.OK))
            throw new IllegalArgumentException(
                getClass().getSimpleName() + "() Illegal Deficiency \"null\"");
        if (dateTime == null)
            throw new IllegalArgumentException(getClass().getSimpleName() + "()" +
                    " Illegal Date \"null\"");

        // Correct improper input
        if (tmNum == null) tmNum = "";
        if (corActions == null) corActions = "";

        this.tmItemNum.set(tmNum.trim());
        this.deficiency.set(deficiency.trim());
        this.dateTime.set(dateTime);

        setCorrectiveAction(corActions.trim());
        setStatus(status);
    }

    private static final String TM_ITEM_NUMBER = "tm";
    private static final String DEFICIENCY = "deficiency";
    private static final String CORRECTIVE_ACTION = "correctiveAction";
    private static final String STATUS_OF_ITEM = "status";
    private static final String DATE_LOGGED = "dateLogged";

    /////////////////////////////////////////////////////////////////////////////////
    //// Setters
    /////////////////////////////////////////////////////////////////////////////////

    /**
     * Set the deficiency label of this.
     * <b>Null values are ignored.
     * @param deficiency
     */
    public void setDeficiency(String deficiency) {
        if (deficiency == null) return;
        this.deficiency.set(deficiency);
    }

    public void setTmItemNum(String tmItemNum) {
        if (tmItemNum == null) return;
        this.tmItemNum.set(tmItemNum);
    }

    /**
     * Set the status of this deficiency entry.
     *
     * @param status Status to set to this deficiency entry.
     */
    public void setStatus(STATUS status) {
        // Check for illegal arguments
        if (status == null) return;
        this.status.set(status);
    }

    /**
     * Set the corrective action for this end item.
     *
     * @param correctiveAction Action taking on this item.
     */
    public void setCorrectiveAction(String correctiveAction) {
        if (correctiveAction == null) return;
        this.correctiveAction.set(correctiveAction);
    }

    /**
     * Returns the current status of this.
     *
     * @return Return the status of the Deficiency.
     */
    public STATUS getStatus() {
        return statusProperty().get();
    }

    /**
     * Returns the current end item of this.
     *
     * @return TM Item number of th end item.
     */
    public String getTmItemNum() {
        return tmItemNumProperty().get();
    }

    /**
     * Returns the Text representation of the deficiency.
     *
     * @return The written deficiency of this.
     */
    public String getDeficiency() {
        return deficiencyProperty().get();
    }

    /**
     * Returns the current corrective action taken on the deficiency entry.
     *
     * @return The corrective action taken
     */
    public String getCorrectiveAction() {
        return correctiveActionProperty().get();
    }

    /**
     * The local date.
     *
     * @return The date of this entry
     */
    public LocalDate getDate() {
        return dateProperty().get();
    }

    /**
     * @return The LocalDate Read only object property.
     */
    public ReadOnlyObjectProperty<LocalDate> dateProperty() {
        return date;
    }

    /**
     * Returns the current dateTime time of this deficiency entry.
     *
     * @return Returns when the corrective action was logged
     */
    public LocalDateTime getDateTime() {
        return dateTimeProperty().get();
    }

    public StringProperty tmItemNumProperty() {
        if (tmItemNum.isNull().get()) {
            tmItemNum.set(getString(TM_ITEM_NUMBER));
        }
        return tmItemNum;
    }

    public StringProperty deficiencyProperty() {
        if (deficiency.isNull().get())
            deficiency.set(getString(DEFICIENCY));
        return deficiency;
    }

    public StringProperty correctiveActionProperty() {
        if (correctiveAction.isNull().get())
            correctiveAction.set(getString(CORRECTIVE_ACTION));
        return correctiveAction;
    }

    public ReadOnlyObjectProperty<LocalDateTime> dateTimeProperty() {
        if (dateTime.isNull().get()) {
            long time = getLong(DATE_LOGGED);
            if (time == 0L) {
                this.dateTime.set(LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault()));
            }
        }
        return dateTime;
    }

    public ObjectProperty<STATUS> statusProperty() {
        if (status.isNull().get())
            status.set(STATUS.getStatus(getString(STATUS_OF_ITEM)));
        return status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DeficiencyEntry)) return false;
        DeficiencyEntry that = (DeficiencyEntry) o;
        if (!dateTime.isEqualTo(that.dateTime).get()) return false;
        return true;
    }

    @Override
    public int hashCode() {
        return getDateTime().hashCode();
    }

    @Override
    public String toString() {
        return "DeficiencyEntry{" +
                "tmItemNum='" + tmItemNum + '\'' +
                ", deficiency='" + deficiency + '\'' +
                ", correctiveAction='" + correctiveAction + '\'' +
                ", status=" + status +
                '}';
    }
    
}
