package com.hotan.army.supply.util.loader;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by mhotan_dev on 1/11/14.
 */
public class BaseLoader {

    private BaseLoader() {}

    /**
     * Creates input stream with the name.
     *
     * @param directory directory where the file is stored.
     * @param fileName File to get stream.
     * @return The Input Stream comprised of the file
     */
    public static InputStream stream(String directory, String fileName) throws FileNotFoundException {
        String completePath = directory + "/" + fileName;
        InputStream s = BaseLoader.class.getClassLoader().getResourceAsStream(completePath);
        if (s == null)
            throw new FileNotFoundException("No file named: " + fileName + " found in dir: " + directory);
        return s;
    }

    /**
     * Return the URL representation of the file found in the directory from the resource folder.
     *
     * @param directory Directory in the resource folder.
     * @param fileName name of file within the directory.
     * @return The URL of the file within the directory.
     * @throws FileNotFoundException
     */
    public static URL getURL(String directory, String fileName) throws FileNotFoundException {
        String completePath = directory + "/" + fileName;
        URL url = BaseLoader.class.getClassLoader().getResource(completePath);
        if (url == null)
            throw new FileNotFoundException("No file named: " + fileName + " found in dir: " + directory);
        return url;
    }
}
