package com.hotan.army.supply.data.parse;

import javafx.beans.property.SimpleObjectProperty;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import org.parse4j.ParseException;
import org.parse4j.ParseObject;
import org.parse4j.ParseQuery;
import org.parse4j.callback.FindCallback;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by mhotan on 3/11/14.
 */
public class ParseUtil {

    /**
     *
     */
    private static final Logger LOG = Logger.getLogger(ParseUtil.class.getSimpleName());

    /**
     * Hidden constructor.
     */
    private ParseUtil() {}

//    /**
//     * Returns the Parse Object from the argument FXParseObject.
//     * @param FXParseObject FXParseObject to turn into ParseObject
//     * @return null if FXParseObject is null, else the parse object that represents the FXParseObject
//     */
//    public static ParseObject toParseObject(FXParseObject FXParseObject) {
//        if (FXParseObject == null) return null;
//        return FXParseObject.packObject();
//    }
//
//    /**
//     * Given a storable class definition produce an instance of the Class based
//     * off the ParseObject.
//     *
//     * @param <T> object extending FXParseObject
//     * @param clazz Class to instantiate
//     * @param po Parse object representation of the Class
//     * @return null on if po is null, instance of T otherwise
//     */
//    public static <T extends FXParseObject> T toStorable(Class<T> clazz, ParseObject po) {
//        if (po == null) {
//            LOG.warning("toStorable(): Null parse object for " +
//                    clazz.getSimpleName() + " input returning null");
//            return null;
//        }
//
//        // Use the constructor for the class that takes a singe parse Object.
//        // This is mandated in the Abstract class of FXParseObject.
//        try {
//            // Check if we need to fetch the itemdata first.
//            Date date = po.getCreatedAt();
//            if (date == null)
//                po.fetchIfNeeded();
//
//            Constructor<T> ctor  = clazz.getConstructor(ParseObject.class);
//            T object = ctor.newInstance(new Object[] {po});
//            return object;
//        } catch (Exception e) {
//            throw new IllegalArgumentException(e);
//        }
//    }
//
//    /**
//     * Retrieves a list of parse objects that represent storable list.
//     *
//     * @requires storables not equal null
//     * @param storables list of storable objects to obtain parse objects from
//     * @return List of ParseObject that represents storables
//     */
//    public static List<ParseObject> toListOfParseObjects(List<? extends FXParseObject> storables) {
//        List<ParseObject> pObjects = new ArrayList<ParseObject>(storables.size());
//        for (FXParseObject s: storables) {
//            pObjects.add(s.packObject());
//        }
//        return pObjects;
//    }
//
//    /**
//     * Retrieves a list of Class instances from a associated relation of the parseObjects.
//     * NOTE: All items in objects must have dynamic types of ParseObject or a class cast parseException
//     * will be thrown
//     *
//     * @param <T> object extending FXParseObject
//     * @param clazz Class definition of the particular instance
//     * @param objects List of Objects that must have dynamic tyoes if ParseObjects
//     * @return List of storables from ParesObjects
//     */
//    public static <T extends FXParseObject> List<T> toListOfStorables(
//            Class<T> clazz, List<ParseObject> objects) {
//        List<T> storables = new ArrayList<T>(objects.size());
//        for (ParseObject o: objects)
//            storables.add(toStorable(clazz, o));
//        return storables;
//    }
//
//    /**
//     * ParseQueries can be used to obtain current instances of a particular
//     * FXParseObject class that exist in the cloud
//     *
//     * @param clazz FXParseObject class to get query for.
//     * @return A simple query for a particular storable class.
//     */
//    public static ParseQuery<ParseObject> getQueryForClass(Class<? extends FXParseObject> clazz) {
//        return ParseQuery.getQuery(clazz.getSimpleName());
//    }

//    /**
//     * Saves all the ParseObjects on the current thread
//     *
//     * @param objects Objects to save on the current thread.
//     * @throws ParseException Unable to save on the current thread.
//     */
//    public static <T extends FXParseObject> void saveOnCurrentThread(T... objects) throws ParseException {
//        for (FXParseObject s: objects)
//            if (s != null)
//                s.saveOnCurrentThread();
//    }
//
//    /**
//     * Saves all the ParseObjects on the current thread
//     *
//     * @param storables Saves the list of storables
//     * @throws ParseException Unable to save on the current thread.
//     */
//    public static void saveOnCurrentThread(Collection<? extends FXParseObject>... storables)
//            throws ParseException {
//        for (Collection<? extends FXParseObject> collection: storables)
//            if (collection != null)
//                for (FXParseObject s: collection)
//                    if (s != null)
//                        s.saveOnCurrentThread();
//    }
//
//    /**
//     * Deletes all the elements in all the collections from the Cloud.
//     *
//     * @param storables Storables to delete
//     * @throws ParseException
//     */
//    public static void deleteOnCurrentThread(Collection<? extends FXParseObject>... storables)
//            throws ParseException {
//        for (Collection<? extends FXParseObject> collection: storables)
//            if (collection != null)
//                for (FXParseObject s: collection)
//                    if (s != null)
//                        s.deleteOnCurrentThread();
//    }
//
//    /**
//     * Delete all the input arguments.
//     *
//     * @param objects Objects to delete from the cloud.
//     * @param <T> FXParseObject objects to delete from the cloud.
//     * @throws ParseException
//     */
//    public static <T extends FXParseObject> void deleteOnCurrentThread(T... objects) throws ParseException {
//        for (FXParseObject s: objects)
//            if (s != null)
//                s.deleteOnCurrentThread();
//    }
//
//

//    /**
//     * Deletes a storable in a background thread.
//     *
//     * @param object Object to delete in the background.
//     * @param callback Callback to notify.
//     */
//    public static void delete(FXParseObject object, final DeleteCallback callback) {
//        // Create the service
//        final DeleteService service = new DeleteService();
//
//        service.setStorable(object);
//
//        if (callback != null) {
//            service.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
//                @Override
//                public void handle(WorkerStateEvent workerStateEvent) {
//                    callback.done(null);
//                }
//            });
//
//            // Set what to do when the save fails.
//            service.setOnFailed(new EventHandler<WorkerStateEvent>() {
//                @Override
//                public void handle(WorkerStateEvent e) {
//                    Throwable t = e.getSource().getException();
//                    assert t != null : "There was a failure but no exception thrown.";
//
//                    if (t instanceof ParseException)
//                        callback.done((ParseException) t);
//                    callback.done(new ParseException(t));
//                }
//            });
//        }
//        service.start();
//    }
//
//    /**
//     * Save the parse object in a javaFX suitable background thread.  On a successful save
//     * the save callback is called with a null Exception argument.
//     *
//     * @param object object to save in the background.
//     * @param saveCallback Callback to call when the save succeeds or fails
//     */
//    public static void save(FXParseObject object, final SaveCallback saveCallback) {
//        if (object == null) return;
//
//        // Create a service instance
//        final SaveService service = new SaveService();
//
//        // set the object to save.
//        service.setStorable(object);
//
//        if (saveCallback != null) {
//            // Set what to do when the save succeeds
//            service.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
//                @Override
//                public void handle(WorkerStateEvent workerStateEvent) {
//                    saveCallback.done(null);
//                }
//            });
//
//            // Set what to do when the save fails.
//            service.setOnFailed(new EventHandler<WorkerStateEvent>() {
//                @Override
//                public void handle(WorkerStateEvent e) {
//                    Throwable t = e.getSource().getException();
//                    assert t != null : "There was a failure but no exception thrown.";
//
//                    if (t instanceof ParseException)
//                        saveCallback.done((ParseException) t);
//                    saveCallback.done(new ParseException(t));
//                }
//            });
//        }
//        service.start();
//    }

    public static <T> void put(ParseObject po, String key, List<T> values) {
        if (po == null || key == null) throw new NullPointerException();

        // Ignore null values.
        if (values == null) return;

        List<T> list = po.getList(key);
        if (list == null) {
            po.put(key, values);
            return;
        }

        boolean dirty = false;
        for (T value: values) {
            if (list.contains(value)) continue;

            list.add(value);
            dirty |= true;
        }

        if (dirty)
            po.put(key, list);
    }

    /**
     * Wrapper around putting a value in a parse object.  This restricts the
     *
     * @param po Parse Object to put value in.
     * @param key KEy to use
     * @param value Value to use.
     */
    public static void put(ParseObject po, String key, Object value) {
        if (po == null || key == null) throw new NullPointerException();

        // Ignore null values.
        if (value == null) return;

        Object current = po.get(key);
        if (current == null || !value.equals(current)) {
            // makes the object dirty at this point.
            po.put(key, value);
        }
    }

    /**
     * Executes the query in the background.  On success the returns the list of ParseObject
     *
     * @param query Query to execute in the background.
     * @param callback callback to notify when the query is complete.
     */
    public static <T extends ParseObject> void query(ParseQuery<T> query, final FindCallback<T> callback) {
        // Create a service just for the query.
        final QueryService service = new QueryService();

        // Set the query object for this service to execute.
        service.setQuery(query);

        // Set what happens when the service succeeds
        service.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent e) {
                // Notify success via callback.
                List<T> objects = (List<T>) e.getSource().getValue();
                callback.done(objects, null);
            }
        });

        // set what happens when the service fails.
        service.setOnFailed(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent e) {
                // Show the exception that caused it to fail.
                Throwable t = e.getSource().getException();
                assert t != null : "There was a failure but no exception thrown.";

                // Notify via callback.
                if (t instanceof ParseException)
                    callback.done(null, (ParseException) t);
                callback.done(null, new ParseException(t));
            }
        });
        service.start();
    }

    /**
     *
     * @param <T> ParseObject
     */
    private static class QueryService<T extends ParseObject> extends Service<List<T>> {

        private final SimpleObjectProperty<ParseQuery<T>> query =
                new SimpleObjectProperty<ParseQuery<T>>();

        public ParseQuery<T> getQuery() {
            return query.get();
        }

        public SimpleObjectProperty<ParseQuery<T>> queryProperty() {
            return query;
        }

        public void setQuery(ParseQuery<T> query) {
            this.query.set(query);
        }

        @Override
        protected Task<List<T>> createTask() {
            final ParseQuery<T> innerQuery = getQuery();
            return new Task<List<T>>() {
                @Override
                protected List<T> call() throws Exception {
                    return innerQuery.find();
                }
            };
        }
    }



}
