package com.hotan.army.supply.data.items.components;

/**
 * An interface the describes the mandatory requirement to be an DoD Accountable item.
 *
 * @author Michael Hotan, michael.hotan@gmail.com
 * Created by mhotan_dev on 1/7/14.
 */
public interface Accountable {

    /**
     * Every Accountable item has a pronouncable name.
     *
     * @return The name of this item.
     */
    public String getName();

    /**
     * Every Accountable item has a National Stock Number.
     *
     * @return the item's National Stock Number.
     */
    public String getNSN();

}
