package com.hotan.army.supply.data.items;

import com.hotan.army.supply.data.Mergeable;
import com.hotan.army.supply.data.inspection.DeficiencyEntry;
import com.hotan.army.supply.data.items.components.EndItemBasicIssueComponent;
import com.hotan.army.supply.data.items.components.EndItemComponent;
import com.hotan.army.supply.data.items.itemdata.EndItemData;
import com.hotan.army.supply.data.items.itemdata.EndItemDataManager;
import com.hotan.army.supply.data.location.LocatedAt;
import com.hotan.army.supply.data.parse.FXParseObject;
import com.hotan.army.supply.data.tracking.SignedOutRecord;
import com.hotan.army.supply.util.PropertyMerger;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.parse4j.ParseException;
import org.parse4j.ParseObject;
import org.parse4j.ParseQuery;
import org.parse4j.callback.FindCallback;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Class that represents a single end item.
 *
 * @author Michael Hotan, michael.hotan@gmail.com
 */
public abstract class EndItem extends FXParseObject implements Mergeable<EndItem> {

    private static final Logger LOG = Logger.getLogger(EndItem.class.getSimpleName());

    /*
    Representation invariant:
    location cannot be null
    clNumber cannot be null
    COIEComponents cannot be null
    BIIComponents cannot be null
    NSN cannot be null
    LIN cannot be null

    Abstract function:
    location ==> is the current location of the
    currentSignedOutRecord ==> the individual that this item is signed out to.
    if currentSignedOutRecord is null then it is signed out to no one
    if currentSignedOutRecord is not null then it is signed out to.
     */

    /**
     * Lazy instantiated members
     */
    private final StringProperty clNumber, NSN, LIN;

    /**
     * Location of this end Item
     */
    private final ObjectProperty<LocatedAt> location;

    /**
     * The individual that the end item is signed out to.
     */
    private final ObjectProperty<SignedOutRecord> currentSignedOutRecord;

    /**
     * The individual that the end item is signed out to.
     */
    private final ObjectProperty<EndItemData> data;

    /**
     * List of COIE components specifically for this end item.
     */
    private final ObservableList<EndItemComponent> COIEComponents;

    /**
     * List of BII components specifically for this end item..
     */
    private final ObservableList<EndItemBasicIssueComponent> BIIComponents;

    /**
     * The deficiencies of this
     */
    private final ObservableList<DeficiencyEntry> deficiencyEntries;

    // Static references.
    private static final String BELONGS_TO = "belongsTo";
    private static final String CL = "cl";
    private static final String NSN_KEY = "nsn";
    private static final String LIN_KEY = "lin";
    private static final String LOCATION = "location";
    private static final String RECORD = "trackingRecord";
    private static final String DATA = "endItemData";

    /**
     * Constructs a bare bones end item.
     * <br> By default there is not items
     */
    protected EndItem() {
        clNumber = new SimpleStringProperty();
        location = new SimpleObjectProperty<LocatedAt>();
        data = new SimpleObjectProperty<EndItemData>();
        currentSignedOutRecord = new SimpleObjectProperty<SignedOutRecord>();
        NSN = new SimpleStringProperty();
        LIN = new SimpleStringProperty();

        // Initialize the list to be thread safe.
        deficiencyEntries = FXCollections.synchronizedObservableList(FXCollections.observableArrayList());
        COIEComponents = FXCollections.synchronizedObservableList(FXCollections.observableArrayList());
        BIIComponents = FXCollections.synchronizedObservableList(FXCollections.observableArrayList());

        clNumber.addListener((ov, oldVal, newVal) -> {
            if (newVal != null)
                put(CL, newVal);
            else
                remove(CL);
        });
        NSN.addListener((ov, oldVal, newVal) -> put(NSN_KEY, newVal));
        LIN.addListener((ov, oldVal, newVal) -> put(LIN_KEY, newVal));
    }

    /**
     * Creates an End Item.
     *
     * @param nsn NSN of this End item
     * @param lin LIN of this End item
     */
    protected EndItem(String nsn, String lin) {
        this("", nsn, lin);
    }

    /**
     * Constructs an End Item that belongs in an End Item group.
     *
     * @param clNumber CL Number of this end item.
     * @param nsn NSN of this end item. Cannot be null.
     * @param lin LIN of this end item. Cannot be null.
     */
    protected EndItem(String clNumber, String nsn, String lin) {
        this();
        if (clNumber == null) clNumber = "";
        if (nsn == null || nsn.trim().isEmpty())
            throw new IllegalArgumentException(getClass().getSimpleName() + "() Illegal NSN \"" + nsn + "\"");
        if (lin == null || lin.trim().isEmpty())
            throw new IllegalArgumentException(getClass().getSimpleName() + "() Illegal LIN \"" + lin + "\"");
        // Instantiate the members
        this.clNumber.set(clNumber);
        this.NSN.set(nsn);
        this.LIN.set(lin);
    }

    @Override
    public void save() throws ParseException {
        // Create a many to one relation from
        // this end item to its stored location.
        LocatedAt storageLocation = getLocation();
        if (storageLocation != null) {
            if (storageLocation.getObjectId() == null)
                storageLocation.save();
            put(LOCATION, storageLocation.getPointer());
        } else
            remove(LOCATION);

        // Create a reference to the signed out record.
        SignedOutRecord record = getCurrentSignedOutRecord();
        if (record != null) {
            if (record.getObjectId() == null)
                record.save();
            put(RECORD, record.getPointer());
        } else
            remove(RECORD);

        // Save the end item data
        EndItemData data = getData();
        if (data != null) {
            if (data.getObjectId() == null)
                data.save();
            put(DATA, data.getPointer());
        } // Should always have one data point

        // Save this before we add a reference to other objects.
        super.save();

        // Set a one to Many relationship with all the components
        for (EndItemBasicIssueComponent comp: BIIComponents) {
            // If there is already a pointer to this entry then ignore saving it.
            ParseObject po = comp.getParseObject(BELONGS_TO);
            if (po != null && this.getObjectId().equals(po.getObjectId())) continue;

            // Add the pointer to this and save in a background thread.
            comp.put(BELONGS_TO, this.getPointer());
            comp.saveInBackground();
        }
        for (EndItemComponent comp: COIEComponents) {
            ParseObject po = comp.getParseObject(BELONGS_TO);
            if (po != null && this.getObjectId().equals(po.getObjectId())) continue;

            // Add the pointer to this and save in a background thread.
            comp.put(BELONGS_TO, this.getPointer());
            comp.saveInBackground();
        }
        for (DeficiencyEntry entry: this.deficiencyEntries) {
            ParseObject po = entry.getParseObject(BELONGS_TO);
            if (po != null && this.getObjectId().equals(po.getObjectId())) continue;

            // Add the pointer to this and save in a background thread.
            entry.put(BELONGS_TO, this.getPointer());
            entry.saveInBackground();
        }
        // Add a many relation for all Signed out records.
        if (record != null) {
            ParseObject po = record.getParseObject(BELONGS_TO);
            if (po == null || !this.getObjectId().equals(record.getObjectId())) {
                record.put(BELONGS_TO, this.getPointer());
            }
        }
    }

    @Override
    public void delete() throws ParseException {
        super.delete();

        LocatedAt storageLocation = getLocation();
        if (storageLocation != null)
            storageLocation.deleteInBackground();
    }

    @Override
    public <T extends ParseObject> T fetchIfNeeded() throws ParseException {
        super.fetchIfNeeded();

        // Pull the entire list of all the items
        ParseQuery<EndItemBasicIssueComponent> biiQuery = ParseQuery.getQuery(EndItemBasicIssueComponent.class);
        biiQuery.whereEqualTo(BELONGS_TO, this.getPointer());
        List<EndItemBasicIssueComponent> biiComps = biiQuery.find();
        if (biiComps != null)
            biiComps.stream().forEach(comp -> addBII(comp));

        // Pull all the COIE components
        ParseQuery<EndItemComponent> coieQuery = ParseQuery.getQuery(EndItemComponent.class);
        coieQuery.whereEqualTo(BELONGS_TO, this.getPointer());
        List<EndItemComponent> coieComps = coieQuery.find();
        if (coieComps != null)
            coieComps.stream().forEach(comp -> addCOEI(comp));

        ParseQuery<DeficiencyEntry> defQueries = ParseQuery.getQuery(DeficiencyEntry.class);
        defQueries.whereEqualTo(BELONGS_TO, this.getPointer());
        List<DeficiencyEntry> entries = defQueries.find();
        if (entries != null)
            entries.stream().forEach(entry -> addDeficiency(entry));

        return (T) this;
    }

    /////////////////////////////////////////////////
    //// Merge interface.
    /////////////////////////////////////////////////

    /**
     * Merges the the argument with this.
     * @param item Item to merge with this.
     */
    @Override
    public void merge(EndItem item) {
        if (this == item || item == null) return; // Can't merge with self
        if (!this.equals(item)) return;

        // Merge the location of these values.
        PropertyMerger.merge(this.location, item.location);
        PropertyMerger.merge(this.clNumber, item.clNumber);

        // Merge all the end items
        PropertyMerger.merge(COIEComponents, item.COIEComponents);
        PropertyMerger.merge(BIIComponents, item.BIIComponents);
    }

    /////////////////////////////////////////////////
    //// Setters
    /////////////////////////////////////////////////

    /**
     *
     * @param data Set the End Item Data.
     */
    public void setData(EndItemData data) {
        this.data.set(data);
    }

    /**
     * Sets the CL number of this end item.
     * @param clNumber The cl number of this.
     */
    protected void setClNumber(String clNumber) {
        this.clNumber.set(clNumber);
    }

    /**
     * Sets the current Record for when this item was signed out.
     *
     * @param currentSignedOutRecord The current record.
     */
    public void setCurrentSignedOutRecord(SignedOutRecord currentSignedOutRecord) {
        if (this.currentSignedOutRecord.isEqualTo(currentSignedOutRecord).get()) return;
        this.currentSignedOutRecord.set(currentSignedOutRecord);
    }

    /**
     * Set the location of this End Item.
     *
     * @param location The location of this
     */
    public void setLocation(LocatedAt location) {
        if (this.location.isEqualTo(location).get()) return;
        this.location.set(location);
    }

    /////////////////////////////////////////////////
    //// Mutators
    /////////////////////////////////////////////////


    public void addDeficiency(DeficiencyEntry entry) {
        if (entry == null || deficiencyEntries.contains(entry)) return;
        getDeficiencyEntries().add(entry);
    }

    /**
     * Adds COEI component to this.
     * @param component Component to add.
     */
    public void addCOEI(EndItemComponent component) {
        if (component == null || COIEComponents.contains(component)) return;
        getCOIEComponents().add(component);
    }

    /**
     * Adds BII component to this.
     * @param component BII Component to add.
     */
    public void addBII(EndItemBasicIssueComponent component) {
        if (component == null || BIIComponents.contains(component)) return;
        getBIIComponents().add(component);
    }

    /**
     * Attempts to remove an entry from this.
     * @param entry Entry to be removed
     * @return Whether or not the entry was moved.
     */
    public boolean removeDeficiencyEntry(DeficiencyEntry entry) {
        if (getDeficiencyEntries().remove(entry)) {
            // Remove any reference that this element belongs to this.
            if (this.equals(entry.get(BELONGS_TO))) {
                entry.remove(BELONGS_TO);
            }
            return true;
        }
        return false;
    }

    /**
     * Removes a component from this.
     *
     * @param component Component to remove.
     * @return Whether or not the component was moved.
     */
    public boolean removeCOEI(EndItemComponent component) {
        if (getCOIEComponents().remove(component)) {
            // Remove any reference that this element belongs to this.
            if (this.equals(component.get(BELONGS_TO)))
                component.remove(BELONGS_TO);
            return true;
        }
        return false;
    }

    /**
     * Removes BII component.
     * @param component BII component to remove.
     */
    public boolean removeBII(EndItemBasicIssueComponent component) {
        if (getBIIComponents().remove(component)) {
            // Remove any reference that this element belongs to this.
            if (this.equals(component.get(BELONGS_TO)))
                component.remove(BELONGS_TO);
            return true;
        }
        return false;
    }

    /////////////////////////////////////////////////
    //// Getters
    /////////////////////////////////////////////////

    public ObservableList<DeficiencyEntry> getDeficiencyEntries() {
        if (deficiencyEntries.isEmpty() && this.getObjectId() != null) {
            try {
                this.fetchIfNeeded(); // Implicitly pulls from the cloud
            } catch (ParseException e) {
                LOG.warning("Error occured while attempting to fetch list of items: " + e.getMessage());
            }
        }
        return deficiencyEntries;
    }

    /**
     *
     * @return The signed out record of this.
     */
    public SignedOutRecord getCurrentSignedOutRecord() {
        return currentSignedOutRecordProperty().get();
    }

    /**
     *
     * @param cb
     */
    public void getCurrentSignedOutRecord(FindCallback<SignedOutRecord> cb) {

        // IF we have the item locally.
        if (currentSignedOutRecord.isNotNull().get()) {
            List<SignedOutRecord> records = new ArrayList<>();
            records.add(currentSignedOutRecord.get());
            cb.done(records, null);
        }

        ParseQuery<SignedOutRecord> query = ParseQuery.getQuery(SignedOutRecord.class);
        query.whereEqualTo(BELONGS_TO, this);
        query.addDescendingOrder(SignedOutRecord.DATE_SIGNED_OUT);
        query.limit(1);
        query.findInBackground(new FindCallback<SignedOutRecord>() {
            @Override
            public void done(List<SignedOutRecord> list, ParseException e) {
                // Check if there was an error
                if (e != null) {
                    cb.done(list, e);
                    return;
                }

                if (list == null || list.isEmpty()) {
                    setCurrentSignedOutRecord(null);
                    return;
                }

                // Update our properties.
                SignedOutRecord record = list.get(0);
                setCurrentSignedOutRecord(record);

                // Pass through the message
                cb.done(list, e);
            }
        });
    }

    /**
     * All EndItem has particular data that pertains to this specific EndItem type.
     *
     * @return Non null EndItemData
     */
    public EndItemData getData() {
        return dataProperty().get();
    }

    public String getName() {
        return nameProperty().get();
    }

    /**
     * Returns the label for the name.  IE Serial Number could have a label SN
     *
     * @return name label.
     */
    public abstract String getNameLabel();

    public LocatedAt getLocation() {
        return locationProperty().get();
    }

    public String getClNumber() {
        return clNumberProperty().get();
    }

    public String getNSN() {
        return NSNProperty().get();
    }

    public String getLIN() {
        return LINProperty().get();
    }

    public ObservableList<EndItemComponent> getCOIEComponents() {
        if (COIEComponents.isEmpty() && this.getObjectId() != null) { // If the COIE Components is empty check if there is ParseData
            try {
                this.fetchIfNeeded();// Implicitly pulls from the cloud
            } catch (ParseException e) {
                LOG.warning("Error occured while attempting to fetch list of items: " + e.getMessage());
            }
        }
        return COIEComponents;
    }

    public ObservableList<EndItemBasicIssueComponent> getBIIComponents() {
        if (BIIComponents.isEmpty() && this.getObjectId() != null) { // If the COIE Components is empty check if there is ParseData
            try {
                this.fetchIfNeeded(); // Implicitly pulls from the cloud
            } catch (ParseException e) {
                LOG.warning("Error occured while attempting to fetch list of items: " + e.getMessage());
            }
        }
        return BIIComponents;
    }

    /////////////////////////////////////////////////
    //// Properties
    /////////////////////////////////////////////////

    public abstract ReadOnlyStringProperty nameProperty();

    /**
     * Obtains the property of the Current Signed Record.
     *
     * @return Current Signed out record.
     */
    public ObjectProperty<SignedOutRecord> currentSignedOutRecordProperty() {
        if (currentSignedOutRecord.isNull().get()) {
            ParseObject po = getParseObject(RECORD);
            if (po != null && po.getObjectId() != null) {
                try {
                    SignedOutRecord record = FXParseObject.createWithoutData(SignedOutRecord.class, po.getObjectId());
                    record.fetchIfNeeded();
                    this.currentSignedOutRecord.set(record);
                } catch (ParseException e) {
                    LOG.warning("Unable to pull Current SignedOutRecord:"
                            + po.getObjectId() + " due to: " + e.getMessage());
                }
            }
        }
        return currentSignedOutRecord;
    }

    public StringProperty clNumberProperty() {
        if (this.clNumber.isNull().get())
            this.clNumber.set(getString(CL));
        return clNumber;
    }

    /**
     * @return The End Item Data of this.
     */
    public ObjectProperty<EndItemData> dataProperty() {
        if (data.isNull().get()) {
            ParseObject pointer = getParseObject(DATA);
            if (pointer != null && pointer.getObjectId() != null) {
                try {
                    this.data.set(EndItemDataManager.getInstance().get(pointer.getObjectId()));
                } catch (ParseException e) {
                    LOG.warning("Exception occurred while attempting to grab End Item Data" + e.getMessage());
                }
            }
        }
        return data;
    }

    /**
     *
     * @return
     */
    public ObjectProperty<LocatedAt> locationProperty() {
        if (this.location.isNull().get()) {
            ParseObject pointer = getParseObject(LOCATION);
            if (pointer != null && pointer.getObjectId() != null) {
                try {
                    LocatedAt locatedAt = FXParseObject.createWithoutData(LocatedAt.class,
                            pointer.getObjectId());
                    locatedAt.fetchIfNeeded();
                    this.location.set(locatedAt);
                } catch (ParseException e) {
                    LOG.warning("Exception occurred while attempting to grab End Item Data" + e.getMessage());
                }
            }
        }
        return location;
    }

    public ReadOnlyStringProperty LINProperty() {
        // Find the LIN element.
        if (LIN.isNull().get())
            LIN.set(getString(LIN_KEY));
        return LIN;
    }

    public ReadOnlyStringProperty NSNProperty() {
        if (NSN.isNull().get())
            NSN.set(getString(NSN_KEY));
        return NSN;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EndItem)) return false;

        EndItem endItem = (EndItem) o;
        if (!getLIN().equals(endItem.getLIN())) return false;
        if (!getNSN().equals(endItem.getNSN())) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = getNSN().hashCode();
        result = 31 * result + getLIN().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "{" +
                "NSN=" + NSN.get() +
                ", LIN=" + LIN.get() + ", " + getNameLabel() + "=" + getName() +
                '}';
    }
}
