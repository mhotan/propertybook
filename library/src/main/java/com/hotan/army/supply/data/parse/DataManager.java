package com.hotan.army.supply.data.parse;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.parse4j.ParseException;
import org.parse4j.ParseQuery;
import org.parse4j.callback.FindCallback;
import org.parse4j.callback.GetCallback;

import java.util.List;

/**
 * Create a data manager for Parse Objects.
 *
 * @param <T> ParseObject for this manager.
 */
public abstract class DataManager<T extends FXParseObject> {

    /**
     *  Mapping for Object ID -> the EndItemData.
     */
    protected final ObservableList<T> objects;

    /**
     * Creates an isntance of a data manager.
     * <b>Only accessible from subclasses
     */
    protected DataManager() {
        objects = FXCollections.observableArrayList();
    }

    /**
     * Adds the parse object to this.  If there is already a parse object with the same object ID
     * then ignore it. An exception is thrown if the object has not been saved.
     *
     * @param object A saved Object to be added locally to this manager.
     */
    public synchronized void add(T object) {
        // Ignore null values.
        if (object == null) return;
        if (object.getObjectId() == null)
            throw new IllegalArgumentException(getClass().getSimpleName() + "add() " +
                    object + " does not have an object ID.");
        if (this.objects.stream().anyMatch(currObj -> currObj.getObjectId().equals(object.getObjectId()))) return;
        this.objects.add(object);
    }

    /**
     * Attempts to retrieve an object.  The hierarchy of retrieval is check locally then
     * the cloud.
     *
     * @param objectId String object ID to get.
     * @return The instance with the corresponding object ID.
     * @throws ParseException Error occured when accessing the cloud.
     */
    public synchronized T get(String objectId) throws ParseException {
        if (objectId == null || objectId.trim().isEmpty())
            throw new IllegalArgumentException(getClass().getSimpleName() + "get(), " +
                    "Illegal Object ID: \"" + objectId + "\"");
        // Find any object that has the match object ID.
        T found = this.objects.stream().filter(curObj -> curObj.getObjectId().equals(objectId)).findAny().orElse(null);
        if (found != null) return found;

        // Pull from cloud if not found locally.
        ParseQuery<T> query = ParseQuery.getQuery(getDataClass());
        found = query.get(objectId);
        if (found == null) return null;

        // TODO Fix Fetch if need.  Does not return actual sub type.
        found.fetchIfNeeded();
        T foundFetched = found;
        // Update our internal storage.
        add(foundFetched);
        return foundFetched;
    }

    protected void findFirstInBackgroud(ParseQuery<T> query, GetCallback<T> cb) {
        if (cb == null)
            throw new NullPointerException(getClass().getSimpleName() + "get(bookname, findCallback) " +
                    "Null FindCallback");
        query.findInBackground(new FindCallback<T>() {
            @Override
            public void done(List<T> list, ParseException e) {
                if (e != null) {
                    cb.done(null, e);
                    return;
                }
                T object = list.get(0);
                add(object);
                cb.done(object, null);
            }
        });
    }

    /**
     * @return A list of all the current objects.
     */
    public ObservableList<T> getAll() {
        return objects;
    }

    /**
     * @return The class type of internal data type.
     */
    protected abstract Class<T> getDataClass();

}
