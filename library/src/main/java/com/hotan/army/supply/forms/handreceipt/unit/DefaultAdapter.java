package com.hotan.army.supply.forms.handreceipt.unit;

import com.hotan.army.supply.data.items.EndItem;
import com.hotan.army.supply.data.items.itemdata.EndItemData;
import com.hotan.army.supply.data.items.NonSerializedEndItem;
import com.hotan.army.supply.data.items.SerializedEndItem;
import com.hotan.army.supply.data.personnel.Operator;
import com.hotan.army.supply.data.personnel.OperatorManager;
import com.hotan.army.supply.data.personnel.Rank;
import com.hotan.army.supply.forms.FormUtil;
import com.hotan.army.supply.forms.FormatException;
import com.hotan.army.supply.util.Alphabet;
import com.hotan.army.supply.util.POIUtil;
import com.hotan.army.supply.util.POIUtil.IndexPair;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.util.Pair;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.parse4j.ParseException;

import java.util.*;
import java.util.logging.Logger;

/**
 * @author Michael Hotan, michael.hotan@gmail.com
 */
public class DefaultAdapter implements UnitLevelHandReceiptAdapter {

    private static final Logger LOG = Logger.getLogger(DefaultAdapter.class.getSimpleName());

    // Index of the first row that could contain an End Item.
    // All rows after the admin title block.
    private static final int FIRST_ITEM_ROW_INDEX = 7;

    private static final IndexPair DATE_CELL = IndexPair.valueOf(0, 0);
    private static final IndexPair UIC_DESC_CELL = IndexPair.valueOf(3, Alphabet.A.index());
    private static final IndexPair WHOFROM_CELL = IndexPair.valueOf(2, Alphabet.F.index());
    private static final IndexPair WHOTO_CELL = WHOFROM_CELL.indexOnBottom();
    private static final IndexPair TEAM_CELL = WHOTO_CELL;

    @Override
    public Date getDate(HSSFSheet sheet) {
        Cell cell = POIUtil.getCell(sheet, DATE_CELL);
        String value = cell.getStringCellValue();
        value = value.replace("DATE PREPARED:", "");
        value = value.replace("UNIT LEVEL HAND RECEIPT", "");
        value = value.replace(" ", "");
        String[] dateStr = value.split("/");

        if (dateStr == null || dateStr.length != 3) {
            LOG.warning("Unable to parse date");
            return new Date();
        }

        Calendar cal = Calendar.getInstance();
        cal.clear();
        int month = Integer.parseInt(dateStr[0]) - 1;
        int day = Integer.parseInt(dateStr[1]);
        int year = Integer.parseInt(dateStr[2]) + 2000;
        cal.set(year, month, day);
        return cal.getTime();
    }

    @Override
    public String getUIC(HSSFSheet sheet) {
        Cell cell = POIUtil.getCell(sheet, UIC_DESC_CELL);
        return FormUtil.getValueWithSplit(cell, "UIC/DESC: ", 0);
    }

    @Override
    public String getDESC(HSSFSheet sheet) {
        Cell cell = POIUtil.getCell(sheet, UIC_DESC_CELL);
        return FormUtil.getValueWithSplit(cell, "UIC/DESC: ", 1);
    }

    @Override
    public String getTeam(HSSFSheet sheet) {
        Cell cell = POIUtil.getCell(sheet, TEAM_CELL);
        return FormUtil.getValueWithSplit(cell, "TO: ", 0);
    }

    @Override
    public Operator getWhoFrom(HSSFSheet sheet) {
        Cell cell = POIUtil.getCell(sheet, WHOFROM_CELL);
        return getOperator(cell, "FROM: ");
    }

    @Override
    public Operator getWhoTo(HSSFSheet sheet) {
        Cell cell = POIUtil.getCell(sheet, WHOTO_CELL);
        return getOperator(cell, "TO: ");
    }

    /**
     * Returns the operator.
     *
     * @param cell The cell the operator is contained in.
     * @param prefix The prefix preceding the Operator
     * @return Operator at cell, null if unable to retrieve operator
     */
    private static Operator getOperator(Cell cell, String prefix) {
        assert cell != null;
        assert prefix != null;
        String rankString = FormUtil.getValueWithSplit(cell, prefix, 2);
        String completeName = FormUtil.getValueWithSplit(cell, prefix, 1);
        String[] splitName = completeName.split(", ");
        if (splitName.length != 2) {
            throw new IllegalArgumentException("No name found in cell.");
        }
        try {
            return OperatorManager.getInstance().add(
                    splitName[1], "", splitName[0], Rank.getRank(rankString), null);
        } catch (ParseException e) {
            return null;
        }
    }

    @Override
    public ObservableMap<EndItemData, ObservableList<EndItem>> processHandReceipt(Workbook workbook) {
        ObservableMap<EndItemData, ObservableList<EndItem>> mapping = FXCollections.observableHashMap();

        Sheet sheet = workbook.getSheetAt(0);
        int lastRow = sheet.getLastRowNum();
        for (int i = FIRST_ITEM_ROW_INDEX; i <= lastRow; ++i) {

            // For each row check if it is the header for an enditem
            Row row = sheet.getRow(i);
            if (row == null) continue;
            // If the head is not white then consider it a header.
            if (isEndItemHeader(row)) {
                Pair<EndItemData, ObservableList<EndItem>> pair = processRow(row);

                // Check if the the current set of End Items has
                // a itemdata object that matches the one provided by the pair.
                if (mapping.containsKey(pair.getKey())) {
                    // Go through each itemdata and merge the equal set.
                    mapping.keySet().stream().forEach(data -> {
                        if (pair.getKey().equals(data))
                            data.merge(pair.getKey());
                    });

                    // Now merge the list of end items from one to the other.
                    // Only includes single unique enditems within the list.
                    ObservableList<EndItem> items = mapping.get(pair.getKey());
                    pair.getValue().stream().forEach(item -> {
                        if (items.contains(item)) return;
                        items.add(item);
                    });

                } else { // The end item itemdata was not currently in the mapping
                    mapping.put(pair.getKey(), pair.getValue());
                }
            }
        }
        return mapping;
    }

    @Override
    public void validate(Workbook workbook) throws FormatException {
        Sheet sheet = workbook.getSheetAt(0);
        if (sheet == null)
            throw new FormatException("Cannot find initial sheet");
        if (POIUtil.getCell(sheet, UIC_DESC_CELL) == null)
            throw new FormatException("UIC and DESC Cell not found at " + UIC_DESC_CELL);
        if (POIUtil.getCell(sheet, DATE_CELL) == null)
            throw new FormatException("Date Cell not found at " + DATE_CELL);
        if (POIUtil.getCell(sheet, TEAM_CELL) == null)
            throw new FormatException("Team Cell not found at " + TEAM_CELL);
        if (POIUtil.getCell(sheet, WHOFROM_CELL) == null)
            throw new FormatException("Who From Cell not found at " + WHOFROM_CELL);
        if (POIUtil.getCell(sheet, WHOTO_CELL) == null)
            throw new FormatException("Who To Cell not found at " + WHOTO_CELL);
    }

    /**
     * Process the end item for the row.
     * <br>Extracts the META Data for the end item
     * <br>If serial numbers exists, collect them all for the group.
     *
     * @param header Header of the end item.
     */
    private Pair<EndItemData, ObservableList<EndItem>> processRow(Row header) {
        String lin = header.getCell(0).getStringCellValue().trim();
        String nsn = header.getCell(2).getStringCellValue().trim();
        String nomenclature = header.getCell(4).getStringCellValue().trim();
        String qtyStr = header.getCell(9).getStringCellValue().trim();
        int qty = Integer.valueOf(qtyStr);

        // Create a new EndItem Data.
        final EndItemData data = new EndItemData(nsn, lin);
        data.addName(nomenclature);

        // Create a list for all the end items.
        final ObservableList<EndItem> items = FXCollections.observableArrayList();

        // Go to the next row
        Row nextRow = header.getSheet().getRow(header.getRowNum() + 1);

        // If the item is serialized then there will be a serial number
        // below the header
        if (nextRow == null || nextRow.getCell(0) == null
                || nextRow.getCell(0).getStringCellValue() == null ||
                nextRow.getCell(0).getStringCellValue().isEmpty()) {
            // Create individual end items for this itemdata.
            for (int i = 1; i <= qty; ++i) {
                items.add(new NonSerializedEndItem("" + i, nsn, lin));
            }

        } else { // There are serial numbers

            // Create a group that are serialized.
            List<String> serials = new ArrayList<>();

            // While the next row is not the header to the next end item,
            // or the number of serial numbers matches the qty specified in the form.
            while (serials.size() < qty && !isEndItemHeader(nextRow)) {
                List<String> tmp = getSerialNumbers(nextRow);
                serials.addAll(tmp);
                nextRow = header.getSheet().getRow(nextRow.getRowNum() + 1);
            }

            // Check the quantity matches the number of serial numbers
            if (qty != serials.size()) {
                LOG.warning("Number of serial numbers found "
                        + serials.size() + " did not match defined quantity " + qty);
            }

            // Add the serial numbered EndItem
            for (String serial: serials) {
                items.add(new SerializedEndItem(serial, nsn, lin));
            }
        }

        return new Pair<>(data, items);
    }

    private static final int SERIAL_NUMBER_ROW_POSITION_1 = 0;
    private static final int SERIAL_NUMBER_ROW_POSITION_2 = 4;
    private static final int SERIAL_NUMBER_ROW_POSITION_3 = 5;

    /**
     * Extract all the serial numbers for a given HSSF row that
     * is in the correct format.
     *
     *
     * @param row Row to find all the serial numbers
     * @return List of String serial numbers
     */
    private List<String> getSerialNumbers(Row row) {
        List<String> serialNums = new ArrayList<String>();
        if (isEndItemHeader(row)) return serialNums;

        // Extract the three serial number position
        List<String> tmp = new ArrayList<String>();
        tmp.add(row.getCell(SERIAL_NUMBER_ROW_POSITION_1).getStringCellValue());
        tmp.add(row.getCell(SERIAL_NUMBER_ROW_POSITION_2).getStringCellValue());
        tmp.add(row.getCell(SERIAL_NUMBER_ROW_POSITION_3).getStringCellValue());

        // Filter through the serial numbers
        for (String tmpSerial: tmp) {
            // If the serial number is empty or null ignore.
            if (tmpSerial == null || tmpSerial.isEmpty()) continue;
            serialNums.add(tmpSerial.trim());
        }
        return serialNums;
    }

    /**
     * Based on the color background of the hand receipt judge whether
     * or not the row represents an end Item header.
     *
     *
     * @param row Row to parse.
     * @return true if it is header false otherwise.
     */
    private static boolean isEndItemHeader(Row row) {
        // LIN Cell
        Cell linCell = row.getCell(0);
        if (linCell == null) return false;
        String cellVal = linCell.getStringCellValue();
        if (cellVal == null || cellVal.length() != 6) return false;

        // NSN Cell
        Cell nsnCell = row.getCell(2);
        if (nsnCell == null) return false;
        String nsnVal = nsnCell.getStringCellValue();
        if (nsnVal == null || nsnVal.length() != 13) return false;

        Cell nameCell = row.getCell(4);
        if (nameCell == null) return false;
        String nameVal = nameCell.getStringCellValue();
        if (nameVal == null || nameVal.isEmpty()) return false;

        return true;
    }
}
