package com.hotan.army.supply.data;

import com.hotan.army.supply.data.items.EndItem;
import com.hotan.army.supply.data.items.itemdata.EndItemData;
import javafx.beans.binding.Bindings;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 * Wrapper class for EndItem Data and its components. This class is not apart of the the underliyng
 * model representation.
 */
public class EndItemGroup {

/*
End Item groups were not included in the model because it was a burden
* */

    private final StringProperty nsn, lin, defaultName, pubNum, pubDate;

    private final IntegerProperty authorizedQty;

    private final EndItemData data;
    private final ObservableList<EndItem> items;

    EndItemGroup(EndItemData data, ObservableList<EndItem> items) {
        this.nsn = new SimpleStringProperty();
        this.lin = new SimpleStringProperty();
        this.defaultName = new SimpleStringProperty();
        this.pubNum = new SimpleStringProperty();
        this.pubDate = new SimpleStringProperty();
        this.authorizedQty = new SimpleIntegerProperty();

        this.nsn.bind(data.NSNProperty());
        this.lin.bind(data.LINProperty());
        this.defaultName.bind(data.defaultNameProperty());
        this.pubNum.bind(data.publicationNumberProperty());
        this.pubDate.bind(data.publicationDateProperty());
        this.authorizedQty.bind(Bindings.size(items));

        this.data = data;
        this.items = items;
    }

    public String getNsn() {
        return nsn.get();
    }

    public StringProperty nsnProperty() {
        return nsn;
    }

    public String getLin() {
        return lin.get();
    }

    public StringProperty linProperty() {
        return lin;
    }

    public String getDefaultName() {
        return defaultName.get();
    }

    public StringProperty defaultNameProperty() {
        return defaultName;
    }

    public String getPubNum() {
        return pubNum.get();
    }

    public StringProperty pubNumProperty() {
        return pubNum;
    }

    public String getPubDate() {
        return pubDate.get();
    }

    public StringProperty pubDateProperty() {
        return pubDate;
    }

    public int getAuthorizedQty() {
        return authorizedQty.get();
    }

    public IntegerProperty authorizedQtyProperty() {
        return authorizedQty;
    }

    public EndItemData getData() {
        return data;
    }

    public ObservableList<EndItem> getItems() {
        return items;
    }

}
