package com.hotan.army.supply.forms;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * A base template for documents in a xls form.
 */
public abstract class Template {

    /**
     * Reference to a xls workbook that is used as a reference.
     */
    protected final HSSFWorkbook workbook;

    /**
     * Adapter used to communicate.
     */
    protected final FormAdapter adapter;

    /**
     * Creates a template from a workbook.
     *
     * @param wb workbook to create a template from.
     * @param adapter Adapter used to interact with the workbook.
     * @throws FormatException Form is not compatible with the adapter.
     */
    protected Template(HSSFWorkbook wb, FormAdapter adapter) throws FormatException {
        if (wb == null)
            throw new IllegalArgumentException(getClass().getSimpleName() + "(), Null Workbook");
        if (adapter == null)
            throw new IllegalArgumentException(getClass().getSimpleName() + "(), Null Form Adapter");
        // Always validate the workbook.
        adapter.validate(wb);
        this.workbook = wb;
        this.adapter = adapter;
    }
    
    /**
     * Return the underlying workbook
     *  
     * @return Returns the underlying workbook.
     */
    protected Workbook getWorkbook() {
        return workbook;
    }

    /**
     * Returns the adapter for this
     *
     * @return the adapter used to communicate with this workbook.
     */
    protected FormAdapter getAdapter() {
        return adapter;
    }

    /**
     * Saves a copy of the template to the directory path and defined name.
     * 
     * @param dirPath Directory path to save copy to
     * @param name Name to associate to copy with.
     * @throws java.io.IOException Error occured when writing out the coverpage
     */
    public void save(String dirPath, String name) throws IOException {
        if (name == null)
            throw new NullPointerException("Template.save() Can't save Null name");
        if (name.isEmpty()) 
            throw new IllegalArgumentException("Template.save() Name must not be empty");
        if (dirPath == null)
            dirPath = "";
        String fullPath = dirPath;
        if (!fullPath.endsWith("/"))
            fullPath += "/";

        // Make sure the name ends in xls or .xlsx
        if (!(name.endsWith(".xls"))) {
            name += ".xls";
        }
        fullPath += name;
        
        Path p = Paths.get(fullPath);
        Files.deleteIfExists(p);
        p = Files.createFile(p);
        OutputStream os = Files.newOutputStream(p);
        workbook.write(os);
        os.close();
    }
    
}
