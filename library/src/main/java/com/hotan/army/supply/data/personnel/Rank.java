package com.hotan.army.supply.data.personnel;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * 
 * @author Michael Hotan, michael.hotan@gmail.com
 */
public enum Rank implements Comparable<Rank> {

    PVT("PVT"), PV2("PV2"), PFC("SPC"), SPC("SPC"), CPL("CPL"), SGT("SGT"), SSG("SSG"),
    SFC("SFC"), MSG("MSG"), SM("SM"), CSM("CSM"), SMA("SMA"),
    LT("LT"), CPT("CPT"), MAJ("MAJ"), LTC("LTC"), COL("COL"),
    BG("BG"), MG("MG"), LTG("LTG"), GEN("GEN"), GOA("GOA"),
    WO1("WO1"), CW2("CW2"), CW3("CW3"), CW4("CW4"), CW5("CW5")
    ;

    public static final ObservableList<Rank> RANKS = FXCollections.unmodifiableObservableList(
            FXCollections.observableArrayList(PVT, PV2, PFC, SPC, CPL, SGT, SSG, SFC, MSG, SM, CSM, SMA, LT, CPT,
                    MAJ, LTC, COL, BG, MG, LTG, GEN, GOA, WO1, CW2, CW3, CW4, CW5));

    // Readable version of the string.
    private final String readableString;

    private Rank(String text) {
        this.readableString = text;
    }
    
    @Override
    public String toString() {
        return readableString;
    }

    /**
     * Returns rank that has the value argument.
     *
     * @param value String representation of the Rank
     * @return The rank that corresponds to the string value.
     */
    public static Rank getRank(String value) {
        for (Rank rank: RANKS) {
            if (rank.readableString.equalsIgnoreCase(value)) {
                return rank;
            }
        }
        return null;
    }
}
