package com.hotan.army.supply.data.items;

import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.parse4j.ParseClassName;

/**
 * Class that represents NonSerialized End item.
 *
 * Created by mhotan on 3/7/14.
 */
@ParseClassName("NonSerializedEndItem")
public class NonSerializedEndItem extends EndItem {

    private static final String UNIQUE_NAME = "uniqueName";

    /**
     * Unique name of this end item that distinguishes itself from other items
     */
    private final StringProperty uniqueName;

    public NonSerializedEndItem() {
        super();
        uniqueName = new SimpleStringProperty();
        uniqueName.addListener((ov, oldVal, newVal) -> {
            put(UNIQUE_NAME, newVal);
        });
    }

    /**
     * Creates an end item with this unique name.
     *
     * @param uniqueName Unique name of this end item.
     */
    public NonSerializedEndItem(String uniqueName, String nsn, String lin) {
        this(uniqueName, "", nsn, lin);
    }

    /**
     * Creates a End Item with the unique name or cl number.
     *
     * @param uniqueName Unique name of this end item.
     * @param clNumber CL Number of this end item.
     */
    public NonSerializedEndItem(String uniqueName, String clNumber, String nsn, String lin) {
        super(clNumber, nsn, lin);
        if (uniqueName == null || uniqueName.trim().isEmpty())
            throw new IllegalArgumentException(getClass().getSimpleName() +
                    "() Illegal Unique name \"" + uniqueName + "\"");
        if (clNumber == null)
            throw new IllegalArgumentException(getClass().getSimpleName() +
                    "() Illegal CL Number \"" + clNumber + "\"");

        this.uniqueName = new SimpleStringProperty();
        this.uniqueName.addListener((ov, oldVal, newVal) -> {
            put(UNIQUE_NAME, newVal);
        });
        this.uniqueName.set(uniqueName);
    }

    @Override
    public String getNameLabel() {
        return "Name";
    }

    @Override
    public ReadOnlyStringProperty nameProperty() {
        if (uniqueName.isNull().get()) {
            uniqueName.set(getString(UNIQUE_NAME));
        }
        return uniqueName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NonSerializedEndItem)) return false;
        if (!super.equals(o)) return false;

        NonSerializedEndItem that = (NonSerializedEndItem) o;
        if (!uniqueName.isEqualTo(that.uniqueName).get()) return false;
        return true;
    }

    @Override
    public int hashCode() {
        return uniqueName.get().hashCode();
    }

    @Override
    public String toString() {
        return "NonSerializedEndItem{" +
                "uniqueName=" + uniqueName + ", " + super.toString() + '}';
    }
}
