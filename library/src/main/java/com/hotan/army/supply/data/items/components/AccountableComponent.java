package com.hotan.army.supply.data.items.components;

import com.hotan.army.supply.data.parse.FXParseObject;
import javafx.beans.property.*;

/**
 * Represents an Accountable Component to a DoD supplied EndItem.
 * <b>IE. Night vision pouches are an accountable item to a night vision device.</b>
 * 
 * @author Michael Hotan, michael.hotan@gmail.com
 */
public abstract class AccountableComponent extends FXParseObject
        implements Accountable {

    private static final String NAME = "name";
    private static final String NSN_KEY = "nsn";
    private static final String ONHAND = "onHandQuantity";
    private static final String AUTHORIZED = "authorizedQty";

    /*
    Representation Invariant:
    name is not null
    NSN is not null or empty
    authorized quantity is never negative
    onHandQuantity is always >= 0 and <= authorizedQuantity

    Abstract Function:

    */

    /**
     * Name of the Accountable Component.
     * <b>Name of the NSN.</b>
     */
    private final StringProperty name, NSN;

    /**
     * The exact quantity authorized.
     */
    private final IntegerProperty authorizedQuantity;

    /**
     * The Quantity of the accoutable components on hand.
     */
    private IntegerProperty onHandQuantity;

    /**
     * Creates an empty accountable component.
     */
    public AccountableComponent() {
        name = new SimpleStringProperty();
        NSN = new SimpleStringProperty();
        authorizedQuantity = new SimpleIntegerProperty();
        onHandQuantity = new SimpleIntegerProperty();

        name.addListener((ov, oldVal, newVal) -> {
            // New value can never be null.
            put(NAME, newVal);
        });
        NSN.addListener((ov, oldVal, newVal) -> {
            // New value can never be null.
            put(NSN_KEY, newVal);
        });
        authorizedQuantity.addListener((ov, oldVal, newVal) -> {
            // New value can never be null.
            put(AUTHORIZED, newVal);
        });

        // Watch for changes in the onHand quantity
        onHandQuantity.addListener((ov, oldVal, newVal) -> {
            if (newVal == null || newVal.intValue() < 0) {
                newVal = 0;
            }
            put(ONHAND, newVal);
        });
    }

    /**
     * Creates an accountable component with exactly an authorized quantity of one.
     *
     * @param name The Name of the Accountable Component.
     * @param nsn NSN of the Accountable Component
     */
    protected AccountableComponent(String name, String nsn) {
        this(name, nsn, 1);
    }

    /**
     * Creates an accountable component with an authorized quantity.
     *
     * @param name The Name of the Accountable Component.
     * @param nsn NSN of the Accountable Component
     * @param authorizedQty The authorized quantity of the accountable component.
     */
    protected AccountableComponent(String name, String nsn, int authorizedQty) {
        this();
        // Check the name
        if (name == null)
            name = "";
        name = name.trim();

        if (nsn == null || nsn.trim().isEmpty())
            throw new IllegalArgumentException(getClass().getSimpleName() +
                    "(), Illegal NSN \"" + nsn + "\"");
        nsn = nsn.trim();

        if (authorizedQty < 0)
            throw new IllegalArgumentException(getClass().getSimpleName() +
                    "(), Authorized Quantity cannot be negative");

        this.name.set(name);
        this.NSN.set(nsn);
        this.authorizedQuantity.set(authorizedQty);
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    ////    Setters
    ////////////////////////////////////////////////////////////////////////////////////////

    public void setOnHandQuantity(int qty) {
        if (qty < 0) qty = 0;
        this.onHandQuantity.set(qty);
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    ////    Getters
    ////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public String getName() {
        return nameProperty().get();
    }

    @Override
    public String getNSN() {
        return NSNProperty().get();
    }

    /**
     * The Authorized quantity of this Accountable component.
     *
     * @return The Authorized Quantity.
     */
    public int getAuthorizedQuantity() {
        return authorizedQuantityProperty().get();
    }

    /**
     * On Hand quantity.
     *
     * @return The on Hand Quantity of this item.
     */
    public int getOnHandQuantity() {
        return onHandQuantityProperty().get();
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    ////    Property Getters
    ////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @return Property containing the On Hand Quantity
     */
    public IntegerProperty onHandQuantityProperty() {
        if (onHandQuantity.lessThanOrEqualTo(0).get()) {
            onHandQuantity.set(getInt(ONHAND));
        }
        return onHandQuantity;
    }

    /**
     * @return Property containing the name.
     */
    public ReadOnlyStringProperty nameProperty() {
        if (name.isNull().get()) {
            name.set(getString(NAME));
        }
        return name;
    }

    /**
     * @return The Property containing the NSN value.
     */
    public ReadOnlyStringProperty NSNProperty() {
        if (NSN.isNull().get())
            NSN.set(getString(NSN_KEY));
        return NSN;
    }

    /**
     * @return The Property containing the Authorized Quantity.
     */
    public ReadOnlyIntegerProperty authorizedQuantityProperty() {
        if (authorizedQuantity.isEqualTo(0).get())
            authorizedQuantity.set(getInt(AUTHORIZED));
        return authorizedQuantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AccountableComponent)) return false;
        AccountableComponent that = (AccountableComponent) o;
        if (!getNSN().equals(that.getNSN())) return false;
        if (!getName().equals(that.getName())) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = getName().hashCode();
        result = 31 * result + getNSN().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name=" + nameProperty().get() +
                ", NSN=" + NSNProperty().get() +
                ", quantity=" + onHandQuantityProperty().get() +
                " / " + authorizedQuantityProperty().get() +
                '}';
    }
}
