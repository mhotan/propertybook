package com.hotan.army.supply.data;

import com.hotan.army.supply.data.items.EndItem;
import com.hotan.army.supply.data.items.itemdata.EndItemData;
import com.hotan.army.supply.data.parse.FXParseObject;
import com.hotan.army.supply.data.personnel.Operator;
import com.hotan.army.supply.data.personnel.OperatorManager;
import com.hotan.army.supply.forms.handreceipt.subcomponent.ComponentHandReceipt;
import com.hotan.army.supply.forms.handreceipt.unit.UnitLevelHandReceipt;
import com.hotan.army.supply.util.PropertyMerger;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.NumberBinding;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import org.parse4j.ParseClassName;
import org.parse4j.ParseException;
import org.parse4j.ParseObject;
import org.parse4j.ParseQuery;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * A Container class that represents a ODA Team PropertyBook
 *
 * @author Michael Hotan, michael.hotan@gmail.com
 */
@ParseClassName("PropertyBook")
public class PropertyBook extends FXParseObject {

    private static final Logger LOG = Logger.getLogger(PropertyBook.class.getSimpleName());

    static final String NAME = "name";
    static final String SIGNEE = "signee";
    static final String SIGNER = "signer";
    static final String DATE_PREPARED = "datePrepared";
    static final String UIC_KEY = "uic";
    static final String DESC_KEY = "desc";
    static final String TEAM = "team";
    static final String BELONGS_TO = "belongsTo";
    static final String SERIALIZED_END_ITEMS = "serializedEndItems";
    static final String NONSERIALIZED_END_ITEMS = "nonSerializedEndItems";

    /*
    Representation Invariant:
    signee cannot be null
    signer cannot be null
    datePrepared cannot be null as well
    name cannot be null
    UIC cannot be null
    DESC cannot be null
    team cannot be null.

    Abstract Function:

    */

    /**
     * Reference to who the property book is signed from and to.
     */
    private final ObjectProperty<Operator> signee, signer;

    /**
     * Date Prepared
     */
    private final ObjectProperty<LocalDate> datePrepared;

    /**
     * UIC, DESC, and Team the property book belongs
     */
    private final StringProperty UIC, DESC, team, name;

    /**
     * Mapping of end item itemdata to the respective list of end items.
     */
    private final ObservableList<EndItem> endItems;

    /**
     * A quick access to all the different types of end items.
     */
    private final ObservableList<EndItemGroup> groups;

    /**
     * The total
     */
    private final IntegerProperty total, onHand;

    public PropertyBook() {
        this.signee = new SimpleObjectProperty<>();
        this.signer = new SimpleObjectProperty<>();
        this.datePrepared = new SimpleObjectProperty<>();
        this.UIC = new SimpleStringProperty();
        this.DESC = new SimpleStringProperty();
        this.team = new SimpleStringProperty();
        this.name = new SimpleStringProperty();
        this.endItems = FXCollections.observableArrayList();
        this.total = new SimpleIntegerProperty();
        this.onHand = new SimpleIntegerProperty();
        this.groups = FXCollections.observableArrayList();

        this.total.bind(Bindings.size(endItems));

        // Everytime the list changes then update the on Hand Binding.
        // Also update the list of end item groups.
        this.endItems.addListener(new ListChangeListener<EndItem>() {
            @Override
            public void onChanged(Change<? extends EndItem> change) {
                onHand.bind(calculateOnHandBindings());

                while (change.next()) {
                    if (change.wasPermutated()) {
                        // NOTE: Currently nothing to do for a permutated list.
//                        for (int i = change.getFrom(); i < change.getTo(); ++i) {
//                            //permutate
//                        }
                        LOG.info("EndItems list was permutated");
                    } else if (change.wasUpdated()) {
                        //update item
                        LOG.info("EndItems list was updated");
                    } else {
                        // If the item has been removed then
                        // update the end item group.
                        change.getRemoved().stream().forEach(item -> {
                            EndItemGroup group = getGroup(item.getNSN(), item.getLIN());
                            assert group != null: "EndItem group should never be null for an end item being removed";
                            group.getItems().remove(item);
                            if (group.getItems().isEmpty())
                                groups.remove(group);

                            // TODO Delete the end item from the cloud.
                            // NOTE: Do not delete the End Item Data.
                        });

                        change.getAddedSubList().stream().forEach(item -> {
                            EndItemGroup group = getGroup(item.getNSN(), item.getLIN());
                            if (group == null) {
                                ObservableList<EndItem> groupItems = FXCollections.observableArrayList();
                                group = new EndItemGroup(item.getData(), groupItems);
                                groups.add(group);
                            }
                            group.getItems().add(item);
                        });
                    }
                }
            }
        });

        // Add all the listeners for for the properties.
        signee.addListener((ov, oldVal, newVal) -> {
            // Remove the old value if there was any.
            if (newVal == null) {
                remove(SIGNEE);
                return;
            }
            if (newVal.getObjectId() != null) {
                // If there object has been saved previously then we can safely add a reference
                put(SIGNEE, FXParseObject.createWithoutData(Operator.class, newVal.getObjectId()));
            }
        });
        signer.addListener((ov, oldVal, newVal) -> {
            // Remove the old value if there was any.
            if (newVal == null) {
                remove(SIGNER);
                return;
            }
            if (newVal.getObjectId() != null) {
                // If there object has been saved previously then we can safely add a reference
                put(SIGNER, FXParseObject.createWithoutData(Operator.class, newVal.getObjectId()));
            }
        });
        datePrepared.addListener((ov, oldVal, newVal) -> {
            // Remove the old value if there was any.
            if (newVal == null) {
                remove(SIGNER);
                return;
            }
            Date date = Date.from(newVal.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
            put(DATE_PREPARED, date.getTime());
        });
        UIC.addListener((ov, oldVal, newVal) -> {
            // Remove the old value if there was any.
            if (newVal == null) {
                remove(UIC_KEY);
                return;
            }
            put(UIC_KEY, newVal);
        });
        DESC.addListener((ov, oldVal, newVal) -> {
            // Remove the old value if there was any.
            if (newVal == null) {
                remove(DESC_KEY);
                return;
            }
            put(DESC_KEY, newVal);
        });
        team.addListener((ov, oldVal, newVal) -> {
            // Remove the old value if there was any.
            if (newVal == null) {
                remove(TEAM);
                return;
            }
            put(TEAM, newVal);
        });
        name.addListener((ov, oldVal, newVal) -> {
            // Remove the old value if there was any.
            if (newVal == null) {
                remove(NAME);
                return;
            }
            put(NAME, newVal);
        });
    }

    private NumberBinding calculateOnHandBindings() {
        NumberBinding onHand = new SimpleIntegerProperty(0).add(0);
        for (EndItem item : endItems) {
            onHand = onHand.add(Bindings.createIntegerBinding(() ->
                            item.currentSignedOutRecordProperty().isNull().get() ? 1 : 0,
                    item.currentSignedOutRecordProperty().isNull()));
        }
        return onHand;
    }

    /**
     * Creates a Property Book based from a Unit Level Hand Receipt.
     *
     * @param unitHR Unit Hand Receipt for team.
     * @param compHR Component Hand Receipt for the team.
     */
    PropertyBook(String name, UnitLevelHandReceipt unitHR, ComponentHandReceipt compHR) {
        this();
        if (name == null || name.trim().isEmpty())
            throw new IllegalArgumentException("illegal name \"" + name + "\"");
        if (unitHR == null)
            throw new NullPointerException("Null Unit Hand Receipt");

        this.name.set(name.trim());
        Instant it = Instant.ofEpochMilli(unitHR.getDatePrepared().getTime());
        this.datePrepared.set(LocalDateTime.ofInstant(it, ZoneId.systemDefault()).toLocalDate());
        // Store all the administrative itemdata.
        this.signee.set(unitHR.getWhoFrom());
        this.signer.set(unitHR.getWhoTo());
        this.UIC.set(unitHR.getUIC());
        this.DESC.set(unitHR.getDESC());
        this.team.set(unitHR.getTeam());


        // Update the
        ObservableMap<EndItemData, ObservableList<EndItem>> unitGroups = unitHR.getItems();
        // Initially add all the end items from the unit hand receipt.
        ObservableMap<EndItemData, ObservableList<EndItem>> endItems = FXCollections.observableMap(unitGroups);

        if (compHR == null) {
            // If there is no component hand receipt then just use the unit hand receipt.
            endItems.keySet().stream().forEach(data -> {
                endItems.get(data).stream().forEach(item -> {
                    item.setData(data);
                    add(item);
                });
            });
        }  else {

            // Use both hand receipts.
            ObservableMap<EndItemData, ObservableList<EndItem>> compGroups = compHR.getItems();
            // Merge the two maps.
            endItems.keySet().stream().forEach(data -> {
                EndItemData compData = compGroups.keySet().stream().filter(
                        tmp -> tmp.equals(data)).findFirst().orElse(null);

                // Merge any other End Item Data instance to the current one.
                if (compData != null) {
                    data.merge(compData);
                    // Merge the two list of end items together.
                    PropertyMerger.merge(endItems.get(data), compGroups.get(compData));
                }

                // Set the itemdata instance for this end item.
                // Update the end item for this property book.
                endItems.get(data).stream().forEach(item -> {
                    item.setData(data); // Set the itemdata for this End Item
                    add(item); // Adds to this Collection.
                });
            });
        }
    }

    @Override
    public void save() throws ParseException {
        // Save the signer
        if (signer.isNotNull().get()) {
            Operator signr = signer.get();
            if (signr.getObjectId() == null)
                signr.save();
            put(SIGNER, signr.getPointer());
        }
        // Save the Signee
        if (signee.isNotNull().get()) {
            Operator signe = signee.get();
            if (signe.getObjectId() == null)
                signe.save();
            put(SIGNEE, signe.getPointer());
        }
        super.save();

        // Save all the EndItem Data if it doesn't current exists.
        for (EndItem item: this.endItems) {
            // Check if the Current Property Book pointer in the item points to this.
            ParseObject pbPointer = item.getParseObject(BELONGS_TO);
            if (pbPointer == null || this.getObjectId().equals(pbPointer.getObjectId())) {
                // Update and Save the current pointer
                item.put(BELONGS_TO, FXParseObject.createWithoutData(PropertyBook.class, this.getObjectId()));
                item.save();
            }
        }
    }

    @Override
    public void delete() throws ParseException {
        super.delete();
        //        TODO Delete all the items that belong to this.
    }

    @Override
    public PropertyBook fetchIfNeeded() throws ParseException {
        PropertyBook book = super.fetchIfNeeded();

        // Check the query.
        ParseQuery<EndItem> query = ParseQuery.getQuery(EndItem.class);
        query.whereEqualTo(BELONGS_TO, this.getPointer());
        List<EndItem> items = query.find();
        if (items == null) {
            LOG.warning(getClass().getSimpleName() + ".fetchIfNeeded() " +
                    "Unable to find any items for Property Book " + this);
            return book;
        }

        // Add all the items for this.
        items.stream().forEach(item -> this.endItems.add(item));
        return book;
    }

    ////////////////////////////////////////////////////////////////////////////////
    //    Setters
    ////////////////////////////////////////////////////////////////////////////////

    private void add(EndItem item) {
        if (this.endItems.contains(item)) return;
        this.endItems.add(item);
    }

    ////////////////////////////////////////////////////////////////////////////////
    //    Getters
    ////////////////////////////////////////////////////////////////////////////////

    /**
     * @return The groups of this property book.
     */
    public ObservableList<EndItemGroup> getGroups() {
        return FXCollections.observableList(this.groups);
    }

    /**
     * Returns the end item group with the matching nsn and lin. Null if no end item
     * exists.
     *
     * @param nsn NSN of the end item group.
     * @param lin LIN of the end item group.
     * @return End Item group with matching NSN and LIN.
     */
    private EndItemGroup getGroup(String nsn, String lin) {
        return groups.stream().filter(group -> group.getNsn().equals(nsn) &&
                group.getLin().equals(lin)).findAny().orElse(null);
    }

    /**
     * @return The name of this property book.
     */
    public String getName() {
        return nameProperty().get();
    }

    /**
     * Returns number of groups.
     *
     * @return number of EndItemGroups
     */
    public int size() {
        return getEndItems().size();
    }

    /**
     * Returns the UIC of this property book.
     *
     * @return the UIC of this Property Book.
     */
    public String getUIC() {
        return UICProperty().get();
    }

    /**
     * Returns the DESC of this property book.
     *
     * @return the DESC of this Propery book.
     */
    public String getDESC() {
        return DESCProperty().get();
    }

    /**
     * The team the property book belongs to.
     *
     * @return The team this property book belongs too.
     */
    public String getTeam() {
        return teamProperty().get();
    }

    /**
     * Returns the operator this
     *
     * @return Person who signing over the collection of End Items.
     */
    public Operator getSignee() {
        return signeeProperty().get();
    }

    /**
     * Returns the Operator who is signing out this property book.
     *
     * @return Person who is signing for the collection of End Items
     */
    public Operator getSigner() {
        return signerProperty().get();
    }

    /**
     * @return The date the property book was prepared.
     */
    public LocalDate getDatePrepared() {
        return datePreparedProperty().get();
    }

    /**
     * @return Total number of end items.
     */
    public int getTotal() {
        return totalProperty().get();
    }

    /**
     * @return Number of on hand elements.
     */
    public int getOnHand() {
        return onHandProperty().get();
    }


    ////////////////////////////////////////////////////////////////////////////////
    //    Properties
    ////////////////////////////////////////////////////////////////////////////////

    public ReadOnlyStringProperty nameProperty() {
        if (name.isNull().get()) {
            name.set(getString(NAME));
        }
        return name;
    }

    public IntegerProperty totalProperty() {
        return total;
    }


    public IntegerProperty onHandProperty() {
        return onHand;
    }

    /**
     * @return The UIC Property
     */
    public ReadOnlyStringProperty UICProperty() {
        if (UIC.isNull().get())
            UIC.set(getString(UIC_KEY));
        return UIC;
    }

    /**
     * @return The DESC Property
     */
    public ReadOnlyStringProperty DESCProperty() {
        if (DESC.isNull().get())
            DESC.set(getString(DESC_KEY));
        return DESC;
    }

    /**
     * @return The assigned team Property
     */
    public ReadOnlyStringProperty teamProperty() {
        if (team.isNull().get())
            team.set(getString(TEAM));
        return team;
    }

    /**
     * @return The signee property
     */
    public ObjectProperty<Operator> signeeProperty() {
        if (signee.isNull().get()) {
            ParseObject pointer = getParseObject(SIGNEE);
            if (pointer != null && pointer.getObjectId() != null) {
                try {
                    Operator operator = OperatorManager.getInstance().get(pointer.getObjectId());
                    signee.set(operator);
                } catch (ParseException e) {
                    LOG.warning("Unable to pull Current Signee Operator:"
                            + pointer.getObjectId() + " due to: " + e.getMessage());
                }
            }
        }
        return signee;
    }

    /**
     * @return The signer property
     */
    public ObjectProperty<Operator> signerProperty() {
        if (signer.isNull().get()) {
            ParseObject pointer = getParseObject(SIGNER);
            if (pointer != null && pointer.getObjectId() != null) {
                try {
                    Operator operator = OperatorManager.getInstance().get(pointer.getObjectId());
                    signer.set(operator);
                } catch (ParseException e) {
                    LOG.warning("Unable to pull Current Signer Operator:"
                            + pointer.getObjectId() + " due to: " + e.getMessage());
                }
            }
        }
        return signer;
    }

    /**
     * @return the date prepared proeprty
     */
    public ReadOnlyObjectProperty<LocalDate> datePreparedProperty() {
        if (datePrepared.isNull().get()) {
            long millisec = getLong(DATE_PREPARED);
            Instant it = Instant.ofEpochMilli(millisec);
            datePrepared.set(LocalDateTime.ofInstant(it, ZoneId.systemDefault()).toLocalDate());
        }
        return datePrepared;
    }

    /**
     * @return the list of end items.
     */
    public ObservableList<EndItem> getEndItems() {
        // If the list is empty and there actually is an object id to use as a reference.
        if (endItems.isEmpty() && getObjectId() != null) {
            try {
                this.fetchIfNeeded(); // Implicitly pulls from the cloud
            } catch (ParseException e) {
                LOG.warning("Error occured while attempting to fetch list of items: " + e.getMessage());
            }
        }
        return endItems;
    }

    @Override
    public String toString() {
        return "PropertyBook{" +
                "name=" + name +
                '}';
    }
}
