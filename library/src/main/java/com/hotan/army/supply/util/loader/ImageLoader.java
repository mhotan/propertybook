/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

package com.hotan.army.supply.util.loader;

import javafx.scene.image.Image;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Class that loads images stored in the directory 'images' directory
 * relative to the project root.
 * 
 * @author mhotan
 */
public class ImageLoader {

    private static final String DIRECTORY = "images";

    /**
     * Cannot instantiate.
     */
    private ImageLoader() {}

    /**
     * Loads image from images directory which is relative to root.
     * 
     * @param imageName Name of the image to load
     * @return Image associated with the name
     * @throws java.io.FileNotFoundException Image not found
     */
    public static Image loadImage(String imageName) throws FileNotFoundException {
        return new Image(BaseLoader.stream(DIRECTORY, imageName));
    }

    /**
     * Loads an image from a file.
     *
     * @param imageFile File to load.
     * @return Image from the file.
     */
    public static Image loadImage(File imageFile) throws FileNotFoundException {
        return new Image(new FileInputStream(imageFile));
    }
    
}
