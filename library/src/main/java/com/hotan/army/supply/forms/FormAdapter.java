package com.hotan.army.supply.forms;

import org.apache.poi.ss.usermodel.Workbook;

/**
 * Allows interactions and methods to validate interacting with a workbook.
 *
 * @author Michael Hotan, michael.hotan@gmail.com
 */
public interface FormAdapter {

    /**
     * Checks whether this workbook works with this adapter.
     *
     * @param workbook Workbook to validate.
     * @throws com.hotan.army.supply.forms.FormatException The workbook is incompatible for this Adapter.
     */
    public void validate(Workbook workbook) throws FormatException;

}
