package com.hotan.army.supply.util;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by mhotan on 3/13/14.
 */
public class ImageConverter {

    private ImageConverter() {}

    public static final String IMAGE_FORMAT = "png";

    /**
     * Converts bytes to image.
     *
     * @param bytes Bytes that represent the image.
     * @return The JavaFX Image that is represented by these bytes.
     */
    public static Image toImage(byte[] bytes) {
        if (bytes == null) return null;
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        try {
            BufferedImage image = ImageIO.read(bais);
            return SwingFXUtils.toFXImage(image, null);
        } catch (IOException e) {
            throw new RuntimeException("");
        }
    }

    /**
     * Converts an image to a byte array.
     *
     * @param image Image to convert.
     * @return the byte array of the image.
     */
    public static byte[] toByteArray(Image image) {
        BufferedImage bufImage = SwingFXUtils.fromFXImage(image, null);
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        try {
            ImageIO.write(bufImage, IMAGE_FORMAT, byteStream);
            return byteStream.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException("Unable to right image " + image + " because of " + e);
        }
    }

}
