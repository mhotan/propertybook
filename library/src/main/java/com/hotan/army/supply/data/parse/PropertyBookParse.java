package com.hotan.army.supply.data.parse;

import com.hotan.army.supply.data.PropertyBook;
import com.hotan.army.supply.data.inspection.DeficiencyEntry;
import com.hotan.army.supply.data.items.NonSerializedEndItem;
import com.hotan.army.supply.data.items.SerializedEndItem;
import com.hotan.army.supply.data.items.components.EndItemBasicIssueComponent;
import com.hotan.army.supply.data.items.components.EndItemComponent;
import com.hotan.army.supply.data.items.itemdata.EndItemData;
import com.hotan.army.supply.data.location.Location;
import com.hotan.army.supply.data.personnel.Operator;
import com.hotan.army.supply.data.tracking.SignedOutRecord;
import org.parse4j.util.ParseRegistry;

/**
 * Created by mhotan on 3/21/14.
 */
public class PropertyBookParse {

    public static void registerClasses() {
        ParseRegistry.registerDefaultSubClasses();
        ParseRegistry.registerSubclass(DeficiencyEntry.class);
        ParseRegistry.registerSubclass(EndItemBasicIssueComponent.class);
        ParseRegistry.registerSubclass(EndItemComponent.class);
        ParseRegistry.registerSubclass(EndItemData.class);
        ParseRegistry.registerSubclass(NonSerializedEndItem.class);
        ParseRegistry.registerSubclass(SerializedEndItem.class);
        ParseRegistry.registerSubclass(Location.class);
        ParseRegistry.registerSubclass(Operator.class);
        ParseRegistry.registerSubclass(SignedOutRecord.class);
        ParseRegistry.registerSubclass(PropertyBook.class);
    }

}
