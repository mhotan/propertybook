package com.hotan.army.supply.data.location;

import com.hotan.army.supply.data.parse.DataManager;
import org.parse4j.ParseException;
import org.parse4j.ParseQuery;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by mhotan on 3/23/14.
 */
public class LocationManager extends DataManager<Location> {

    private static final Logger LOG = Logger.getLogger(LocationManager.class.getName());

    private static LocationManager instance;

    public static LocationManager getInstance() {
        if (instance == null)
            instance = new LocationManager();
        return instance;
    }

    private LocationManager() {
        super();

        // Fetch current Location on the cloud.
        ParseQuery<Location> query = ParseQuery.getQuery(Location.class);
        List<Location> locations = null;
        try {
            locations = query.find();
            if (locations == null || locations.isEmpty()) return;
            locations.stream().forEach(loc -> this.objects.add(loc));
        } catch (ParseException e) {
            LOG.warning(getClass().getSimpleName() + "() Unable to initially fetch Location");
        }

    }

    /**
     * Adds a location to Manager.
     *
     * @param name Name of the location to add.
     * @return Location instance.
     */
    public synchronized Location add(String name) throws ParseException {
        return add(name, 0.0, 0.0);
    }

    /**
     *  Add the location with name and coordinates.
     *
     * @param name Name of the location to add.
     * @param latitude Latitude of the location
     * @param longitude Longitude of the location.
     * @return Location added or stored.
     * @throws ParseException
     */
    public synchronized Location add(String name, double latitude, double longitude) throws ParseException {
        if (name == null || name.trim().isEmpty())
            throw new NullPointerException(getClass().getSimpleName() + "add(String name) " +
                    "Illegal location name \"" + name + "\"");
        name = name.trim();

        // Check if we have the location already.
        final String finalName = name.trim().toLowerCase();
        Location location = this.objects.stream().filter(loc ->
                loc.getName().toLowerCase().equals(finalName)).findFirst().orElse(null);
        if (location != null) {
            location.setLatitude(latitude);
            location.setLongitude(longitude);
            return location;
        }

        // Create and save a new location
        location = new Location(name);
        location.save();
        add(location);
        return location;
    }

    @Override
    protected Class<Location> getDataClass() {
        return Location.class;
    }

    /**
     * Gets the location with the name "n"
     *
     * @param n Name to find Location by.
     * @return Location with associated name.
     * @throws ParseException Could not pull from the cloud.
     */
    public synchronized Location getByName(String n) throws ParseException {
        if (n == null || n.trim().isEmpty())
            throw new IllegalArgumentException(getClass().getSimpleName() + "getByName(String name) " +
                    "Illegal name \"" + n.trim() + "\"");
        final String name = n.trim();

        // Check if we have the location locally.
        Location location = this.objects.stream().filter(loc -> name.equals(loc.getName())).findFirst().orElse(null);
        if (location != null)
            return location;

        // Find the location with the associated name.
        ParseQuery<Location> query = ParseQuery.getQuery(Location.class);
        query.whereEqualTo(Location.NAME, name);
        List<Location> locations = query.find();
        if (locations == null || locations.isEmpty()) return null;

        // fetch the location and add it to the internal runtime cache.
        locations.get(0).fetchIfNeeded();
        location = locations.get(0);
        add(location);
        return location;
    }

}
