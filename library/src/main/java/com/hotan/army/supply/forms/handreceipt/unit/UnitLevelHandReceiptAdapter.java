package com.hotan.army.supply.forms.handreceipt.unit;

import com.hotan.army.supply.data.items.EndItem;
import com.hotan.army.supply.data.items.itemdata.EndItemData;
import com.hotan.army.supply.data.personnel.Operator;
import com.hotan.army.supply.forms.FormAdapter;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.Date;

/**
 * Created by mhotan_dev on 1/10/14.
 */
public interface UnitLevelHandReceiptAdapter extends FormAdapter {

    public Date getDate(HSSFSheet sheet);
    public String getUIC(HSSFSheet sheet);
    public String getDESC(HSSFSheet sheet);
    public String getTeam(HSSFSheet sheet);
    public Operator getWhoFrom(HSSFSheet sheet);
    public Operator getWhoTo(HSSFSheet sheet);

    /**
     * Process the workbook and extract all the EndItemGroups contained.
     *
     * @param workbook Workbook to process.
     * @return All the EndItem groups.
     */
    ObservableMap<EndItemData, ObservableList<EndItem>> processHandReceipt(Workbook workbook);

}
