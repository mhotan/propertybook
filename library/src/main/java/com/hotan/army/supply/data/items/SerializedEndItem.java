package com.hotan.army.supply.data.items;

import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.parse4j.ParseClassName;

/**
 * A class that
 *
 * @author Michael Hotan
 * Created by Michael Hotan on 3/7/14.
 */
@ParseClassName("SerializedEndItem")
public class SerializedEndItem extends EndItem {

    private static final String SERIAL_NUMBER = "serialNumber";

    private final StringProperty serialNumber;

    /*
    Representation Invariant:
    serialNumber.get() is not null and is not empty

    Abstract Function:
    serialNumber is the serial number of this unique end item.
     */

    public SerializedEndItem() {
        super();
        this.serialNumber = new SimpleStringProperty();

        this.serialNumber.addListener((ov, oldVal, newVal) -> {
            put(SERIAL_NUMBER, serialNumber);
        });
    }

    /**
     * Creates a serialized End Item.
     *
     * @param serialNumber Serial Number of this End Item.
     */
    public SerializedEndItem(String serialNumber, String nsn, String lin) {
        this(serialNumber, "", nsn, lin);
    }

    /**
     * Creates a new Serialized end item.
     *
     * @param serialNumber Serial number of this End Item.
     * @param clNumber CL number.
     */
    public SerializedEndItem(String serialNumber, String clNumber, String nsn, String lin) {
        super(clNumber, nsn, lin);

        this.serialNumber = new SimpleStringProperty();
        this.serialNumber.addListener((ov, oldVal, newVal) -> {
            put(SERIAL_NUMBER, serialNumber);
        });
        this.serialNumber.set(serialNumber);
    }

    @Override
    public String getNameLabel() {
        return "SN";
    }

    @Override
    public ReadOnlyStringProperty nameProperty() {
        if (serialNumber.isNull().get()) {
            serialNumber.set(getString(SERIAL_NUMBER));
        }
        return serialNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SerializedEndItem)) return false;
        if (!super.equals(o)) return false;

        SerializedEndItem that = (SerializedEndItem) o;
        if (!serialNumber.isEqualTo(that.serialNumber).get()) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + serialNumber.get().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "SerializedEndItem{" +
                "serialNumber=" + serialNumber + ", " + super.toString() + '}';
    }
}
