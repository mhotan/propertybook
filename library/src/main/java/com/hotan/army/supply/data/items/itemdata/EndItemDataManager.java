package com.hotan.army.supply.data.items.itemdata;

import com.hotan.army.supply.data.parse.DataManager;
import org.parse4j.ParseException;
import org.parse4j.ParseQuery;

import java.util.List;

/**
 * Created by mhotan on 3/23/14.
 */
public class EndItemDataManager extends DataManager<EndItemData> {

    private static EndItemDataManager instance;

    public static EndItemDataManager getInstance() {
        if (instance == null)
            instance = new EndItemDataManager();
        return instance;
    }

    /**
     * Private singleton constructor.
     */
    private EndItemDataManager() {
        super();
    }

    @Override
    protected Class<EndItemData> getDataClass() {
        return EndItemData.class;
    }

    /**
     * Obtains EndItem Data from this.
     *
     * @param nsn NSN of the End item itemdata
     * @param lin LIN of the End item itemdata.
     * @return The End Item Data instance that has a matching NSN and LIN. NUll if none exists.
     * @throws ParseException Exception when querying the cloud.
     */
    public synchronized EndItemData get(String nsn, String lin) throws ParseException {
        if (nsn == null || nsn.trim().isEmpty() || lin == null || lin.trim().isEmpty())
            throw new IllegalArgumentException(getClass().getSimpleName() + "get() " +
                    "Illegal Key NSN: \"" + nsn + "\" LIN: \"" + lin + "\"");
        final String nsnKey = nsn.trim();
        final String linKey = lin.trim();

        // Check if the EndItem Data is found locally.
        EndItemData foundData = this.objects.stream().filter(
                data -> data.getNSN().equals(nsnKey) && data.getLIN().equals(linKey)).
                findAny().orElse(null);
        if (foundData != null)
            return foundData;

        // Create a query based on the current
        ParseQuery<EndItemData> query = ParseQuery.getQuery(EndItemData.class);
        query.whereEqualTo(EndItemData.NSN_KEY, nsn);
        query.whereEqualTo(EndItemData.LIN_KEY, lin);
        List<EndItemData> found = query.find();

        // Unable to find
        if (found == null) return null;

        // Pull the itemdata from the cloud.
        found.get(0).fetchIfNeeded();
        foundData = found.get(0);
        add(foundData);
        return foundData;
    }

}
