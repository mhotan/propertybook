package com.hotan.army.supply.data;

import com.hotan.army.supply.data.parse.FXParseObject;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.parse4j.ParseClassName;
import org.parse4j.ParseException;
import org.parse4j.ParseObject;
import org.parse4j.ParseQuery;

import java.util.List;

/**
 * Created by mhotan on 3/21/14.
 */
@ParseClassName("A")
public class TestClassA extends FXParseObject {

    private static final String NAME = "name";
    private static final String VALUE = "value";
    private static final String INTERNAL = "internal";
    private static final String BELONGS_TO = "belongsTo";

    private final StringProperty name;

    private final ObjectProperty<TestClassB> internal;

    private final ObservableList<TestClassB> internals;

    public TestClassA() {
        name = new SimpleStringProperty();
        internal = new SimpleObjectProperty<TestClassB>();
        internals = FXCollections.observableArrayList();
    }

    @Override
    public void save() throws ParseException {
        if (internal.isNotNull().get()) {
            TestClassB b = internal.get();
            if (b.getObjectId() == null)
                b.save();
            put(INTERNAL, FXParseObject.createWithoutData(TestClassB.class, b.getObjectId()));
        }
        super.save();

        for (TestClassB b: internals) {
            b.put(BELONGS_TO, this);
            b.save();
        }
    }

    @Override
    public void delete() throws ParseException {
        if (internal.isNotNull().get())
            internal.get().delete();

        for (TestClassB b: internals)
            b.delete();
        super.delete();
    }

    @Override
    public TestClassA fetchIfNeeded() throws ParseException {
        TestClassA thisclass = super.fetchIfNeeded();
        if (this.internals.isEmpty()) { // If we need to fetch itemdata
            ParseQuery<TestClassB> query = ParseQuery.getQuery(TestClassB.class);
            query.whereEqualTo(BELONGS_TO, this);
            List<TestClassB> bs = query.find();
            this.internals.addAll(bs);
        }
        return this;
    }

    public void addB(TestClassB b) {
        if (b == null) return;
        internals.add(b);
    }

    public boolean removeB(TestClassB b) {
        if (internals.remove(b)) {
            if (this.equals(b.get(BELONGS_TO)))
                b.remove(BELONGS_TO);
            return true;
        }
        return false;
    }

    public ObservableList<TestClassB> getInternals() {
        return internals;
    }

    public void setName(String name) {
        this.name.set(name);
        if (name != null)
            put(NAME, name);
        else
            remove(NAME);
    }

    public void setInternal(TestClassB internal) {
        this.internal.set(internal);
        if (internal == null)
            remove(INTERNAL);
    }

    public String getName() {
        return nameProperty().get();
    }

    public TestClassB getInternal() {
        return internalProperty().get();
    }

    public StringProperty nameProperty() {
        if (name.isNull().get()) {
            name.set(getString(NAME));
        }
        return name;
    }

    public ObjectProperty<TestClassB> internalProperty() {
        if (internal.isNull().get()) {
            ParseObject po = getParseObject(INTERNAL);
            if (po != null) {
                TestClassB b = new TestClassB();
                b.setObjectId(po.getObjectId());
                try {
                    b.fetchIfNeeded();
                } catch (ParseException e) {}
                internal.set(b);
            }
        }
        return internal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TestClassA)) return false;

        TestClassA that = (TestClassA) o;

        if (!internal.equals(that.internal)) return false;
        if (!name.equals(that.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + internal.hashCode();
        return result;
    }

}
