package com.hotan.army.supply.data;

import com.hotan.army.supply.data.parse.FXParseObject;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.parse4j.ParseClassName;

/**
 * Created by mhotan on 3/21/14.
 */
@ParseClassName("B")
public class TestClassB extends FXParseObject {

    private static final String NAME = "name";
    private static final String VALUE = "value";

    private final StringProperty name;

    private final StringProperty value;

    public TestClassB() {
        name = new SimpleStringProperty();
        value = new SimpleStringProperty();
    }

    public void setName(String name) {
        this.name.set(name);
        if (name != null)
            put(NAME, name);
        else
            remove(NAME);
    }

    public void setValue(String value) {
        this.value.set(value);
        if (value != null)
            put(VALUE, value);
        else
            remove(VALUE);
    }

    public String getName() {
        return name.get();
    }

    public String getValue() {
        return value.get();
    }

    public StringProperty nameProperty() {
        if (name.isNull().get()) {
            name.set(getString(NAME));
        }
        return name;
    }

    public StringProperty valueProperty() {
        if (value.isNull().get()) {
            value.set(getString(VALUE));
        }
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TestClassB)) return false;

        TestClassB that = (TestClassB) o;

        if (!nameProperty().isEqualTo(that.nameProperty()).get()) return false;
        if (!valueProperty().isEqualTo(that.valueProperty()).get()) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + value.hashCode();
        return result;
    }

}
