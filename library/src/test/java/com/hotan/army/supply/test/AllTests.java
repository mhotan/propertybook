package com.hotan.army.supply.test;

import com.hotan.army.supply.test.data.PropertyBookTest;
import com.hotan.army.supply.test.data.items.EndItemTest;
import com.hotan.army.supply.test.forms.coverpage.CoverPageTest;
import com.hotan.army.supply.test.forms.handreceipt.subcomponent.ComponentHRTest;
import com.hotan.army.supply.test.forms.handreceipt.unit.UnitHRTest;
import com.hotan.army.supply.test.util.TemplateLoaderTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({ ComponentHRTest.class, CoverPageTest.class, EndItemTest.class,
        TemplateLoaderTest.class, UnitHRTest.class, PropertyBookTest.class })
public class AllTests {

}
