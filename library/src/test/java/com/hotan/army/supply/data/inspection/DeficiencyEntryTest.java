package com.hotan.army.supply.data.inspection;

import com.hotan.army.supply.ApplicationTest;
import junit.framework.TestCase;
import org.junit.Test;
import org.parse4j.ParseException;
import org.parse4j.ParseObject;
import org.parse4j.ParseQuery;

/**
 * Created by mhotan on 3/11/14.
 */
public class DeficiencyEntryTest extends TestCase {

    @Test
    public void testParse() throws ParseException {
        ApplicationTest.initialize();
        DeficiencyEntry entry = new DeficiencyEntry("TM", "defs", "Corrective Actions", DeficiencyEntry.STATUS.OK);
        entry.save();
        String id = entry.getObjectId();
        ParseQuery<ParseObject> query = ParseQuery.getQuery(DeficiencyEntry.class.getSimpleName());
        ParseObject parseObject = query.get(id);
        assertNotNull("Parse Object cannot be null", parseObject);
        entry.delete();

    }

}
