package com.hotan.army.supply.test.forms.handreceipt.unit;

import com.hotan.army.supply.ApplicationTest;
import com.hotan.army.supply.data.personnel.Operator;
import com.hotan.army.supply.data.personnel.OperatorManager;
import com.hotan.army.supply.data.personnel.Rank;
import com.hotan.army.supply.forms.FormatException;
import com.hotan.army.supply.forms.handreceipt.unit.UnitLevelHandReceipt;
import com.hotan.army.supply.util.loader.POILoader;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.parse4j.ParseException;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;


public class UnitHRTest {

    private static final int NUM_ITEMS = 49;
    
    private static UnitLevelHandReceipt unitHandReceipt;

    private OperatorManager manager;

    @BeforeClass
    public static void setupClass() throws IOException, FormatException {
        HSSFWorkbook wb = POILoader.getXLSWorkbook("9111_UNIT_HR.xls");
        unitHandReceipt = new UnitLevelHandReceipt(wb);
    }

    @Before
    public void setup() throws ParseException {
        manager = OperatorManager.getInstance();
    }

    @After
    public void tearDown() {

    }
    
    @Test
    public void testValidDate() {
        Date datePrep = unitHandReceipt.getDatePrepared();
        Calendar cal = Calendar.getInstance();
        cal.clear();
        cal.set(2013, 8, 9);
        assertEquals("Date equals correct value", cal.getTime(), datePrep);
    }
    
    @Test
    public void testValidUIC() {
        assertEquals("InCorrect UIC", "WTN6A0", unitHandReceipt.getUIC());
    }

    @Test
    public void testValidDESC() {
        assertEquals("InCorrect DESC", "1ST BN, 19TH SFG, CO A", unitHandReceipt.getDESC());
    }
    
    @Test
    public void testTeam() {
        assertEquals("InCorrect team", "911", unitHandReceipt.getTeam());
    }
    
    @Test
    public void testFrom() {
        ApplicationTest.initialize();
        Operator operator = unitHandReceipt.getWhoFrom();
        assertEquals("First name is correct", "JONATHAN", operator.getFirstName());
        assertEquals("Last name is correct", "TSCHETTER", operator.getLastName());
        assertEquals("Middle name is correct", "", operator.getMiddleName());
        assertEquals("Rank is correct", Rank.MAJ, operator.getRank());
        assertEquals("MOS is correct",  null, operator.getMOS());
    }
    
    @Test
    public void testTo() {
        ApplicationTest.initialize();
        Operator operator = unitHandReceipt.getWhoTo();
        assertEquals("First name is correct", "JAMES", operator.getFirstName());
        assertEquals("Last name is correct", "MITCHELL", operator.getLastName());
        assertEquals("Middle name is correct", "", operator.getMiddleName());
        assertEquals("Rank is correct", Rank.CPT, operator.getRank());
        assertEquals("MOS is correct",  null, operator.getMOS());
    }
    
    @Test
    public void testCorrectNumEndItems() {
        assertEquals("Correct number of Enditems", NUM_ITEMS, unitHandReceipt.getItems().size());
    }
    
    @Test 
    public void testNumberOfGPSs() {
        assertEquals("Incorrect number of GPSs", 12, unitHandReceipt.getItems("5825015795635", "09065N").size());
    }

    @Test
    public void testNumberOfAntennaBaseRadio() {
        assertEquals("Incorrect number of Antenna Base Radios", 1, unitHandReceipt.getItems("589501D047479", "70203N").size());
    }

    @Test
    public void testNumberOfAntenna() {
        assertEquals("Incorrect number of Antennas", 1, unitHandReceipt.getItems("5985015577550", "70203N").size());
    }

    @Test
    public void testNumberOfBatteryChargers() {
        assertEquals("Incorrect number of Battery Chargers", 1, unitHandReceipt.getItems("6130015043675", "70208N").size());
    }

    @Test
    public void testNumberOfComputerSystemsDigital() {
        assertEquals("Incorrect number of Digital Computer Systems", 1,
                unitHandReceipt.getItems("7010015230185", "70210N").size());
    }

    @Test
    public void testNumberOfMicroLapTopPortablel() {
        assertEquals("Incorrect number of Digital Computer Systems", 2,
                unitHandReceipt.getItems("701001C010367", "70210N").size());
    }
}
