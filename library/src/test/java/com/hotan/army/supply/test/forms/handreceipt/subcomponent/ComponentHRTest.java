package com.hotan.army.supply.test.forms.handreceipt.subcomponent;

import com.hotan.army.supply.data.items.EndItem;
import com.hotan.army.supply.data.items.itemdata.EndItemData;
import com.hotan.army.supply.forms.FormatException;
import com.hotan.army.supply.forms.handreceipt.subcomponent.ComponentHandReceipt;
import com.hotan.army.supply.forms.handreceipt.unit.UnitLevelHandReceipt;
import com.hotan.army.supply.util.loader.POILoader;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class ComponentHRTest {

    // We don't have sub component list for every thing.
    private static final int NUM_ITEMS = 29;

    private static ComponentHandReceipt HR;
    private static UnitLevelHandReceipt UnitHR;

    @BeforeClass
    public static void setupClass() throws IOException, FormatException {
        HSSFWorkbook wb = POILoader.getXLSWorkbook("ComponentHandReceipt.xls");
        HR = new ComponentHandReceipt(wb);

        HSSFWorkbook wb2 = POILoader.getXLSWorkbook("9111_UNIT_HR.xls");
        UnitHR = new UnitLevelHandReceipt(wb2);
    }

    @Test
    public void testNumEndItemGroups() {
        ObservableMap<EndItemData, ObservableList<EndItem>> groups = HR.getItems();
        assertEquals("Size not correct", NUM_ITEMS, groups.size());
    }

    @Test
    public void testAllComponentInUnit() {
        ObservableMap<EndItemData, ObservableList<EndItem>> compGroups = HR.getItems();
        compGroups.keySet().stream().forEach(data ->
                        assertTrue("Unit HR must have NSN: " + data.getNSN() + " LIN:" + data.getLIN(),
                                UnitHR.hasGroup(data.getNSN(), data.getLIN()))
        );
    }

    @Test
    public void testAllComponentInUnitCount() {
        ObservableMap<EndItemData, ObservableList<EndItem>> compMap = HR.getItems();
        ObservableMap<EndItemData, ObservableList<EndItem>> unitMap = UnitHR.getItems();
        compMap.keySet().stream().forEach(data -> {
            assertTrue("Unit Data does not exists in unit hand receipt", unitMap.containsKey(data));
            ObservableList<EndItem> unitItems = unitMap.get(data);
            assertEquals("Number of end items not equal", compMap.get(data).size(), unitItems.size());
        });

    }

}
