package com.hotan.army.supply.test.data;

import com.hotan.army.supply.ApplicationTest;
import com.hotan.army.supply.data.PropertyBook;
import com.hotan.army.supply.data.PropertyBookManager;
import com.hotan.army.supply.exceptions.propertybook.PropertyBookException;
import com.hotan.army.supply.forms.FormatException;
import com.hotan.army.supply.forms.handreceipt.subcomponent.ComponentHandReceipt;
import com.hotan.army.supply.forms.handreceipt.unit.UnitLevelHandReceipt;
import com.hotan.army.supply.util.loader.POILoader;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.parse4j.ParseException;

import java.io.IOException;

public class PropertyBookTest {

    private static ComponentHandReceipt HR;
    private static UnitLevelHandReceipt UnitHR;

    private static PropertyBook PB;

    private void setUpParse() {
        ApplicationTest.initialize();
    }


    @BeforeClass
    public static void setupClass() throws IOException, FormatException {
        HSSFWorkbook wb = POILoader.getXLSWorkbook("ComponentHandReceipt.xls");
        HR = new ComponentHandReceipt(wb);

        HSSFWorkbook wb2 = POILoader.getXLSWorkbook("9111_UNIT_HR.xls");
        UnitHR = new UnitLevelHandReceipt(wb2);
    }

    @Before
    public void setup() {
        setUpParse();
    }

    @Test
    public void tesynchronizedListstSize() throws PropertyBookException, ParseException {
        // test via callback.
        PB = PropertyBookManager.getInstance().addBook("test book", UnitHR, HR);
        Assert.assertEquals(49, PB.getGroups().size());
        Assert.assertEquals(100, PB.getEndItems().size());
        PB.deleteInBackground();

    }

}
