package com.hotan.army.supply.data;

import com.hotan.army.supply.ApplicationTest;
import javafx.collections.ObservableList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.parse4j.ParseException;
import org.parse4j.ParseQuery;
import org.parse4j.util.ParseRegistry;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by mhotan on 3/21/14.
 */
public class ParseObjectsTest {

    TestClassA a;
    TestClassB b;

    private void setUpParse() {
        ParseRegistry.registerSubclass(TestClassA.class);
        ParseRegistry.registerSubclass(TestClassB.class);
        ApplicationTest.initialize();
    }

    @Before
    public void setUp() throws Exception {
        setUpParse();
        a = new TestClassA();
        a.setName("A");
        b = new TestClassB();
        b.setName("B");
        b.setValue("Random Value");
    }

    @After
    public void tearDown() throws Exception {
        try {
            a.delete();
        } catch (ParseException e) {}
        try {
            b.delete();
        } catch (ParseException e) {}
    }

    @Test
    public void testNestingObjects() throws ParseException {
        for (int i = 0; i < 1000; i++) {
            TestClassB b = new TestClassB();
            b.setName("" + (i + 1));
            b.setValue("Value: " + (i + 1));
            a.addB(b);
        }
        a.save();

        ParseQuery<TestClassA> query = ParseQuery.getQuery(TestClassA.class);
        List<TestClassA> as = query.find();
        TestClassA aCopy = as.get(0);

        ObservableList list = aCopy.getInternals();

        assertEquals(a.getInternals().size(), list.size());
    }

    @Test
    public void testCreatingNestedObject() throws Exception {
        a.setInternal(b);
        a.save();

        ParseQuery<TestClassA> query = ParseQuery.getQuery(TestClassA.class);
        List<TestClassA> as = query.find();
        TestClassA aCopy = as.get(0);

        TestClassB bCopy = aCopy.getInternal();
        assertEquals(b, bCopy);
    }

}
