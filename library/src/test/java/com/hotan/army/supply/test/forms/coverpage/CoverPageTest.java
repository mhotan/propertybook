package com.hotan.army.supply.test.forms.coverpage;

import com.hotan.army.supply.data.items.EndItem;
import com.hotan.army.supply.data.items.NonSerializedEndItem;
import com.hotan.army.supply.data.items.itemdata.EndItemData;
import com.hotan.army.supply.data.personnel.MOS;
import com.hotan.army.supply.forms.FormatException;
import com.hotan.army.supply.forms.coverpage.EndItemCoverPage;
import com.hotan.army.supply.util.POIUtil;
import com.hotan.army.supply.util.SheetCopier;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


public class CoverPageTest {

    private static final String DIR = "bin/test/" + CoverPageTest.class.getSimpleName();
    
    @BeforeClass
    public static void setup() throws IOException {
        Files.createDirectories(Paths.get(DIR));
    }
    
    @Test
    public void test() throws IOException, FormatException {
        HSSFWorkbook book = new HSSFWorkbook();
        book.createSheet("test");
        HSSFSheet sheet = book.getSheet("test");
        POIUtil.fitSheetToPage(sheet);
        
        EndItemData data = new EndItemData("nsn", "lin");
        data.addName("test");

        EndItemCoverPage cp = new EndItemCoverPage();
        cp.setData(data, FXCollections.observableArrayList());
        
        SheetCopier.copySheets(sheet, cp.getSheet());
        File f = new File(DIR + "/" + "test.xls");
        if (f.exists())
            f.delete();
        f.createNewFile();
        
        book.write(new FileOutputStream(f));
    }
    
    @Test
    public void testSave() throws FormatException, IOException {
        EndItemData data = new EndItemData("nsn", "lin");
        data.addName("test");
        data.setAssignedMOS(MOS.BRAVO);

        ObservableList<EndItem> items = FXCollections.observableArrayList();
        items.add(new NonSerializedEndItem("1", "nsn", "lin"));
        items.add(new NonSerializedEndItem("2", "nsn", "lin"));
        items.add(new NonSerializedEndItem("3", "nsn", "lin"));
        items.add(new NonSerializedEndItem("4", "nsn", "lin"));
        
        EndItemCoverPage cp = new EndItemCoverPage();
        cp.setData(data, items);
        cp.save(DIR + "/", "test2.xls");
    }

}
