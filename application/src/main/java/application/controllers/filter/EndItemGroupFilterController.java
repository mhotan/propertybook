package application.controllers.filter;

import application.controllers.BaseController;
import com.hotan.army.supply.data.EndItemGroup;
import com.hotan.army.supply.data.items.SerializedEndItem;
import com.hotan.army.supply.data.personnel.MOS;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.GridPane;
import org.controlsfx.control.CheckComboBox;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 *
 *
 * @author Michael Hotan, michael.hotan@gmail.com
 */
public class EndItemGroupFilterController extends BaseController {

    @FXML
    private GridPane grid;

    @FXML
    private CheckBox serializedCheckBox, availableItemsCheckBox, notAvailableCheckBox;


    private final FilterChangedListener<EndItemGroup> listener;

    private final CheckComboBox<MOS> mosCheckComboBox;

    private final ObjectProperty<Predicate<EndItemGroup>> predicate;

    public EndItemGroupFilterController(FilterChangedListener<EndItemGroup> listener) {
        this.listener = listener;
        this.mosCheckComboBox = new CheckComboBox<>(MOS.MOSES);
        this.mosCheckComboBox.maxWidth(Double.MAX_VALUE);
        this.predicate = new SimpleObjectProperty<>();
    }


    @FXML
    protected void initialize() {
        assert serializedCheckBox != null : "fx:id=\"serializedCheckBox\" was not injected: check your FXML file 'EndItemGroupFilter.fxml'.";
        assert grid != null : "fx:id=\"grid\" was not injected: check your FXML file 'EndItemGroupFilter.fxml'.";
        assert notAvailableCheckBox != null : "fx:id=\"notAvailableCheckBox\" was not injected: check your FXML file 'EndItemGroupFilter.fxml'.";
        assert availableItemsCheckBox != null : "fx:id=\"availableItemsCheckBox\" was not injected: check your FXML file 'EndItemGroupFilter.fxml'.";
        super.initialize();
    }

    @Override
    protected void setUpUI() {
        // Add the check combo box to the
        grid.add(mosCheckComboBox, 1, 1);

        // Add a listener for all components.
        mosCheckComboBox.getCheckModel().selectedItemProperty().addListener((ov, oldMOS, newMOS) -> udpatePredicate());
        ChangeListener<Boolean> checkBoxChangeListener = (ov, oldBoolean, newBoolean) -> udpatePredicate();
        serializedCheckBox.selectedProperty().addListener(checkBoxChangeListener);
        availableItemsCheckBox.selectedProperty().addListener(checkBoxChangeListener);
        notAvailableCheckBox.selectedProperty().addListener(checkBoxChangeListener);

        availableItemsCheckBox.selectedProperty().addListener((ov, oldVal, newVal) -> {
            if (newVal)
                notAvailableCheckBox.setSelected(false);
        });

        notAvailableCheckBox.selectedProperty().addListener((ov, oldVal, newVal) -> {
            if (newVal)
                availableItemsCheckBox.setSelected(false);
        });
    }

    /**
     * Notifies the listener with the current predicate.
     */
    private void udpatePredicate() {
        // Values could be null
        List<MOS> checkedMOS = mosCheckComboBox.getCheckModel().getSelectedItems();
        boolean serialized = serializedCheckBox.isSelected();
        boolean available = availableItemsCheckBox.isSelected();
        boolean nonAvailable = notAvailableCheckBox.isSelected();

        // Create new predicate and obtain a reference to old one.
        Predicate<EndItemGroup> oldPred = predicate.get();
        predicate.set(new EndItemGroupPredicate(checkedMOS, serialized, available, nonAvailable));

        // Notify the listener.
        listener.changed(predicate, oldPred, predicate.get());
    }

    private static class EndItemGroupPredicate implements Predicate<EndItemGroup> {

        private final List<MOS> moses;
        private final boolean serialized, availableItems, notAvailableItems;

        EndItemGroupPredicate(List<MOS> moses, boolean serialized, boolean availableItems, boolean notAvailableItems) {
            if (moses != null)
                this.moses = new ArrayList<MOS>(moses);
            else
                this.moses = new ArrayList<MOS>();
            this.serialized = serialized;
            this.availableItems = availableItems;
            this.notAvailableItems = notAvailableItems;
        }

        @Override
        public boolean test(EndItemGroup endItemGroup) {
            boolean pass = true;

            // Check whether the end item group is assigned to a filtered MOS
            MOS mos = endItemGroup.getData().getAssignedMOS();
            if (!moses.isEmpty()) {
                pass &= moses.contains(mos);
            }

            if (serialized && !endItemGroup.getItems().isEmpty()) {
                pass &= (endItemGroup.getItems().get(0) instanceof SerializedEndItem);
            }

            if (availableItems) {
                pass &= endItemGroup.getItems().stream().anyMatch(item -> item.getCurrentSignedOutRecord() == null);
            }

            if (notAvailableItems) {
                pass &= endItemGroup.getItems().stream().anyMatch(item -> item.getCurrentSignedOutRecord() != null);
            }

            return pass;
        }
    }
}
