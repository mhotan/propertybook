package application.controllers.filter;

import javafx.beans.value.ChangeListener;

import java.util.function.Predicate;

/**
 * Created by mhotan on 3/20/14.
 */
public interface FilterChangedListener<T> extends ChangeListener<Predicate<T>> {
}
