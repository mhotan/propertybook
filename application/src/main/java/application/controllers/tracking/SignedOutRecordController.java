package application.controllers.tracking;

import application.controllers.BaseController;
import application.controllers.items.location.LocationController;
import application.views.location.LocationView;
import com.hotan.army.supply.data.location.LocatedAt;
import com.hotan.army.supply.data.personnel.Operator;
import com.hotan.army.supply.data.personnel.OperatorManager;
import com.hotan.army.supply.data.tracking.SignedOutRecord;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

import java.time.LocalDate;
import java.util.logging.Logger;

/**
 * Created by mhotan on 3/24/14.
 */
public class SignedOutRecordController extends BaseController {

    private static final Logger LOG = Logger.getLogger(SignedOutRecordController.class.getName());

    private static final String CREATE_NEW_RECORD = "Create a new Record";
    private static final String UPDATE_RECORD = "Update Record";

    @FXML
    private HBox operatorsSelectionPane, locationPane, datePane;

    @FXML
    private ComboBox<Operator> operatorComboBox;

    @FXML
    private Label signedOutLabel, signedInLabel, recordLabel;

    @FXML
    private Button saveRecordButton;

    @FXML
    private Pane saveButtonFillerPane;

    @FXML
    private DatePicker signedInDatePicker, signedOutDatePicker;


    private final ObjectProperty<SignedOutRecord> record;

    private final LocationController locationController;

    /**
     * Create a signed out record controller.
     */
    public SignedOutRecordController() {
        locationController = new LocationController();
        record = new SimpleObjectProperty<>();
    }

    /**
     * Set the current record to show.
     *
     * @param record Record to set view to, or null if the desire is to allow the user to create a new one.
     */
    public void setRecord(SignedOutRecord record) {
        this.record.set(record);
    }

    public SignedOutRecord getRecord() {
        return record.get();
    }

    public ObjectProperty<SignedOutRecord> recordProperty() {
        return record;
    }

    @FXML
    void onSaveRecord(ActionEvent event) {
        // Obtain the mandatory field to sign out an object
        Operator operator = operatorComboBox.getValue();
        LocalDate signedOut = signedOutDatePicker.getValue();

        // Extract the location.
        LocatedAt locatedAt = locationController.getLocation();

        SignedOutRecord record = new SignedOutRecord(operator, signedOut);
        record.setLocation(locatedAt);

        // TODO Disable all the controllers and attempt to save in background thread
        // Once done saving set the view to this record
        // TODO Move to event handler after back ground thread is complete.
        setRecord(record);
    }

    @FXML
    protected void initialize() {
        assert operatorsSelectionPane != null : "fx:id=\"operatorsSelectionPane\" was not injected: check your FXML file 'SignedOutRecord.fxml'.";
        assert operatorComboBox != null : "fx:id=\"operatorComboBox\" was not injected: check your FXML file 'SignedOutRecord.fxml'.";
        assert signedOutLabel != null : "fx:id=\"signedOutLabel\" was not injected: check your FXML file 'SignedOutRecord.fxml'.";
        assert saveRecordButton != null : "fx:id=\"saveRecordButton\" was not injected: check your FXML file 'SignedOutRecord.fxml'.";
        assert locationPane != null : "fx:id=\"locationPane\" was not injected: check your FXML file 'SignedOutRecord.fxml'.";
        assert saveButtonFillerPane != null : "fx:id=\"saveButtonFillerPane\" was not injected: check your FXML file 'SignedOutRecord.fxml'.";
        assert signedInLabel != null : "fx:id=\"signedInLabel\" was not injected: check your FXML file 'SignedOutRecord.fxml'.";
        assert signedInDatePicker != null : "fx:id=\"signedInDatePicker\" was not injected: check your FXML file 'SignedOutRecord.fxml'.";
        assert recordLabel != null : "fx:id=\"recordLabel\" was not injected: check your FXML file 'SignedOutRecord.fxml'.";
        assert datePane != null : "fx:id=\"datePane\" was not injected: check your FXML file 'SignedOutRecord.fxml'.";
        assert signedOutDatePicker != null : "fx:id=\"signedOutDatePicker\" was not injected: check your FXML file 'SignedOutRecord.fxml'.";
        super.initialize();
    }

    @Override
    protected void setUpUI() {

        // Set the combobox to the current list of Operators.
        operatorComboBox.setItems(OperatorManager.getInstance().getAll());

        // Bind the filler pane the button So that the label is centered
        saveButtonFillerPane.visibleProperty().bindBidirectional(saveButtonFillerPane.managedProperty());
        saveButtonFillerPane.visibleProperty().bind(saveRecordButton.visibleProperty());
        saveButtonFillerPane.prefWidthProperty().bind(saveRecordButton.widthProperty());

        // Make sure the save record button is available when there isn't a save record.
        saveRecordButton.visibleProperty().bindBidirectional(saveRecordButton.managedProperty());
        // Save should be available when
        saveRecordButton.visibleProperty().bind(
                record.isNull().and(
                operatorComboBox.getSelectionModel().selectedItemProperty().isNotNull().and(
                        signedOutDatePicker.valueProperty().isNotNull()))
        );

        // Create the location view
        LocationView locationView = new LocationView(locationController);
        locationView.setMaxWidth(Double.MAX_VALUE);
        locationPane.getChildren().add(locationView);

        // Make sure the date picker is visible only when there is a record.
        signedInDatePicker.visibleProperty().bind(record.isNotNull());

        // Make sure the the combo box and date picker is not editable when there is a record
        operatorComboBox.disableProperty().bind(record.isNotNull());
        signedOutDatePicker.disableProperty().bind(record.isNotNull());

        record.addListener((ov, oldVal, newVal) -> {
            if (oldVal != null) {
                // Disable bindings
                operatorComboBox.getSelectionModel().clearSelection();
                locationController.locationProperty().unbindBidirectional(oldVal.locationProperty());
                signedOutDatePicker.valueProperty().unbind();
                signedInDatePicker.valueProperty().unbindBidirectional(oldVal.dateSignedInProperty());
            }
            if (newVal == null) {
                operatorComboBox.getSelectionModel().clearSelection();
                locationController.setLocation(null);
                signedOutDatePicker.setValue(LocalDate.now());
                signedInDatePicker.setValue(null);
                recordLabel.setText(CREATE_NEW_RECORD);
                return;
            }

            // Set the combo box the appropiate value
            operatorComboBox.getSelectionModel().select(newVal.getIndividual());
            locationController.locationProperty().bindBidirectional(newVal.locationProperty());
            signedOutDatePicker.valueProperty().bind(newVal.dateSignedOutProperty());
            signedInDatePicker.valueProperty().bindBidirectional(newVal.dateSignedInProperty());
            recordLabel.setText(UPDATE_RECORD);
        });
    }
}
