package application.controllers.main;

import application.controllers.main.details.DetailsPaneController;
import application.controllers.main.master.MasterPaneController;
import application.controllers.items.EndItemSearchController;
import application.controllers.operator.OperatorController;
import application.controllers.unit.UnitInfoController;
import application.views.DetailsPane;
import application.views.MasterPane;
import application.views.MasterDetailsView;
import application.views.items.EndItemSearchView;
import application.views.operator.OperatorView;
import application.views.unit.UnitInfoView;
import com.hotan.army.supply.data.PropertyBook;
import com.hotan.army.supply.data.personnel.Operator;

/**
 * Created by mhotan on 3/7/14.
 */
public class MasterDetailsController implements
        DetailsPaneController.DetailsPaneListener, MasterPaneController.MasterPaneListener {


    private final EndItemSearchView endItemsView;
    private final UnitInfoView unitInfoView;
    private final OperatorView operatorView;

    private final EndItemSearchController endItemSearchController;
    private final UnitInfoController unitInfoController;
    private final OperatorController operatorController;

    private final MasterPaneController masterPaneController;
    private final DetailsPaneController detailsPaneController;

    private MasterDetailsView root;

    private final PropertyBook pb;

    /**
     * Creates a controller for this property book.
     *
     *
     * @param pb Property book to use for itemdata.
     */
    public MasterDetailsController(PropertyBook pb) {
        if (pb == null) throw new NullPointerException("Null Propertybook");
        this.pb = pb;
        masterPaneController = new MasterPaneController(this);
        detailsPaneController = new DetailsPaneController(pb, this);
        endItemSearchController = new EndItemSearchController(pb);
        unitInfoController = new UnitInfoController(pb);
        operatorController = new OperatorController(pb);
        endItemsView = new EndItemSearchView(endItemSearchController);
        unitInfoView = new UnitInfoView(unitInfoController);
        operatorView = new OperatorView(operatorController);
    }

    @Override
    public void onEndItemsSelected() {
        if (root == null) return;
        masterPaneController.setContent(endItemsView);
    }

    @Override
    public void onUnitInfoSelected() {
        if (root == null) return;
        masterPaneController.setContent(unitInfoView);
    }

    @Override
    public void onOperatorsSelected() {
        if (root == null) return;
        masterPaneController.setContent(operatorView);
    }

    @Override
    public void onAddNewOperator() {
        if (root == null) return;
        // Make sure the operator view is showing.
        // Null out the operator so that the view adds a new one.
        operatorController.setOperator(null);
        masterPaneController.setContent(operatorView);
    }

    @Override
    public void onOperatorSelected(Operator operator) {
        if (root == null) return;
        // Make sure the operator view is showing.
        // set the operator so that the view adds a new one.
        operatorController.setOperator(operator);
        masterPaneController.setContent(operatorView);
    }

    @Override
    public boolean onToggleDetailsPane() {
        if (root == null) return false;
        root.setShowDetailNode(!root.isShowDetailNode());
        return root.isShowDetailNode();
    }

    public void setView(MasterDetailsView masterDetailsView) {
        masterDetailsView.setDividerPosition(.25);
        masterDetailsView.setDetailNode(new DetailsPane(detailsPaneController));
        masterDetailsView.setMasterNode(new MasterPane(masterPaneController));
        root = masterDetailsView;
    }
}
