package application.controllers.main.details;

import application.controllers.BaseController;
import application.util.cells.OperatorListCellFactory;
import com.hotan.army.supply.data.PropertyBook;
import com.hotan.army.supply.data.personnel.Operator;
import com.hotan.army.supply.data.personnel.OperatorManager;
import javafx.beans.binding.StringExpression;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.function.Predicate;
import java.util.logging.Logger;

/**
 * Created by mhotan on 3/20/14.
 */
public class DetailsPaneController extends BaseController {

    private static final Logger LOG  = Logger.getGlobal();

    @FXML
    private Accordion accordion;

    @FXML
    private TextArea uicArea, descArea, teamArea;

    @FXML
    private Label itemsOnHandLabel, itemsOnHandTitle, datelastUpdatedLabel, itemsSignedOutLabel,
            totalItemsLabel, itemsTurnedInLabel, itemsSignedOutTitle, totalItemsTitle;

    @FXML
    private TitledPane endItemsPane, unitInfoPane, operatorsPane;

    @FXML
    private ListView<Operator> operatorsList;

    @FXML
    private TextField operatorSearchField;

    @FXML
    private Button searchForOperator, addNewOperatorButton;

    /**
     *
     */
    private final PropertyBook propertyBook;

    private final DetailsPaneListener listener;

    private final FilteredList<Operator> operators;

    public DetailsPaneController(PropertyBook pb, DetailsPaneListener listener) {
        if (pb == null) throw new NullPointerException("Null Property Book");
        if (listener == null) throw new NullPointerException("Null DetailsPaneListener");
        this.propertyBook = pb;
        this.listener = listener;
        operators = new FilteredList<>(OperatorManager.getInstance().getAll(), o -> true);
    }

    @FXML
    void onSearchOperators(ActionEvent event) {
        refreshFilter();
    }

    private void refreshFilter() {
        final String filter = operatorSearchField.getText().toLowerCase().trim();
        operators.setPredicate(new Predicate<Operator>() {
            @Override
            public boolean test(Operator operator) {
                String firstName = operator.getFirstName().toLowerCase();
                String middleName = operator.getMiddleName().toLowerCase();
                String lastName = operator.getLastName().toLowerCase();

                return firstName.contains(filter) ||
                        middleName.contains(filter) ||
                        lastName.contains(filter);
            }
        });
    }

    @FXML
    void onAddNewOperator(ActionEvent event) {
        listener.onAddNewOperator();
    }

    @FXML
    protected void initialize() {
        assert accordion != null : "fx:id=\"accordion\" was not injected: check your FXML file 'DetailsPane.fxml'.";
        assert uicArea != null : "fx:id=\"uicArea\" was not injected: check your FXML file 'DetailsPane.fxml'.";
        assert descArea != null : "fx:id=\"descArea\" was not injected: check your FXML file 'DetailsPane.fxml'.";
        assert itemsOnHandTitle != null : "fx:id=\"itemsOnHandTitle\" was not injected: check your FXML file 'DetailsPane.fxml'.";
        assert datelastUpdatedLabel != null : "fx:id=\"datelastUpdatedLabel\" was not injected: check your FXML file 'DetailsPane.fxml'.";
        assert teamArea != null : "fx:id=\"teamArea\" was not injected: check your FXML file 'DetailsPane.fxml'.";
        assert itemsSignedOutLabel != null : "fx:id=\"itemsSignedOutLabel\" was not injected: check your FXML file 'DetailsPane.fxml'.";
        assert endItemsPane != null : "fx:id=\"endItemsPane\" was not injected: check your FXML file 'DetailsPane.fxml'.";
        assert operatorsList != null : "fx:id=\"operatorsList\" was not injected: check your FXML file 'DetailsPane.fxml'.";
        assert unitInfoPane != null : "fx:id=\"unitInfoPane\" was not injected: check your FXML file 'DetailsPane.fxml'.";
        assert searchForOperator != null : "fx:id=\"searchForOperator\" was not injected: check your FXML file 'DetailsPane.fxml'.";
        assert operatorsPane != null : "fx:id=\"operatorsPane\" was not injected: check your FXML file 'DetailsPane.fxml'.";
        assert totalItemsLabel != null : "fx:id=\"totalItemsLabel\" was not injected: check your FXML file 'DetailsPane.fxml'.";
        assert addNewOperatorButton != null : "fx:id=\"addNewOperatorButton\" was not injected: check your FXML file 'DetailsPane.fxml'.";
        assert operatorSearchField != null : "fx:id=\"operatorSearchField\" was not injected: check your FXML file 'DetailsPane.fxml'.";
        assert itemsTurnedInLabel != null : "fx:id=\"itemsTurnedInLabel\" was not injected: check your FXML file 'DetailsPane.fxml'.";
        assert itemsSignedOutTitle != null : "fx:id=\"itemsSignedOutTitle\" was not injected: check your FXML file 'DetailsPane.fxml'.";
        assert totalItemsTitle != null : "fx:id=\"totalItemsTitle\" was not injected: check your FXML file 'DetailsPane.fxml'.";
        super.initialize();
    }

    @Override
    protected void setUpUI() {
        accordion.expandedPaneProperty().addListener((pane, oldVal, newVal) -> {
            if (newVal == null) return; // Ignore null values.
            if (newVal.equals(operatorsPane))
                listener.onOperatorsSelected();
            else if (newVal.equals(unitInfoPane))
                listener.onUnitInfoSelected();
            else
                listener.onEndItemsSelected();
        });

        // Bind the operators list.
        operatorsList.setItems(operators);
        operatorsList.getSelectionModel().selectedItemProperty().addListener((ov, old, newVal)
                -> listener.onOperatorSelected(newVal));
        operatorsList.setCellFactory(new OperatorListCellFactory());

        // Bind the UIC, DESC, and team label.
        uicArea.textProperty().bind(propertyBook.UICProperty());
        descArea.textProperty().bind(propertyBook.DESCProperty());
        teamArea.textProperty().bind(propertyBook.teamProperty());

        // Update the last updated time.
        Date updatedAt = this.propertyBook.getUpdatedAt();
        if (updatedAt == null)
            updatedAt = Calendar.getInstance().getTime();
        datelastUpdatedLabel.setText(DateFormat.getInstance().format(updatedAt));

        // Bind the total list of end items
        totalItemsLabel.textProperty().bind(this.propertyBook.totalProperty().asString());
        itemsSignedOutLabel.textProperty().bind(
                this.propertyBook.totalProperty().subtract(
                        this.propertyBook.onHandProperty()).asString());
        itemsTurnedInLabel.textProperty().bind(this.propertyBook.onHandProperty().asString());

        // Expand and initial show the unit info pane.
        accordion.setExpandedPane(unitInfoPane);

        // Tool tips
        uicArea.setTooltip(new Tooltip("Unit Identification Code"));
        descArea.setTooltip(new Tooltip("Unit Description"));
        teamArea.setTooltip(new Tooltip("The team the property book belongs to"));
        operatorSearchField.setTooltip(new Tooltip("Type any part of the name to search for a specific operator"));
        StringExpression search = new SimpleStringProperty("Search for \"").concat(operatorSearchField.textProperty()).concat(new SimpleStringProperty("\""));
        Tooltip setTip = new Tooltip();
        setTip.textProperty().bind(search);
        searchForOperator.setTooltip(setTip);
        addNewOperatorButton.setTooltip(new Tooltip("Create a new Operator"));

        StringExpression totalString = new SimpleStringProperty("The total amount of End" +
                " Items that belong to ").concat(teamArea.textProperty());
        Tooltip totalTip = new Tooltip();
        totalTip.textProperty().bind(totalString);
        totalItemsTitle.setTooltip(totalTip);
        StringExpression itemsSignedOut = new SimpleStringProperty("\"The amount of items that are signed out to " +
                "people not in\"").concat(teamArea.textProperty());
        Tooltip signedOutTip = new Tooltip();
        signedOutTip.textProperty().bind(itemsSignedOut);
        itemsSignedOutTitle.setTooltip(signedOutTip);
        itemsOnHandTitle.setTooltip(new Tooltip("The amount of items that are currently not signed out to anyone"));
    }

    public interface DetailsPaneListener {

        void onEndItemsSelected();

        void onUnitInfoSelected();

        void onOperatorsSelected();

        void onAddNewOperator();

        /**
         * Notifies the an operator has been selected.
         *
         * @param operator The operator that is selected.
         */
        void onOperatorSelected(Operator operator);

    }
}
