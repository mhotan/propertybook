package application.controllers.main.master;

import application.controllers.BaseController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;

/**
 * Created by mhotan on 3/19/14.
 */
public class MasterPaneController extends BaseController {

    private static final String SLIDE_OUT = "<<";
    private static final String SLIDE_IN = ">>";

    @FXML
    private Button toggleDetailsPaneButton;

    @FXML
    private ScrollPane content;

    private MasterPaneListener listener;

    public MasterPaneController(MasterPaneListener listener) {
        if (listener == null)
            throw new NullPointerException();
        this.listener = listener;
    }

    @FXML
    void onToggleDetailsPane(ActionEvent event) {
        if (listener.onToggleDetailsPane()) { // Details pane is showing
            toggleDetailsPaneButton.setText(SLIDE_OUT);
        } else {
            toggleDetailsPaneButton.setText(SLIDE_IN);
        }
    }

    @FXML
    protected void initialize() {
        assert toggleDetailsPaneButton != null : "fx:id=\"toggleDetailsPaneButton\" was not injected: check your FXML file 'MasterPane.fxml'.";
        assert content != null : "fx:id=\"content\" was not injected: check your FXML file 'MasterPane.fxml'.";
        super.initialize();
    }

    @Override
    protected void setUpUI() {

    }

    public void setContent(Node content) {
        if (content == null) return;
        if (this.content.getContent() == content) return;
        this.content.setContent(content);
    }

    public interface MasterPaneListener {

        /**
         * Toggles the detail pane between show and hide.
         * @return whether the view is showing or not.
         */
        boolean onToggleDetailsPane();


    }

}
