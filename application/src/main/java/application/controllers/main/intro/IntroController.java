package application.controllers.main.intro;

import application.controllers.BaseController;
import application.util.filechooser.FileChooserBuilder;
import com.hotan.army.supply.data.PropertyBook;
import com.hotan.army.supply.data.PropertyBookManager;
import com.hotan.army.supply.forms.FormatException;
import com.hotan.army.supply.forms.handreceipt.subcomponent.ComponentHandReceipt;
import com.hotan.army.supply.forms.handreceipt.unit.UnitLevelHandReceipt;
import com.hotan.army.supply.util.loader.POILoader;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import org.parse4j.ParseException;
import org.parse4j.callback.GetCallback;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

/**
 * Created by mhotan on 3/6/14.
 */
public class IntroController extends BaseController {

    private static final Logger LOG = Logger.getLogger(IntroController.class.getSimpleName());

    @FXML
    private ProgressIndicator progressIndicator, progressIndicatorNewBook;

    @FXML
    private ListView<PropertyBook> previousProjectsList;

    @FXML
    private Label subcomponentHRPath, unitHRPath, orLabel;

    @FXML
    private Button previousContinueButton, newContinueButton;

    @FXML
    private TextField propertyBookName;

    @FXML
    private StackPane previousBooksStack;

    @FXML
    private VBox previousBooksPane, createNewBookPane;

    private final ObjectProperty<com.hotan.army.supply.data.PropertyBook> selectedBook;

    /**
     * The object container that holds the sub component hand receipt
     */
    private final SimpleObjectProperty<ComponentHandReceipt> componentReceipt;

    /**
     *  Unit Hand Receipt Property.
     */
    private final SimpleObjectProperty<UnitLevelHandReceipt> unitHandReceipt;

    /**
     * Simple reference to PropertyBook Manager
     */
    private final PropertyBookManager manager;

    public IntroController() {
        componentReceipt = new SimpleObjectProperty<ComponentHandReceipt>();
        unitHandReceipt = new SimpleObjectProperty<UnitLevelHandReceipt>();
        selectedBook = new SimpleObjectProperty<>();
        manager = PropertyBookManager.getInstance();
    }

    public PropertyBook getSelectedBook() {
        return selectedBook.get();
    }

    public ObjectProperty<PropertyBook> selectedBookProperty() {
        return selectedBook;
    }

    @FXML
    void onContinue(ActionEvent event) {
        Object source = event.getSource();

        // If we want to continue a previous project.
        if (source == previousContinueButton) {
            // If we are selecting a previous project from the list
            PropertyBook book = previousProjectsList.getSelectionModel().selectedItemProperty().get();
            assert book != null : "Book cannot be null to continue";
            selectedBook.set(book);
            return;
        }

        // Create a new property book and continue.
        createNewPropertBook();
    }

    private void createNewPropertBook() {
        // Start a new property book.
        String name = propertyBookName.getText().trim();
        propertyBookName.setText(name);
        newContinueButton.setVisible(false);
        manager.addBook(name, unitHandReceipt.get(),
                componentReceipt.get(), new PropertyBookGetCallback());
    }

    @FXML
    void onUploadUnitHR(ActionEvent event) {
        FileChooser chooser = FileChooserBuilder.getXLSFileChooser("Select Unit Hand Receipt");
        File file = chooser.showOpenDialog(((Node)event.getTarget()).getScene().getWindow());
        if (file == null) return;
        try {
            unitHandReceipt.set(new UnitLevelHandReceipt(POILoader.getXLSWorkbook(file)));
            unitHRPath.setText(file.getAbsolutePath());
        } catch (Exception e) {
            showException(null, "Unable to load Unit level Hand Receipt", e);
        }
    }

    @FXML
    void onUploadSubcomponentHR(ActionEvent event) {
        FileChooser chooser = FileChooserBuilder.getXLSFileChooser("Select Sub Component Hand Receipt");
        File file = chooser.showOpenDialog(((Node)event.getTarget()).getScene().getWindow());
        if (file == null) return;
        try {
            componentReceipt.set(new ComponentHandReceipt(POILoader.getXLSWorkbook(file)));
            subcomponentHRPath.setText(file.getAbsolutePath());
        } catch (Exception e) {
            showException(null, "Unable to load Sub Component Hand Receipt", e);
        }
    }


    @FXML
    protected void initialize() {
        assert progressIndicator != null : "fx:id=\"progressIndicator\" was not injected: check your FXML file 'Intro.fxml'.";
        assert previousProjectsList != null : "fx:id=\"previousProjectsList\" was not injected: check your FXML file 'Intro.fxml'.";
        assert createNewBookPane != null : "fx:id=\"createNewBookPane\" was not injected: check your FXML file 'Intro.fxml'.";
        assert subcomponentHRPath != null : "fx:id=\"subcomponentHRPath\" was not injected: check your FXML file 'Intro.fxml'.";
        assert previousContinueButton != null : "fx:id=\"previousContinueButton\" was not injected: check your FXML file 'Intro.fxml'.";
        assert orLabel != null : "fx:id=\"orLabel\" was not injected: check your FXML file 'Intro.fxml'.";
        assert progressIndicatorNewBook != null : "fx:id=\"progressIndicatorNewBook\" was not injected: check your FXML file 'Intro.fxml'.";
        assert previousBooksStack != null : "fx:id=\"previousBooksStack\" was not injected: check your FXML file 'Intro.fxml'.";
        assert propertyBookName != null : "fx:id=\"propertyBookName\" was not injected: check your FXML file 'Intro.fxml'.";
        assert previousBooksPane != null : "fx:id=\"previousBooksPane\" was not injected: check your FXML file 'Intro.fxml'.";
        assert newContinueButton != null : "fx:id=\"newContinueButton\" was not injected: check your FXML file 'Intro.fxml'.";
        assert unitHRPath != null : "fx:id=\"unitHRPath\" was not injected: check your FXML file 'Intro.fxml'.";
        super.initialize();
    }

    @Override
    protected void setUpUI() {
        // Bind the visibility with layout.
        previousContinueButton.visibleProperty().bindBidirectional(previousContinueButton.managedProperty());
        newContinueButton.visibleProperty().bindBidirectional(newContinueButton.managedProperty());

        // Set up the continue buttons. Previous button should always be enabled
        previousContinueButton.disableProperty().bind(
                previousProjectsList.getSelectionModel().selectedItemProperty().isNull());
        // Make sure the continue button is disabled at appropiate times.
        newContinueButton.disableProperty().bind(propertyBookName.textProperty().isEmpty().or(
                unitHandReceipt.isNull()).or(componentReceipt.isNull()));

        // Make sure the list
        previousBooksPane.prefHeightProperty().bind(createNewBookPane.prefHeightProperty());
        previousBooksPane.prefWidthProperty().bind(createNewBookPane.prefWidthProperty());

        // Visibility binded to layout property.
        previousBooksStack.managedProperty().bindBidirectional(previousBooksStack.visibleProperty());
        orLabel.managedProperty().bindBidirectional(orLabel.visibleProperty());
        orLabel.visibleProperty().bind(previousBooksStack.visibleProperty());
        progressIndicatorNewBook.visibleProperty().bindBidirectional(progressIndicatorNewBook.managedProperty());
        progressIndicatorNewBook.visibleProperty().bind(newContinueButton.visibleProperty().not());

        // Set up the table with the previous projects.
        previousProjectsList.setItems(manager.getAll());
        previousProjectsList.setCellFactory(view -> new PropertyBookListCell());
        // Make sure that the list is only visible if there are books.
        previousBooksStack.visibleProperty().bind(Bindings.isNotEmpty(manager.getAll()));


        propertyBookName.setOnKeyTyped(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (!keyEvent.getCode().equals(KeyCode.ENTER)) return;
                String name = propertyBookName.getText().trim();
                if (name.isEmpty()) return;
                createNewPropertBook();
            }
        });

        // Load defaults
        try {
            InputStream stream = getClass().getClassLoader().getResourceAsStream("default/9111_UNIT_HR.xls");
            unitHandReceipt.set(new UnitLevelHandReceipt(POILoader.getXLSWorkbook(stream)));
            unitHRPath.setText("9111_UNIT_HR.xls");
            stream.close();

            InputStream stream2 = getClass().getClassLoader().getResourceAsStream("default/ComponentHandReceipt.xls");
            componentReceipt.set(new ComponentHandReceipt(POILoader.getXLSWorkbook(stream2)));
            subcomponentHRPath.setText("ComponentHandReceipt.xls");

        } catch (IOException e) {
            throw new RuntimeException("No default files found");
        } catch (FormatException e) {
            throw new RuntimeException(e);
        }
    }

    private class PropertyBookGetCallback extends GetCallback<PropertyBook> {
        @Override
        public void done(com.hotan.army.supply.data.PropertyBook propertyBook, ParseException e) {
            if (e != null) {
                showException(null, "Unable to Create Property book", e);
                newContinueButton.setVisible(true);
                return;
            }
            selectedBook.set(propertyBook);
        }
    }

    private static class PropertyBookListCell extends ListCell<PropertyBook> {

        @Override
        protected void updateItem(PropertyBook book, boolean b) {
            super.updateItem(book, b);
            if (book != null) {
                setText(book.getName());
            } else {
                setText(null);
            }
        }
    }
}
