package application.controllers.main;

import application.controllers.BaseController;
import application.controllers.main.intro.IntroController;
import application.views.MasterDetailsView;
import application.views.intro.IntroView;
import com.hotan.army.supply.data.PropertyBook;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.layout.BorderPane;

/**
 * Controller for MainView.java.  Handles menu interactions and view management.
 *
 * Created by mhotan on 3/6/14.
 */
public class MainController extends BaseController {

    @FXML
    private BorderPane root;

    private final IntroView introView;

    private final IntroController introController;

    private MasterDetailsView masterDetailsView;

    private final ObjectProperty<PropertyBook> selectedPropertyBook;

    /**
     * Prepare the controller
     */
    public MainController() {
        introController = new IntroController();
        introView = new IntroView(introController);

        selectedPropertyBook = new SimpleObjectProperty<>();
    }

    @FXML
    void onClose(ActionEvent event) {

    }

    @FXML
    void onDelete(ActionEvent event) {

    }

    @FXML
    void generatePropertyBook(ActionEvent event) {

    }

    @FXML
    void generateStayGoList(ActionEvent event) {

    }

    @FXML
    void onAbout(ActionEvent event) {

    }

    @FXML
    protected void initialize() {
        assert root != null : "fx:id=\"root\" was not injected: check your FXML file 'Main.fxml'.";
        super.initialize();
    }

    @Override
    protected void setUpUI() {
        selectedPropertyBook.addListener((ov, oldVal, newVal) -> {
            if (newVal == null) {
                showIntroView();
            } else {
                showMasterView(newVal);
            }
        });
        // List for Any changes in the property book.
        selectedPropertyBook.bind(introController.selectedBookProperty());
        showIntroView();
    }

    private void showIntroView() {
        // Show the intro view.
        showView(introView);
    }

    private void showMasterView(PropertyBook book) {
        MasterDetailsController controller = new MasterDetailsController(book);
        // Possibly add this for callback controller
        masterDetailsView = new MasterDetailsView(controller);
        showView(masterDetailsView);
    }

    private void showView(Node view) {
        root.setCenter(view);
    }
}
