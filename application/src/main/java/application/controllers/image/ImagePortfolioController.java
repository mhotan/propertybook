package application.controllers.image;

import application.Constants;
import application.controllers.BaseController;
import application.util.filechooser.FileChooserBuilder;
import application.util.loader.ImageLoader;
import com.hotan.army.supply.data.items.itemdata.EndItemData;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Pagination;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import org.controlsfx.control.InfoOverlay;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Created by mhotan on 3/20/14.
 */
public class ImagePortfolioController extends BaseController {

    @FXML
    private Pagination pagination;

    private final ObjectProperty<EndItemData> data;

    private static final String[] labels = {"Overview", "Serial Number", "End Item Components", "End Item Location"};
    private static final String[] descriptions = {
            "This is an overview of the complete End Item",
            "The is an image of the Serial Number that uniquely identifies this end item",
            "The layout of the End Item and all its components",
            "This is the location of where the end item is currently stored while not OCONUS.  Be sure to update this image if " +
                    "the end item is moved."};

    private final ObjectProperty<Image>[] images;

    /**
     * Creates a controller for
     *
     * @param Creates a controller to display images for an end item group
     */
    public ImagePortfolioController() {
        this.data = new SimpleObjectProperty<>();
        images = new ObjectProperty[4];
        for (int i = 0; i < images.length; i++) {
            images[i] = new SimpleObjectProperty<>();
        }
    }

    @FXML
    protected void initialize() {
        assert pagination != null : "fx:id=\"pagination\" was not injected: check your FXML file 'ImagePortfolio.fxml'.";
        super.initialize();
    }

    @Override
    protected void setUpUI() {
        pagination.setMaxPageIndicatorCount(images.length);
        pagination.setPageCount(images.length);
        pagination.setPageFactory(
                i -> {
                    // Creates an image view to place in the pane
                    ImageView iv = new ImageView();
                    iv.setFitHeight(200);
                    iv.setFitWidth(200);
                    iv.setPreserveRatio(true);

                    // Set the image
                    setImage(iv, images[i].get());

                    // Create the overlay
                    InfoOverlay overlay = new InfoOverlay(iv, descriptions[i]);
                    StackPane pane = new StackPane(overlay);

                    // If the image is ever changed then update the value.
                    images[i].addListener((ov, old, newVal) -> setImage(iv, newVal));

                    // If the image view is clicked then give the user
                    // the option to update the image.
                    iv.setOnMouseClicked(e -> chooseImage(e, labels[i], images[i]));

                    return pane;
                });

        data.addListener((ov, oldVal, newVal) -> {
            if (oldVal != null)
                for (int i = 0; i < images.length; ++i)
                    images[i].unbindBidirectional(getImageProperty(oldVal, i));

            if (newVal == null) return;

            for (int i = 0; i < images.length; ++i)
                images[i].bindBidirectional(getImageProperty(oldVal, i));
        });

    }

    private static ObjectProperty<Image> getImageProperty(EndItemData data, int index) {
        switch (index) {
            case 0: return data.overviewImageProperty();
            case 1: return data.serialNumberImageProperty();
            case 2: return data.componentsImageProperty();
            case 3: return data.locationImageProperty();
            default: return null;
        }
    }

    public EndItemData getData() {
        return data.get();
    }

    public ObjectProperty<EndItemData> dataProperty() {
        return data;
    }

    private void chooseImage(Event e, String name, ObjectProperty<Image> imageProp) {
        if (imageProp == null) return;

        FileChooser imageChooser = FileChooserBuilder.getImageFileChooser(
                "Select a new Image for " + name);
        File file = imageChooser.showOpenDialog(((Node)e.getTarget()).getScene().getWindow());
        if (file == null) return;

        try {
            Image image = new Image(new FileInputStream(file),
                    Constants.MAX_IMAGE_DIMENSION, Constants.MAX_IMAGE_DIMENSION, true, true);
            imageProp.set(image);
        } catch (FileNotFoundException e2) {
            showException(null, "Unable to find the image", e2);
        }
    }

    /**
     * Does all the redundant checking for setting the image view.
     * @param iv
     * @param itemImage
     */
    private static void setImage(ImageView iv, Image itemImage) {
        if (itemImage == null)
            itemImage = ImageLoader.getNoImageAvailableImage();
        iv.setImage(itemImage);
    }

    private Node generateLoadingPane() {
        VBox box = new VBox();
        box.setMaxWidth(Double.MAX_VALUE);
        box.setMaxHeight(Double.MAX_VALUE);
        ProgressIndicator indicator = new ProgressIndicator();
        indicator.setProgress(-1);
        box.getChildren().add(indicator);
        return box;
    }
}
