package application.controllers.inspection;

import application.controllers.BaseController;
import com.hotan.army.supply.data.inspection.DeficiencyEntry;
import com.hotan.army.supply.data.items.EndItem;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.ChoiceBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by mhotan on 3/8/14.
 */
public class DeficienciesController extends BaseController {

    @FXML
    private TableView<DeficiencyEntry> tableView;

    @FXML
    private TableColumn<DeficiencyEntry, String> actionColumn, itemNumColumn, deficiencyColumn;

    @FXML
    private TableColumn<DeficiencyEntry, LocalDateTime> dateColumn;

    @FXML
    private TableColumn<DeficiencyEntry, DeficiencyEntry.STATUS> statusColumn;

    @FXML
    private VBox tmAndDatePane, datePickerPane;

    @FXML
    private ComboBox<DeficiencyEntry.STATUS> statusComboBox;

    @FXML
    private Button createNewEntryButton, deleteButton, saveNewEntryButton;

    @FXML
    private TextField tmNumberField;

    @FXML
    private TextArea deficiencyTextArea, correctiveActionArea;

    @FXML
    private DatePicker picker;

    /**
     *  Item to draw on this view.
     */
    private final ObjectProperty<EndItem> item;

    /**
     * The entry that is selected
     */
    private final ObjectProperty<DeficiencyEntry> selectedEntry;

    public DeficienciesController() {
        item = new SimpleObjectProperty<>();
        selectedEntry = new SimpleObjectProperty<>();
    }

    @FXML
    void onCreateNewRecord(ActionEvent event) {

        // Clear the selection of the table view thus providing the ability create new
        tableView.getSelectionModel().clearSelection();
        // Hide the create new selectedEntry button.
        statusComboBox.requestFocus();
    }

    @FXML
    void onSaveNewDeficientryEntry(ActionEvent event) {

        DeficiencyEntry.STATUS status = statusComboBox.getSelectionModel().getSelectedItem();
        String tmNum = tmNumberField.getText();
        String deficiency = deficiencyTextArea.getText();
        LocalDateTime dateTime = LocalDateTime.of(picker.getValue(), LocalTime.now());
        String correctiveAction = correctiveActionArea.getText();

        DeficiencyEntry entry = new DeficiencyEntry(tmNum, deficiency,
                correctiveAction, status, dateTime);
        item.get().addDeficiency(entry);
        tableView.getSelectionModel().select(entry);
    }

    @FXML
    void onDeleteButtons(ActionEvent event) {
        DeficiencyEntry entry = selectedEntry.get();
        item.get().removeDeficiencyEntry(entry);
        statusComboBox.requestFocus();
    }

    @FXML
    protected void initialize() {
        assert actionColumn != null : "fx:id=\"actionColumn\" was not injected: check your FXML file 'IssueTracker.fxml'.";
        assert deleteButton != null : "fx:id=\"deleteButton\" was not injected: check your FXML file 'IssueTracker.fxml'.";
        assert itemNumColumn != null : "fx:id=\"itemNumColumn\" was not injected: check your FXML file 'IssueTracker.fxml'.";
        assert tmAndDatePane != null : "fx:id=\"tmAndDatePane\" was not injected: check your FXML file 'IssueTracker.fxml'.";
        assert tableView != null : "fx:id=\"tableView\" was not injected: check your FXML file 'IssueTracker.fxml'.";
        assert deficiencyColumn != null : "fx:id=\"deficiencyColumn\" was not injected: check your FXML file 'IssueTracker.fxml'.";
        assert correctiveActionArea != null : "fx:id=\"correctiveActionArea\" was not injected: check your FXML file 'IssueTracker.fxml'.";
        assert statusComboBox != null : "fx:id=\"statusComboBox\" was not injected: check your FXML file 'IssueTracker.fxml'.";
        assert statusColumn != null : "fx:id=\"statusColumn\" was not injected: check your FXML file 'IssueTracker.fxml'.";
        assert createNewEntryButton != null : "fx:id=\"createNewEntryButton\" was not injected: check your FXML file 'IssueTracker.fxml'.";
        assert tmNumberField != null : "fx:id=\"tmNumberField\" was not injected: check your FXML file 'IssueTracker.fxml'.";
        assert deficiencyTextArea != null : "fx:id=\"deficiencyTextArea\" was not injected: check your FXML file 'IssueTracker.fxml'.";
        assert dateColumn != null : "fx:id=\"dateColumn\" was not injected: check your FXML file 'IssueTracker.fxml'.";
        assert saveNewEntryButton != null : "fx:id=\"saveNewEntryButton\" was not injected: check your FXML file 'IssueTracker.fxml'.";
        assert datePickerPane != null : "fx:id=\"datePickerPane\" was not injected: check your FXML file 'IssueTracker.fxml'.";
        assert picker != null : "fx:id=\"picker\" was not injected: check your FXML file 'IssueTracker.fxml'.";
        super.initialize();
    }

    public void setEndItem(EndItem item) {
        this.item.set(item);
    }

    public EndItem getItem() {
        return item.get();
    }

    public ObjectProperty<EndItem> itemProperty() {
        return item;
    }

    /**
     * Set up the buttons for this view and controller.
     */
    private void setupButtons() {

        // Bind the visibility with the layout properties.
        createNewEntryButton.visibleProperty().bindBidirectional(createNewEntryButton.managedProperty());
        saveNewEntryButton.visibleProperty().bindBidirectional(saveNewEntryButton.managedProperty());
        deleteButton.visibleProperty().bindBidirectional(deleteButton.managedProperty());

        // Whenever the create new record button is seen
        // then hide the save button and visa verca
        saveNewEntryButton.visibleProperty().bind(selectedEntry.isNull());
        createNewEntryButton.visibleProperty().bind(saveNewEntryButton.visibleProperty().not());

        // Bind the delete button with the selected entry.
        deleteButton.visibleProperty().bind(selectedEntry.isNotNull());

        // Binding that notifies that the deficiency text area
        // is not empty.
        BooleanBinding deficiencyNotEmpty = Bindings.createBooleanBinding(() ->
                        !deficiencyTextArea.getText().trim().isEmpty(),
                deficiencyTextArea.textProperty());

        // Whether or not we can save a new Deficiency Entry.
        BooleanBinding isSaveble = statusComboBox.getSelectionModel().
                selectedItemProperty().isEqualTo(DeficiencyEntry.STATUS.OK).or(
                deficiencyNotEmpty.and(statusComboBox.getSelectionModel().
                        selectedItemProperty().isNotNull())).and(
                picker.valueProperty().isNotNull()).and(
                saveNewEntryButton.visibleProperty());
        saveNewEntryButton.disableProperty().bind(isSaveble.not());

        // Add tool tips.
        saveNewEntryButton.setTooltip(new Tooltip("Start filling in the" +
                " appropriate fields to save a new entry."));
    }

    private void setUpTable() {
        // Handle the table.
        dateColumn.setCellFactory(tableColumn -> new DateTableCell());
        dateColumn.setCellValueFactory(new PropertyValueFactory<>("dateTime"));
        actionColumn.setCellValueFactory(new PropertyValueFactory<>("correctiveAction"));
        deficiencyColumn.setCellValueFactory(new PropertyValueFactory<>("deficiency"));
        itemNumColumn.setCellValueFactory(new PropertyValueFactory<>("tmItemNum"));
        statusColumn.setCellValueFactory(new PropertyValueFactory<>("status"));
        statusColumn.setCellFactory(ChoiceBoxTableCell.forTableColumn(DeficiencyEntry.STATUS.STATUSES));
    }

    @Override
    protected void setUpUI() {
        selectedEntry.bind(tableView.getSelectionModel().selectedItemProperty());

        // Set the status box values.
        statusComboBox.setItems(DeficiencyEntry.STATUS.STATUSES);

        // If the status is OK then disable the deficiency text area
        deficiencyTextArea.disableProperty().bind(
                statusComboBox.getSelectionModel().selectedItemProperty().
                        isEqualTo(DeficiencyEntry.STATUS.OK));

        // Set the picker to the current date
        picker.setValue(LocalDate.now());

        // Set up buttons
        setupButtons();

        // Set up the table.
        setUpTable();

        // Show a different end item.  Clear out any previous fields
        this.item.addListener((ov, oldVal, newVal) -> {
            // Setting the selectedEntry to null clears out old selectedEntry bindings
            // Clear the screen of of anything from the past value.
            if (oldVal != null) {
                // Unbind certain features
            }
            if (newVal == null) return;

            // Set the elements of the table view.
            tableView.setItems(newVal.getDeficiencyEntries());
        });

        // Add a listener for when the selected selectedEntry value is changed
        selectedEntry.addListener((ov, oldVal, newVal) -> {
            if (oldVal != null) {
                // Unbind the old properties
                oldVal.statusProperty().unbind();
                tmNumberField.textProperty().unbindBidirectional(oldVal.tmItemNumProperty());
                picker.valueProperty().unbind();
                deficiencyTextArea.textProperty().unbindBidirectional(oldVal.deficiencyProperty());
                correctiveActionArea.textProperty().unbindBidirectional(oldVal.correctiveActionProperty());
            } // There is no selection but the ability to add a new value.
            if (newVal == null) // default out the fields.
            {
                defaultFields();
                return;
            }
            // There is a new selected item
            // Bind the status box.
            statusComboBox.getSelectionModel().select(newVal.getStatus());
            newVal.statusProperty().bind(statusComboBox.getSelectionModel().selectedItemProperty());

            // Bind the TM Number, DatePicker, Deficiency block, and Corrective Action Area.
            tmNumberField.textProperty().bindBidirectional(newVal.tmItemNumProperty());
            picker.valueProperty().bind(newVal.dateProperty());
            deficiencyTextArea.textProperty().bindBidirectional(newVal.deficiencyProperty());
            correctiveActionArea.textProperty().bindBidirectional(newVal.correctiveActionProperty());
        });

        // Set tool tips
        if (saveNewEntryButton.isDisabled()) {
            saveNewEntryButton.setTooltip(SAVE_DISABLED_TOOLIP);
        }
        saveNewEntryButton.disableProperty().addListener((ov, oldVal, disabled) -> {
                    if (disabled)
                        saveNewEntryButton.setTooltip(SAVE_DISABLED_TOOLIP);
                    else
                        saveNewEntryButton.setTooltip(SAVE_ENABLED_TOOLIP);
                });
        createNewEntryButton.setTooltip(new Tooltip("Create a new blank entry for you to start from scratch"));
        deleteButton.setTooltip(new Tooltip("Delete the current selected deficiency entry"));
        statusComboBox.setTooltip(new Tooltip("Select the status of the end item"));
        tmNumberField.setTooltip(new Tooltip("(Optional) Select the TM Item number of the defected component"));
        picker.setTooltip(new Tooltip("Select the date the deficiency was observed."));
        correctiveActionArea.setTooltip(new Tooltip("(Optional) Place any the actions to fix the defected item."));
        deficiencyTextArea.setTooltip(new Tooltip("Place the details of the deficiency here"));
    }

    private static final Tooltip SAVE_DISABLED_TOOLIP = new Tooltip("Select a status, Date, and write out the deficiency.");
    private static final Tooltip SAVE_ENABLED_TOOLIP = new Tooltip("Save the current tooltip.  Can always remove it later");


    private void defaultFields() {
        statusComboBox.getSelectionModel().select(DeficiencyEntry.STATUS.OK);
        tmNumberField.setText("");
        deficiencyTextArea.setText("");
        correctiveActionArea.setText("");
        picker.setValue(LocalDate.now());
    }

    private static class DateTableCell extends TableCell<DeficiencyEntry, LocalDateTime> {

        @Override
        protected void updateItem(LocalDateTime date, boolean b) {
            super.updateItem(date, b);
            if (date != null) {
                String string = date.format(DateTimeFormatter.ISO_LOCAL_DATE);
                setText(string);
            } else {
                setText(null);
            }
        }
    }



}
