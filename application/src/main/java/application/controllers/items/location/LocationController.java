package application.controllers.items.location;

import application.controllers.BaseController;
import application.custom.controls.DoubleTextField;
import com.hotan.army.supply.data.location.LocatedAt;
import com.hotan.army.supply.data.location.Location;
import com.hotan.army.supply.data.location.LocationManager;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import org.parse4j.ParseException;

/**
 * Created by mhotan on 3/24/14.
 */
public class LocationController extends BaseController implements ChangeListener<Location> {

    private static final String FLOATING_POINT_REGEX = "^[-+]?[0-9]*\\.?[0-9]+$";

    @FXML
    private Button saveLocationButton, addLocationButton;

    @FXML
    private TextArea descriptionArea;

    @FXML
    private ComboBox<Location> locationNameBox;

    @FXML
    private HBox locationBar, geoPointPane;

    @FXML
    private TextField locationTextField;

    /**
     * Location of this.
     */
    private final ObjectProperty<LocatedAt> location;

    private final LocationController This;

    /**
     * Text fields for the latitude and longitude
     */
    private final DoubleTextField latitudeField, longitudeField;

    public LocationController() {
        location = new SimpleObjectProperty<>();
        latitudeField = new DoubleTextField();
        latitudeField.setPromptText("Latitude");
        HBox.setHgrow(latitudeField, Priority.NEVER);
        longitudeField = new DoubleTextField();
        longitudeField.setPromptText("Longitude");
        HBox.setHgrow(longitudeField, Priority.NEVER);
        This = this;
    }

    /**
     * set the location to view.
     *
     * @param location Location to set the item at.
     */
    public void setLocation(LocatedAt location) {
        this.location.set(location);
    }

    public LocatedAt getLocation() {
        return location.get();
    }

    public ObjectProperty<LocatedAt> locationProperty() {
        return location;
    }

    @FXML
    void onAddLocation(ActionEvent event) {
        locationNameBox.getSelectionModel().clearSelection();
    }

    @FXML
    void onSaveLocation(ActionEvent event) {
        String name = locationTextField.getText().trim();
        if (name.isEmpty())
            return;
        try {
            Location location = LocationManager.getInstance().add(name);
            double oldLon = location.getLongitude();
            double oldLat = location.getLatitude();
            try {
                Double lon = Double.valueOf(longitudeField.getText());
                lon = Math.min(Location.MAX_LONGITUDE, Math.max(Location.MIN_LONGITUDE, lon));
                latitudeField.setText("" + lon);
                location.setLongitude(lon);
            } catch (NumberFormatException e) {
                latitudeField.setText("" + oldLon);
            }

            try {
                Double lat = Double.valueOf(latitudeField.getText());
                lat = Math.min(Location.MAX_LATITUDE, Math.max(Location.MIN_LATITUDE, lat));
                latitudeField.setText("" + lat);
            } catch (NumberFormatException e) {
                latitudeField.setText("" + oldLat);
            }

            locationNameBox.getSelectionModel().select(location);
            locationTextField.setText("");

            String desc = descriptionArea.getText().trim();
            LocatedAt lAt = new LocatedAt(location, desc);
            setLocation(lAt);
        } catch (ParseException e) {
            showException(null, "Unable to create a new exception", e);
        }
    }

    @FXML
    protected void initialize() {
        assert saveLocationButton != null : "fx:id=\"saveLocationButton\" was not injected: check your FXML file 'Location.fxml'.";
        assert descriptionArea != null : "fx:id=\"descriptionArea\" was not injected: check your FXML file 'Location.fxml'.";
        assert locationNameBox != null : "fx:id=\"locationNameBox\" was not injected: check your FXML file 'Location.fxml'.";
        assert locationBar != null : "fx:id=\"locationBar\" was not injected: check your FXML file 'Location.fxml'.";
        assert addLocationButton != null : "fx:id=\"addLocationButton\" was not injected: check your FXML file 'Location.fxml'.";
        assert geoPointPane != null : "fx:id=\"geoPointPane\" was not injected: check your FXML file 'Location.fxml'.";
        assert locationTextField != null : "fx:id=\"locationTextField\" was not injected: check your FXML file 'Location.fxml'.";
        super.initialize();
    }

    @Override
    protected void setUpUI() {

        // Initialize the text fields
        geoPointPane.getChildren().addAll(latitudeField, longitudeField);

        // Set the location cell factory and list.
        locationNameBox.setCellFactory(listView -> new LocationCell());
        locationNameBox.setItems(LocationManager.getInstance().getAll());

        // Bind the visbility
        saveLocationButton.visibleProperty().bindBidirectional(saveLocationButton.managedProperty());
        addLocationButton.visibleProperty().bindBidirectional(addLocationButton.managedProperty());
        locationTextField.visibleProperty().bindBidirectional(locationTextField.managedProperty());
        locationNameBox.visibleProperty().bindBidirectional(locationNameBox.managedProperty());

        // Only show the text field if there isn't one already selected
        locationTextField.visibleProperty().bind(locationNameBox.getSelectionModel().selectedItemProperty().isNull());

        // Only show the add location view when the text view is not present
        addLocationButton.visibleProperty().bind(locationTextField.visibleProperty().not());

        // The visibility of buttons should be inverted
        saveLocationButton.visibleProperty().bind(locationTextField.visibleProperty());
        saveLocationButton.disableProperty().bind(locationTextField.textProperty().isEmpty());

        // Listen for changes in the location instance.
        this.location.addListener((ov, oldVal, newVal) -> {
            if (oldVal != null) {
                // TODO Unbind fields
                oldVal.locationProperty().removeListener(This);
                oldVal.locationProperty().unbind();
                descriptionArea.textProperty().unbindBidirectional(oldVal.descriptionProperty());
            }
            if (newVal == null) {
                descriptionArea.setText("");
                locationNameBox.getSelectionModel().clearSelection();
                resetCoordinates();
                return;
            }

            // Bind the location object .
            locationNameBox.getSelectionModel().select(newVal.getLocation());
            newVal.locationProperty().bind(locationNameBox.getSelectionModel().selectedItemProperty());

            // Bind the description
            descriptionArea.textProperty().bindBidirectional(newVal.descriptionProperty());

            // Handle
            newVal.locationProperty().addListener(This);
        });


        locationNameBox.setTooltip(new Tooltip("Set the location where this end item is stored."));
        locationTextField.setTooltip(new Tooltip("Type the name of a new location to store this item at"));
        addLocationButton.setTooltip(new Tooltip("Add a new location by name and geographic location"));
        latitudeField.setTooltip(new Tooltip("Latitude of the location.  Latitude must be between -90.0 and 90.0"));
        longitudeField.setTooltip(new Tooltip("Longitude of the location. Longitude must be between -180 and 180"));
        descriptionArea.setTooltip(new Tooltip("A detail description of where the item is at"));
        saveLocationButton.setTooltip(new Tooltip("Save and use the current location"));
    }

    private void resetCoordinates() {
        latitudeField.setEditable(true);
        longitudeField.setEditable(true);
        latitudeField.textProperty().unbind();
        longitudeField.textProperty().unbind();
        latitudeField.setText("");
        longitudeField.setText("");
    }

    @Override
    public void changed(ObservableValue<? extends Location> ov, Location oldLoc, Location newLoc) {
        setLocation(oldLoc, newLoc);
    }

    private void setLocation(Location oldLoc, Location newLoc) {
        if (oldLoc != null) {
//            Remove all the bindings
            resetCoordinates();
        }
        if (newLoc == null) return;

        latitudeField.textProperty().bind(newLoc.latitudeProperty().asString());
        latitudeField.setEditable(false);
        longitudeField.textProperty().bind(newLoc.longitudeProperty().asString());
        longitudeField.setEditable(false);
    }

    private static class LocationCell extends ListCell<Location> {

        @Override
        protected void updateItem(Location location, boolean b) {
            super.updateItem(location, b);
            if (location != null) {
                setText(location.getName());
            } else
                setText(null);
        }
    }
}
