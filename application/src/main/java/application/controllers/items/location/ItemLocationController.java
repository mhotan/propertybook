package application.controllers.items.location;

import application.controllers.BaseController;
import application.controllers.tracking.SignedOutRecordController;
import application.util.loader.ImageLoader;
import application.views.location.LocationView;
import application.views.tracking.SignedOutRecordView;
import com.hotan.army.supply.data.items.EndItem;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;

import java.time.format.DateTimeFormatter;

/**
 * Created by mhotan on 3/8/14.
 */
public class ItemLocationController extends BaseController {

    private static final String SIGNOUT_LABEL = "Sign Out";
    private static final Tooltip SIGNOUT_TOOLTIP = new Tooltip("Sign out this item");
    private static final String CANCEL_LABEL = "Cancel";
    private static final Tooltip CANCEL_TOOLTIP = new Tooltip("Cancel the sign out");

    @FXML
    private Label signedInOutLabel;

    @FXML
    private ImageView imageView;

    @FXML
    private Hyperlink handReceiptLink;

    @FXML
    private StackPane contentPane;

    @FXML
    private Button signInButton, signOutButton, cancelButton;

    @FXML
    private VBox signInOutPane;

    /**
     * The end item to display.
     */
    private final ObjectProperty<EndItem> item;

    private final SignedOutRecordController signedOutRecordController;

    private final SignedOutRecordView recordView;

    private final LocationController onHandLocationController;

    private final LocationView onHandLocationView;

    private final ObjectProperty<Image> locationImage;

    public ItemLocationController() {
        item = new SimpleObjectProperty<>();
        signedOutRecordController = new SignedOutRecordController();
        onHandLocationController = new LocationController();
        recordView = new SignedOutRecordView(signedOutRecordController);
        onHandLocationView = new LocationView(onHandLocationController);
        locationImage = new SimpleObjectProperty<>();
    }

    public void setEndItem(EndItem item) {
        this.item.set(item);
    }

    public EndItem getItem() {
        return item.get();
    }

    public ObjectProperty<EndItem> itemProperty() {
        return item;
    }

    @FXML
    void onDownloadHandReceipt(ActionEvent event) {
        showNotYetImplemented(null);
    }

    @FXML
    void onCancel(ActionEvent event) {
        recordView.setVisible(false);
    }

    @FXML
    void onSignIn(ActionEvent event) {
        Action[] actions = {Dialog.Actions.YES, Dialog.Actions.CANCEL};
        Action confirmation = showConfirmation(null, "Signing in " + item.get().getName(),
                "Are you sure you want to sign this item back in from "
         + this.item.get().getCurrentSignedOutRecord().getIndividual() + "?", actions);
        if (confirmation.equals(Dialog.Actions.YES)) {
            // Clear out the old record.
            item.get().setCurrentSignedOutRecord(null);
        }
    }

    @FXML
    void onSignOut(ActionEvent event) {
        recordView.setVisible(true);
    }


    @FXML
    protected void initialize() {
        assert signedInOutLabel != null : "fx:id=\"signedInOutLabel\" was not injected: check your FXML file 'ItemLocation.fxml'.";
        assert cancelButton != null : "fx:id=\"cancelButton\" was not injected: check your FXML file 'ItemLocation.fxml'.";
        assert imageView != null : "fx:id=\"imageView\" was not injected: check your FXML file 'ItemLocation.fxml'.";
        assert handReceiptLink != null : "fx:id=\"handReceiptLink\" was not injected: check your FXML file 'ItemLocation.fxml'.";
        assert signInButton != null : "fx:id=\"signInButton\" was not injected: check your FXML file 'ItemLocation.fxml'.";
        assert contentPane != null : "fx:id=\"contentPane\" was not injected: check your FXML file 'ItemLocation.fxml'.";
        assert signInOutPane != null : "fx:id=\"signInOutPane\" was not injected: check your FXML file 'ItemLocation.fxml'.";
        assert signOutButton != null : "fx:id=\"signOutButton\" was not injected: check your FXML file 'ItemLocation.fxml'.";
        super.initialize();
    }

    @Override
    protected void setUpUI() {

        // Add all the views and then just manage the visibility.
        contentPane.getChildren().add(onHandLocationView);
        contentPane.getChildren().add(recordView);

        // Visibility and layout property.
        recordView.visibleProperty().bindBidirectional(recordView.managedProperty());

        // Bind button visibilities
        signOutButton.visibleProperty().bindBidirectional(signOutButton.managedProperty());
        signInButton.visibleProperty().bindBidirectional(signInButton.managedProperty());
        cancelButton.visibleProperty().bindBidirectional(cancelButton.managedProperty());

        handReceiptLink.visibleProperty().bind(signInButton.visibleProperty());
        // Show the hand receipt is not available yet.
        handReceiptLink.setOnAction(e -> showNotYetImplemented(null));

        // Make sure the sign in and out buttons are only visible when
        // there is or is not a current record.
        signInButton.visibleProperty().bind(signedOutRecordController.recordProperty().isNotNull());
        signOutButton.visibleProperty().bind(signedOutRecordController.recordProperty().isNull().and(
                recordView.visibleProperty().not()));
        cancelButton.visibleProperty().bind(signedOutRecordController.recordProperty().isNull().and(
                recordView.visibleProperty()));

        // Add a listener for location image.
        locationImage.addListener((ov, oldVal, newVal) -> {
            setImage(newVal);
        });

        // Make sure the two panes
        item.addListener((ov, oldVal, newVal) -> {
            if (oldVal != null) {
                // Unbind
                locationImage.unbind();
                signedInOutLabel.textProperty().unbind();
                onHandLocationController.locationProperty().unbind();
                signedOutRecordController.recordProperty().unbindBidirectional(
                        oldVal.currentSignedOutRecordProperty());
            }
            if (newVal == null) {
//                Nothing to do
                return;
            }

            // Create all the bindings
            locationImage.bind(newVal.getData().locationImageProperty());
            setImage(newVal.getData().getLocationImage());
            signedInOutLabel.textProperty().bind(Bindings.createStringBinding(() -> {
                if (newVal.currentSignedOutRecordProperty().isNotNull().get()) {
                    return newVal.getNameLabel() + "=" + newVal.getName() + " Signed out to " + newVal.getCurrentSignedOutRecord().getIndividual() + " " +
                            "on " + newVal.getCurrentSignedOutRecord().dateSignedOutProperty().get().format(
                            DateTimeFormatter.ISO_LOCAL_DATE);
                } else {
                    return newVal.getName() + " is not signed out.";
                }
            }, newVal.currentSignedOutRecordProperty()));
            onHandLocationController.locationProperty().bind(newVal.locationProperty());
            signedOutRecordController.recordProperty().bindBidirectional(
                    newVal.currentSignedOutRecordProperty());

            // Set the intial view.
            if (newVal.getCurrentSignedOutRecord() != null)
                recordView.setVisible(true);
            else
                recordView.setVisible(false);
        });
    }

    private void setImage(Image image) {
        if (image == null)
            image = ImageLoader.getNoImageAvailableImage();
        imageView.setImage(image);
    }

}
