package application.controllers.items.components;

import application.controllers.BaseController;
import com.hotan.army.supply.data.items.EndItem;
import com.hotan.army.supply.data.items.components.AccountableComponent;
import com.hotan.army.supply.data.items.components.EndItemBasicIssueComponent;
import com.hotan.army.supply.data.items.components.EndItemComponent;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.Callback;
import javafx.util.converter.IntegerStringConverter;

/**
 * Created by mhotan on 3/8/14.
 */
public class ComponentsController extends BaseController {

    @FXML
    private TableView<EndItemBasicIssueComponent> biiTable;

    @FXML
    private TableView<EndItemComponent> coeiTable;

    @FXML
    private TableColumn<EndItemBasicIssueComponent, String> biiNameColumn, biiNSNColumn;

    @FXML
    private TableColumn<EndItemComponent, String> coeiNameColumn, coeiNSNColumn;

    @FXML
    private TableColumn<EndItemComponent, Integer> coeiOnHandColumn, coeiAuthorizedColumn;

    @FXML
    private TableColumn<EndItemBasicIssueComponent, Integer> biiOnHandColumn, biiAuthorizedColumn;

    @FXML
    private Label biiLabel, coeiLabel, noItemsLabel;

    private final ObjectProperty<EndItem> item;

    public ComponentsController() {
        item = new SimpleObjectProperty<>();
    }

    @FXML
    protected void initialize() {
        assert biiNSNColumn != null : "fx:id=\"biiNSNColumn\" was not injected: check your FXML file 'Components.fxml'.";
        assert coeiNameColumn != null : "fx:id=\"coeiNameColumn\" was not injected: check your FXML file 'Components.fxml'.";
        assert biiOnHandColumn != null : "fx:id=\"biiOnHandColumn\" was not injected: check your FXML file 'Components.fxml'.";
        assert coeiTable != null : "fx:id=\"coeiTable\" was not injected: check your FXML file 'Components.fxml'.";
        assert biiTable != null : "fx:id=\"biiTable\" was not injected: check your FXML file 'Components.fxml'.";
        assert biiLabel != null : "fx:id=\"biiLabel\" was not injected: check your FXML file 'Components.fxml'.";
        assert biiNameColumn != null : "fx:id=\"biiNameColumn\" was not injected: check your FXML file 'Components.fxml'.";
        assert biiAuthorizedColumn != null : "fx:id=\"biiAuthorizedColumn\" was not injected: check your FXML file 'Components.fxml'.";
        assert coeiLabel != null : "fx:id=\"coeiLabel\" was not injected: check your FXML file 'Components.fxml'.";
        assert noItemsLabel != null : "fx:id=\"noItemsLabel\" was not injected: check your FXML file 'Components.fxml'.";
        assert coeiNSNColumn != null : "fx:id=\"coeiNSNColumn\" was not injected: check your FXML file 'Components.fxml'.";
        assert coeiOnHandColumn != null : "fx:id=\"coeiOnHandColumn\" was not injected: check your FXML file 'Components.fxml'.";
        assert coeiAuthorizedColumn != null : "fx:id=\"coeiAuthorizedColumn\" was not injected: check your FXML file 'Components.fxml'.";
        super.initialize();
    }

    public void setEndItem(EndItem item) {
        this.item.set(item);
    }

    public EndItem getItem() {
        return item.get();
    }

    public ObjectProperty<EndItem> itemProperty() {
        return item;
    }

    @Override
    protected void setUpUI() {

        // Set the name columns of both tables.
        biiNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        coeiNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));

        // Set the NSN columns of both tables
        biiNSNColumn.setCellValueFactory(new PropertyValueFactory<>("NSN"));
        coeiNSNColumn.setCellValueFactory(new PropertyValueFactory<>("NSN"));

        // Set the authorized quantity
        biiAuthorizedColumn.setCellValueFactory(new PropertyValueFactory<>("authorizedQuantity"));
        coeiAuthorizedColumn.setCellValueFactory(new PropertyValueFactory<>("authorizedQuantity"));

        // Set the on hand quantity
        biiOnHandColumn.setCellValueFactory(new PropertyValueFactory<>("onHandQuantity"));
        coeiOnHandColumn.setCellValueFactory(new PropertyValueFactory<>("onHandQuantity"));
        // Make sure that we are able to edit the value.
        Callback<TableColumn<EndItemBasicIssueComponent, Integer>, TableCell<EndItemBasicIssueComponent, Integer>> callback =
                TextFieldTableCell.forTableColumn(new IntegerStringConverter());
        biiOnHandColumn.setCellFactory(callback);
        Callback<TableColumn<EndItemComponent, Integer>, TableCell<EndItemComponent, Integer>> callback2 =
                TextFieldTableCell.forTableColumn(new IntegerStringConverter());
        coeiOnHandColumn.setCellFactory(callback2);

        // Make sure we can handle edits directly to the table.
        biiOnHandColumn.setOnEditCommit(new BIIOnHandComponenetHandler());
        coeiOnHandColumn.setOnEditCommit(new COIEOnHandComponentHandler());

        biiOnHandColumn.setEditable(true);
        coeiOnHandColumn.setEditable(true);

        // Make sure we hide the visibility is bound to the layout properties.
        biiTable.visibleProperty().bindBidirectional(biiTable.managedProperty());
        coeiTable.visibleProperty().bindBidirectional(coeiTable.managedProperty());
        biiLabel.visibleProperty().bindBidirectional(biiLabel.managedProperty());
        biiLabel.visibleProperty().bind(biiTable.visibleProperty());
        coeiLabel.visibleProperty().bindBidirectional(coeiLabel.managedProperty());
        coeiLabel.visibleProperty().bind(coeiTable.visibleProperty());

        noItemsLabel.visibleProperty().bindBidirectional(noItemsLabel.managedProperty());
        BooleanBinding anyTablesVisible = biiTable.visibleProperty().or(coeiTable.visibleProperty());
        noItemsLabel.visibleProperty().bind(anyTablesVisible.not());

        item.addListener((ov, oldVal, newVal) -> {
            if (oldVal != null) {
                coeiLabel.textProperty().unbind();
                biiLabel.textProperty().unbind();
                biiTable.visibleProperty().unbind();
                coeiTable.visibleProperty().unbind();
            }

            if (newVal == null) return;

            coeiLabel.textProperty().bind(
                    new SimpleStringProperty("Components for ").concat(
                            newVal.nameProperty()).concat(" (COEI)"));
            biiLabel.textProperty().bind(
                    new SimpleStringProperty("Basic Issue Items for ").concat(
                            newVal.nameProperty()).concat(" (BII)"));

            coeiTable.setItems(newVal.getCOIEComponents());
            biiTable.setItems(newVal.getBIIComponents());
            biiTable.visibleProperty().bind(Bindings.isNotEmpty(biiTable.getItems()));
            coeiTable.visibleProperty().bind(Bindings.isNotEmpty(coeiTable.getItems()));
        });
    }

    private static class COIEOnHandComponentHandler implements
            EventHandler<TableColumn.CellEditEvent<EndItemComponent, Integer>> {

        @Override
        public void handle(TableColumn.CellEditEvent<EndItemComponent, Integer> event) {
            Integer onHand = event.getNewValue();
            if (onHand == null) return;
            AccountableComponent component = event.getRowValue();
            component.setOnHandQuantity(onHand);
        }
    }

    private static class BIIOnHandComponenetHandler implements
            EventHandler<TableColumn.CellEditEvent<EndItemBasicIssueComponent, Integer>> {

        @Override
        public void handle(TableColumn.CellEditEvent<EndItemBasicIssueComponent, Integer> event) {
            Integer onHand = event.getNewValue();
            if (onHand == null) return;
            AccountableComponent component = event.getRowValue();
            component.setOnHandQuantity(onHand);
        }
    }

}
