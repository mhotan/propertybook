package application.controllers.items;


import application.controllers.BaseController;
import application.controllers.filter.FilterChangedListener;
import application.views.filter.EndItemGroupFilterPopOver;
import application.views.items.EndItemDetailsView;
import com.hotan.army.supply.data.EndItemGroup;
import com.hotan.army.supply.data.PropertyBook;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import org.controlsfx.control.PopOver;

import java.util.ArrayList;
import java.util.function.Predicate;
import java.util.logging.Logger;

/**
 * Controller for end item details view.
 *
 * Created by mhotan on 3/7/14.
 */
public class EndItemSearchController extends BaseController implements FilterChangedListener<EndItemGroup> {

    private static final Logger LOG = Logger.getLogger(EndItemSearchController.class.getName());

    @FXML
    private VBox root;

    @FXML
    private SplitPane splitPane;

    @FXML
    private TableColumn<EndItemGroup, String> columnNSN, columnName, columnLIN, columnPubNum, columnPubDate;

    @FXML
    private TableColumn<EndItemGroup, Integer> columnAuth;

    @FXML
    private TableView<EndItemGroup> tableView;

    @FXML
    private TextField searchTextField;

    @FXML
    private Button filterButton;

    /**
     * Entire Property Book
     */
    private final PropertyBook pb;

    /**
     * Distinct list of end items.
     */
    private final FilteredList<EndItemGroup> groups;

    /**
     * Controller for the end item detail view.
     */
    private EndItemDetailsController detailsController;

    private SearchFilter filter;

    public EndItemSearchController(PropertyBook pb) {
        this.pb = pb;
        this.detailsController = new EndItemDetailsController();

        // Populate the list of end item groups.
        this.groups = new FilteredList<>(this.pb.getGroups(), group -> true);
    }

    private void updateFilter() {
        this.groups.setPredicate(group -> true);
        this.groups.setPredicate(filter);
    }

    @FXML
    void onSearch(ActionEvent event) {
        updateFilter();
    }

    @FXML
    protected void initialize() {
        assert columnAuth != null : "fx:id=\"columnAuth\" was not injected: check your FXML file 'EndItemSearch.fxml'.";
        assert columnNSN != null : "fx:id=\"columnNSN\" was not injected: check your FXML file 'EndItemSearch.fxml'.";
        assert tableView != null : "fx:id=\"tableView\" was not injected: check your FXML file 'EndItemSearch.fxml'.";
        assert searchTextField != null : "fx:id=\"searchTextField\" was not injected: check your FXML file 'EndItemSearch.fxml'.";
        assert columnLIN != null : "fx:id=\"columnLIN\" was not injected: check your FXML file 'EndItemSearch.fxml'.";
        assert columnPubNum != null : "fx:id=\"columnPubNum\" was not injected: check your FXML file 'EndItemSearch.fxml'.";
        assert columnPubDate != null : "fx:id=\"columnPubDate\" was not injected: check your FXML file 'EndItemSearch.fxml'.";
        assert splitPane != null : "fx:id=\"splitPane\" was not injected: check your FXML file 'EndItemSearch.fxml'.";
        assert root != null : "fx:id=\"root\" was not injected: check your FXML file 'EndItemSearch.fxml'.";
        assert filterButton != null : "fx:id=\"filterButton\" was not injected: check your FXML file 'EndItemSearch.fxml'.";
        assert columnName != null : "fx:id=\"columnName\" was not injected: check your FXML file 'EndItemSearch.fxml'.";
        super.initialize();
    }

    @Override
    protected void setUpUI() {

        // Set the Cell Value factory for all the table columns
        tableView.managedProperty().bindBidirectional(tableView.visibleProperty());
        columnAuth.setCellValueFactory(new PropertyValueFactory<>("authorizedQty"));
        columnName.setCellValueFactory(new PropertyValueFactory<>("defaultName"));
        columnLIN.setCellValueFactory(new PropertyValueFactory<>("lin"));
        columnNSN.setCellValueFactory(new PropertyValueFactory<>("nsn"));
        columnPubNum.setCellValueFactory(new PropertyValueFactory<>("pubNum"));
        columnPubDate.setCellValueFactory(new PropertyValueFactory<>("pubDate"));

        // Set the End Item Group for the table view.
        tableView.setItems(groups);

        // The table view.
        tableView.getSelectionModel().selectedItemProperty().addListener((oVal, oldVal, newVal) -> {
            if (newVal == null) return;
            detailsController.setGroup(newVal);
            tableView.requestFocus();
        });

        // Add the view and update the view..
        Node view = new EndItemDetailsView(detailsController);
        splitPane.getItems().add(view);

        detailsController.supportViewVisbilityProperty().bind(
                splitPane.getDividers().get(0).positionProperty().lessThan(.20));

        // Initialize filter of this.
        this.filter = new SearchFilter();
        updateFilter();

        // Initialize the popOver for the filter view.
        final PopOver popOver = new EndItemGroupFilterPopOver(this, PopOver.ArrowLocation.RIGHT_CENTER);
        // Add the Pop over view to the
        filterButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (!popOver.isDetachable())
                    popOver.hide();
                popOver.show(filterButton, e.getScreenX(), e.getScreenY());
            }
        });

        // Tool tips
        searchTextField.setTooltip(new Tooltip("Type a little bit of the nomenclature, LIN, NSN in order to " +
                "search within this property book."));
        filterButton.setTooltip(new Tooltip("Provides more advanced filters."));
    }


    @Override
    public void changed(ObservableValue<? extends Predicate<EndItemGroup>> ov,
                        Predicate<EndItemGroup> oldFilter, Predicate<EndItemGroup> newFilter) {
        filter.setInternalFilter(newFilter);
        updateFilter();
    }

    private class SearchFilter implements Predicate<EndItemGroup> {

        private final SimpleObjectProperty<Predicate<EndItemGroup>> internalFilter;

        SearchFilter() {
            internalFilter = new SimpleObjectProperty<Predicate<EndItemGroup>>();
        }

        public void setInternalFilter(Predicate<EndItemGroup> internalFilter) {
            this.internalFilter.set(internalFilter);
        }

        @Override
        public boolean test(EndItemGroup endItemGroup) {
            if (endItemGroup == null) {
                LOG.warning(getClass().getName() + ".test() searching on ILLEGAL null End item group");
                return false;
            }

            Predicate<EndItemGroup> internalFilter = this.internalFilter.get();
            if (internalFilter != null && !internalFilter.test(endItemGroup))
                return false;

            // Build up the list of all possible string we can match on
            String searchText = searchTextField.getText().trim().toLowerCase();
            ArrayList<String> properties = new ArrayList<String>();
            properties.addAll(endItemGroup.getData().getNames());
            properties.add(endItemGroup.getData().getNSN());
            properties.add(endItemGroup.getData().getLIN());

            // test against every property and if we find a match then return true immediately.
            for (String property: properties) {
                property = property.trim().toLowerCase();
                if (property.contains(searchText)) return true;
            }
            return false;
        }
    }

}
