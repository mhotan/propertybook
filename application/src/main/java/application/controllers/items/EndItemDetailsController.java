package application.controllers.items;

import application.controllers.BaseController;
import application.controllers.image.ImagePortfolioController;
import application.controllers.inspection.DeficienciesController;
import application.controllers.items.components.ComponentsController;
import application.controllers.items.location.ItemLocationController;
import application.views.images.ImagePortfolio;
import application.views.items.ComponentsView;
import application.views.items.DeficienciesView;
import application.views.location.ItemLocationView;
import com.hotan.army.supply.data.EndItemGroup;
import com.hotan.army.supply.data.items.EndItem;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * Created by mhotan on 3/8/14.
 */
public class EndItemDetailsController extends BaseController {

    @FXML
    private VBox root;

    @FXML
    private TabPane tabPane;

    @FXML
    private GridPane nsnLinGrid;

    @FXML
    private TextField addNameTextField;

    @FXML
    private Label authQtyLabel, nsnLabel, linLabel;

    @FXML
    private ScrollPane issueTabPane, locationTabPane, componentsTabPane;

    @FXML
    private ListView<String> aliasListView;

    @FXML
    private ListView<EndItem> itemListView;

    @FXML
    private HBox photoPane, addNamePane;


    @FXML
    private Button saveNameButton, addNameButton;

    /**
     * Controller for the items location view.
     */
    private final ItemLocationController locationController;

    /**
     * Controller that manages deficiencies view.
     */
    private final DeficienciesController deficienciesController;

    /**
     * Controller that manages components view.
     */
    private final ComponentsController componentsController;

    /**
     *
     */
    private final ObjectProperty<EndItemGroup> endItemGroup;

    /**
     * Creates and prepares the controller to handle FXML views
     * and handle EndItem input.
     */
    public EndItemDetailsController() {
        this.endItemGroup = new SimpleObjectProperty<>();
        locationController = new ItemLocationController();
        deficienciesController = new DeficienciesController();
        componentsController = new ComponentsController();
    }

    @FXML
    void onAddName(ActionEvent event) {
        addNameButton.setVisible(false);
    }

    @FXML
    void onSaveName(ActionEvent event) {
        String name = addNameTextField.getText().trim();
        addNameTextField.setText(name);
        if (name.isEmpty()) {
            addNameButton.setVisible(true);
            return;
        }
        endItemGroup.get().getData().addName(name);
    }

    @FXML
    protected void initialize() {
        assert addNameTextField != null : "fx:id=\"addNameTextField\" was not injected: check your FXML file 'EndItemDetails.fxml'.";
        assert photoPane != null : "fx:id=\"photoPane\" was not injected: check your FXML file 'EndItemDetails.fxml'.";
        assert locationTabPane != null : "fx:id=\"locationTabPane\" was not injected: check your FXML file 'EndItemDetails.fxml'.";
        assert nsnLinGrid != null : "fx:id=\"nsnLinGrid\" was not injected: check your FXML file 'EndItemDetails.fxml'.";
        assert itemListView != null : "fx:id=\"itemListView\" was not injected: check your FXML file 'EndItemDetails.fxml'.";
        assert nsnLabel != null : "fx:id=\"nsnLabel\" was not injected: check your FXML file 'EndItemDetails.fxml'.";
        assert authQtyLabel != null : "fx:id=\"authQtyLabel\" was not injected: check your FXML file 'EndItemDetails.fxml'.";
        assert addNameButton != null : "fx:id=\"addNameButton\" was not injected: check your FXML file 'EndItemDetails.fxml'.";
        assert addNamePane != null : "fx:id=\"addNamePane\" was not injected: check your FXML file 'EndItemDetails.fxml'.";
        assert saveNameButton != null : "fx:id=\"saveNameButton\" was not injected: check your FXML file 'EndItemDetails.fxml'.";
        assert issueTabPane != null : "fx:id=\"issueTabPane\" was not injected: check your FXML file 'EndItemDetails.fxml'.";
        assert root != null : "fx:id=\"root\" was not injected: check your FXML file 'EndItemDetails.fxml'.";
        assert linLabel != null : "fx:id=\"linLabel\" was not injected: check your FXML file 'EndItemDetails.fxml'.";
        assert tabPane != null : "fx:id=\"tabPane\" was not injected: check your FXML file 'EndItemDetails.fxml'.";
        assert componentsTabPane != null : "fx:id=\"componentsTabPane\" was not injected: check your FXML file 'EndItemDetails.fxml'.";
        super.initialize();
    }

    @Override
    protected void setUpUI() {
        // Bind the grid views visibility to its layout
        nsnLinGrid.managedProperty().bindBidirectional(nsnLinGrid.visibleProperty());

        // Update the End Item list view cell factory to present the
        // end items correctly.
        itemListView.setCellFactory(view -> new EndItemListCell());

        // Populate the tab panes with the components views.
        ItemLocationView locationView = new ItemLocationView(locationController);
        locationView.maxWidthProperty().bind(locationTabPane.widthProperty());
        locationTabPane.setContent(locationView);
        DeficienciesView deficiencyView = new DeficienciesView(deficienciesController);
        deficiencyView.maxWidthProperty().bind(issueTabPane.widthProperty());
        issueTabPane.setContent(deficiencyView);
        ComponentsView componentsView = new ComponentsView(componentsController);
        componentsView.maxWidthProperty().bind(componentsTabPane.widthProperty());
        componentsTabPane.setContent(componentsView);

        // If there is no more end item then hide this view.
        root.visibleProperty().bind(endItemGroup.isNotNull());

        // If there is a selected item then
        tabPane.visibleProperty().bind(itemListView.getSelectionModel().selectedItemProperty().isNotNull());

        // Create bindings for the view that allows adding name.
        addNameButton.visibleProperty().bindBidirectional(addNameButton.managedProperty());
        addNamePane.visibleProperty().bindBidirectional(addNamePane.managedProperty());
        addNamePane.visibleProperty().bind(addNameButton.visibleProperty().not());
        saveNameButton.disableProperty().bind(addNameTextField.textProperty().isEmpty().or(endItemGroup.isNull()));
        addNameButton.disableProperty().bind(endItemGroup.isNull());

        // Create the image view and add it to the root view.
        final ImagePortfolioController controller = new ImagePortfolioController();
        ImagePortfolio portfolio = new ImagePortfolio(controller);
        portfolio.setMaxHeight(Double.MAX_VALUE);
        portfolio.setMaxWidth(Double.MAX_VALUE);
        photoPane.getChildren().add(portfolio);

        // Update the view based on the current endItem group
        endItemGroup.addListener((ov, oldVal, newVal) -> {
            if (oldVal != null) {
                nsnLabel.textProperty().unbind();
                linLabel.textProperty().unbind();
                authQtyLabel.textProperty().unbind();

                // Bind all the controller to the selected
                locationController.itemProperty().unbind();
                deficienciesController.itemProperty().unbind();
                componentsController.itemProperty().unbind();

                // Clear out the list view.
                aliasListView.setItems(null);
                itemListView.setItems(null);
            }

            // No selected end item.
            if (newVal == null) return;

            // Bind the NSN and LIN labels. bind the data because the data will never change for this end item.
            nsnLabel.textProperty().bind(newVal.getData().NSNProperty());
            linLabel.textProperty().bind(newVal.getData().LINProperty());
            // Bind the quantity.
            authQtyLabel.textProperty().bind(Bindings.size(newVal.getItems()).asString());

            // Bind the internal views.
            locationController.itemProperty().bind(itemListView.getSelectionModel().selectedItemProperty());
            deficienciesController.itemProperty().bind(itemListView.getSelectionModel().selectedItemProperty());
            componentsController.itemProperty().bind(itemListView.getSelectionModel().selectedItemProperty());

            // New Selected item.
            // Update the list.

            aliasListView.setItems(newVal.getData().getNames());
            itemListView.setItems(newVal.getItems());

//            // Always select the first name.
//            // Refresh with first element.
//            itemListView.getSelectionModel().select(null);
            itemListView.getSelectionModel().select(0);

            // Set the tool tip.
            authQtyLabel.setTooltip(new Tooltip("The number of this " + endItemGroup.toString() + " that exists in this Property Book"));
        });

        // Tool Tip Configuration.
        nsnLabel.setTooltip(new Tooltip("The National Stock Number of this End Item"));
        linLabel.setTooltip(new Tooltip("The Line Item Number of this End Item"));
    }


    public BooleanProperty supportViewVisbilityProperty() {
        return nsnLinGrid.visibleProperty();
    }

    public void setGroup(EndItemGroup group) {
        endItemGroup.set(group);
    }

    /**
     * List cell to show an end item.
     */
    private static class EndItemListCell extends ListCell<EndItem> {

        @Override
        public void updateItem(EndItem item, boolean empty) {
            super.updateItem(item, empty);
            if (item != null) {
                setText(item.getName());
            } else
                setText(null);
        }

    }
}
