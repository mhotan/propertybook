package application.controllers.items;

import application.controllers.BaseController;
import application.util.loader.ImageLoader;
import com.hotan.army.supply.data.items.EndItem;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

/**
 * Created by mhotan on 3/21/14.
 */
public class EndItemListItemController extends BaseController {

    @FXML
    private Label defaultNameLabel, nsnLabel, linLabel, actualNameLabel, nameRepresentationLabel;

    @FXML
    private HBox root;

    @FXML
    private ImageView imageView;

    private final ObjectProperty<EndItem> item;

    private final ObjectProperty<Image> viewImage;

    /**
     * Creates a controller for the end item.
     *
     * @param item End Item to show.
     */
    public EndItemListItemController(EndItem item) {
        if (item == null)
            throw new NullPointerException(getClass().getSimpleName() + "() EndItem cannot be null.");
        this.item = new SimpleObjectProperty<>(item);
        this.viewImage = new SimpleObjectProperty<>();

    }

    @FXML
    protected void initialize() {
        assert nameRepresentationLabel != null : "fx:id=\"nameRepresentationLabel\" was not injected: check your FXML file 'EndItemListCell.fxml'.";
        assert defaultNameLabel != null : "fx:id=\"defaultNameLabel\" was not injected: check your FXML file 'EndItemListCell.fxml'.";
        assert nsnLabel != null : "fx:id=\"nsnLabel\" was not injected: check your FXML file 'EndItemListCell.fxml'.";
        assert root != null : "fx:id=\"root\" was not injected: check your FXML file 'EndItemListCell.fxml'.";
        assert imageView != null : "fx:id=\"imageView\" was not injected: check your FXML file 'EndItemListCell.fxml'.";
        assert linLabel != null : "fx:id=\"linLabel\" was not injected: check your FXML file 'EndItemListCell.fxml'.";
        assert actualNameLabel != null : "fx:id=\"actualNameLabel\" was not injected: check your FXML file 'EndItemListCell.fxml'.";
        super.initialize();
    }

    @Override
    protected void setUpUI() {
        defaultNameLabel.textProperty().bind(this.item.get().getData().defaultNameProperty());
        nsnLabel.textProperty().bind(this.item.get().NSNProperty());
        linLabel.textProperty().bind(this.item.get().LINProperty());
        nameRepresentationLabel.textProperty().set(this.item.get().getNameLabel());
        actualNameLabel.textProperty().bind(this.item.get().nameProperty());

        // bind the image to the overview image.
        this.viewImage.addListener((ov, oldVal, newVal) -> {
            if (newVal == null)
                newVal = ImageLoader.getNoImageAvailableImage();
            imageView.setImage(newVal);
        });
        this.viewImage.bind(item.get().getData().overviewImageProperty());
    }
}
