package application.controllers;

import javafx.fxml.FXML;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialogs;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by mhotan on 3/19/14.
 */
public abstract class BaseController {

    @FXML
    protected ResourceBundle resources;

    @FXML
    protected URL location;

    /**
     * Sets up the interface to for the application.
     */
    protected abstract void setUpUI();

    @FXML
    protected void initialize() {
        // TODO Current not using asserts to check for FXML internal members.
        setUpUI();
    }

    /**
     * Show alert dialog with the error message inputted.  Null or empty titles
     * will be replaced by default titles.
     *
     * @param owner Owning node for this dialog message.
     * @param title Title of the error message.
     * @param errorMessage Message to show. Cannot be null or empty
     * @return The action response from the user.
     */
    protected Action showError(Object owner, String title, String errorMessage) {
        if (errorMessage == null || errorMessage.trim().isEmpty())
            throw new IllegalArgumentException("Error message cannot be null.");
        Dialogs d = configureDialog(owner, Dialogs.create());
        if (title == null || title.trim().isEmpty())
            title = "Looks like there was an Error!";
        return d.title(title).message(errorMessage.trim()).showError();
    }

    /**
     * Shows an exception that occured.
     *
     * @param owner Owning Node for the dialog, null if there is no owner.
     * @param title Title of the dialog
     * @param e Exception to show.
     * @return The action the user took for this.
     */
    protected Action showException(Object owner, String title, Exception e) {
        Dialogs d = configureDialog(owner, Dialogs.create());
        if (title == null || title.trim().isEmpty())
            title = "Looks like there was an Exception!";
        return d.masthead("Exception Encountered").title(title).showException(e);
    }

    /**
     * Shows a confirmation dialog.
     *
     * @param owner Owning Node for the dialog, null if there is no owner.
     * @param title Title of the dialog
     * @param message Message of the dialog
     * @return the action returned from the dialog.
     */
    protected Action showConfirmation(Object owner, String title, String message, Action[] actions) {
        if (title == null || title.trim().isEmpty())
            throw new IllegalArgumentException(getClass().getSimpleName() + "showConfirmation() Cannot have null title");
        if (message == null || message.trim().isEmpty())
            throw new IllegalArgumentException(getClass().getSimpleName() + "showConfirmation() Cannot have null message");
        Dialogs d = configureDialog(owner, Dialogs.create());
        return d.title(title.trim()).message(message.trim()).owner(owner).showConfirm();
    }

    /**
     * Show a dialog saying the feature is not just yet implemented.
     *
     * @param owner Owning node for this dialog message.
     */
    protected void showNotYetImplemented(Object owner) {
        Dialogs d = configureDialog(owner, Dialogs.create());
        d.title("Not Implemented Yet").message("Apologies! This feature " +
                "is not implemented just yet.").showInformation();
    }

    private Dialogs configureDialog(Object owner, Dialogs dialog) {
        // Make sure the dialog appears to be a native OS dependent view.
        dialog.nativeTitleBar();

        // If there is an owner to set this dialog to.
        // Then set it now.
        if (owner != null) {
            dialog.owner(owner);
        }

        // Make sure dialog can't leave the scene graph.
        dialog.lightweight();

        return dialog;
    }

}
