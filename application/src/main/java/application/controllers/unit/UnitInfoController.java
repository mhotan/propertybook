package application.controllers.unit;

import application.controllers.BaseController;
import application.util.cells.OperatorListCellFactory;
import com.hotan.army.supply.data.PropertyBook;
import com.hotan.army.supply.data.personnel.Operator;
import com.hotan.army.supply.data.personnel.OperatorManager;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by mhotan on 3/10/14.
 */
public class UnitInfoController extends BaseController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ComboBox<Operator> juniorCharlieCB, seniorBravoCB, seniorCharlieCB, warrantCB, seniorEchoCB, companyCommanderCB,
            companyS4CB, juniorEchoCB, teamCommanderCB, seniorDeltaCB, teamSergeantCB, juniorBravoCB,
            intelSgtCB, juniorDeltaCB;

    private final PropertyBook pb;

    private final OperatorListCellFactory cellFactory;

    private final ObservableList<Operator> operators;

    /**
     * Creates a unit info controller that handles the interaction
     * between the user and the unit info view.
     *
     * @param pb Property book to use for this UnitInfo.
     */
    public UnitInfoController(PropertyBook pb) {
        this.pb = pb;
        cellFactory = new OperatorListCellFactory();
        operators = OperatorManager.getInstance().getAll();;
    }

    @FXML
    protected void initialize() {
        assert juniorCharlieCB != null : "fx:id=\"juniorCharlieCB\" was not injected: check your FXML file 'UnitInfo.fxml'.";
        assert seniorBravoCB != null : "fx:id=\"seniorBravoCB\" was not injected: check your FXML file 'UnitInfo.fxml'.";
        assert seniorCharlieCB != null : "fx:id=\"seniorCharlieCB\" was not injected: check your FXML file 'UnitInfo.fxml'.";
        assert warrantCB != null : "fx:id=\"warrantCB\" was not injected: check your FXML file 'UnitInfo.fxml'.";
        assert seniorEchoCB != null : "fx:id=\"seniorEchoCB\" was not injected: check your FXML file 'UnitInfo.fxml'.";
        assert companyCommanderCB != null : "fx:id=\"companyCommanderCB\" was not injected: check your FXML file 'UnitInfo.fxml'.";
        assert companyS4CB != null : "fx:id=\"companyS4CB\" was not injected: check your FXML file 'UnitInfo.fxml'.";
        assert juniorEchoCB != null : "fx:id=\"juniorEchoCB\" was not injected: check your FXML file 'UnitInfo.fxml'.";
        assert teamCommanderCB != null : "fx:id=\"teamCommanderCB\" was not injected: check your FXML file 'UnitInfo.fxml'.";
        assert seniorDeltaCB != null : "fx:id=\"seniorDeltaCB\" was not injected: check your FXML file 'UnitInfo.fxml'.";
        assert teamSergeantCB != null : "fx:id=\"teamSergeantCB\" was not injected: check your FXML file 'UnitInfo.fxml'.";
        assert juniorBravoCB != null : "fx:id=\"juniorBravoCB\" was not injected: check your FXML file 'UnitInfo.fxml'.";
        assert intelSgtCB != null : "fx:id=\"intelSgtCB\" was not injected: check your FXML file 'UnitInfo.fxml'.";
        assert juniorDeltaCB != null : "fx:id=\"juniorDeltaCB\" was not injected: check your FXML file 'UnitInfo.fxml'.";
        super.initialize();
    }

    @Override
    protected void setUpUI() {
        processComboBox(juniorCharlieCB);
        processComboBox(seniorBravoCB);
        processComboBox(seniorCharlieCB);
        processComboBox(warrantCB);
        processComboBox(seniorEchoCB);
        processComboBox(companyCommanderCB);
        processComboBox(companyS4CB);
        processComboBox(juniorEchoCB);
        processComboBox(teamCommanderCB);
        processComboBox(seniorDeltaCB);
        processComboBox(teamSergeantCB);
        processComboBox(juniorBravoCB);
        processComboBox(intelSgtCB);
        processComboBox(juniorDeltaCB);
    }

    private void processComboBox(ComboBox<Operator> box) {
        box.setCellFactory(cellFactory);
        box.setItems(operators);
        box.setButtonCell(new OperatorCell());
    }

    private static class OperatorCell extends ListCell<Operator> {

        @Override
        protected void updateItem(Operator operator, boolean b) {
            super.updateItem(operator, b);
            if (operator != null) {
                setText(OperatorListCellFactory.processOperator(operator));
            } else
                setText(null);
        }
    }
}
