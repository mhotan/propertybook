package application.controllers.operator;

import application.controllers.BaseController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

/**
 * Created by mhotan on 3/23/14.
 */
public class AddOperatorController extends BaseController {

    @FXML
    private ComboBox<?> rankBox;

    @FXML
    Button addButton, saveButton;

    @FXML
    private ComboBox<?> mosBox;

    @FXML
    private TextField lastNameField, firstNameField, middleNameField;

    @FXML
    void onSaveNewOperator(ActionEvent event) {

    }

    @FXML
    void onCreateNew(ActionEvent event) {

    }

    @FXML
    protected void initialize() {
        assert rankBox != null : "fx:id=\"rankBox\" was not injected: check your FXML file 'AddOperatorView.fxml'.";
        assert addButton != null : "fx:id=\"addButton\" was not injected: check your FXML file 'AddOperatorView.fxml'.";
        assert mosBox != null : "fx:id=\"mosBox\" was not injected: check your FXML file 'AddOperatorView.fxml'.";
        assert lastNameField != null : "fx:id=\"lastNameField\" was not injected: check your FXML file 'AddOperatorView.fxml'.";
        assert saveButton != null : "fx:id=\"saveButton\" was not injected: check your FXML file 'AddOperatorView.fxml'.";
        assert firstNameField != null : "fx:id=\"firstNameField\" was not injected: check your FXML file 'AddOperatorView.fxml'.";
        assert middleNameField != null : "fx:id=\"middleNameField\" was not injected: check your FXML file 'AddOperatorView.fxml'.";
        super.initialize();
    }

    @Override
    protected void setUpUI() {

        // Bind the button visibility to its layout properties
        addButton.visibleProperty().bindBidirectional(addButton.managedProperty());
        saveButton.visibleProperty().bindBidirectional(saveButton.managedProperty());

    }
}
