package application.controllers.operator;

import application.controllers.BaseController;
import application.controllers.items.EndItemListItemController;
import application.util.listeners.NumberTextChangeListener;
import application.util.listeners.RestrictiveTextListener;
import application.views.items.EndItemListItemView;
import com.hotan.army.supply.data.PropertyBook;
import com.hotan.army.supply.data.items.EndItem;
import com.hotan.army.supply.data.personnel.MOS;
import com.hotan.army.supply.data.personnel.Operator;
import com.hotan.army.supply.data.personnel.OperatorManager;
import com.hotan.army.supply.data.personnel.Rank;
import com.hotan.army.supply.data.tracking.SignedOutRecord;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.StringExpression;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.parse4j.ParseException;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by mhotan on 3/10/14.
 */
public class OperatorController extends BaseController {

    /**
     * default predicate for showing no values at all.
     */
    private static final Predicate<EndItem> REFRESH_PREDICATE = item -> true;
    private static final Predicate<EndItem> DEFAULT_CURRENT_PREDICATE = item -> false;
    private static final Predicate<EndItem> DEFAULT_AVAILABLE_PREDICATE = item ->
            item.currentSignedOutRecordProperty().isNull().get();

    @FXML
    private Button printHandReceiptButton, saveNewOperatorButton;

    @FXML
    private TitledPane contactInfoPane;

    @FXML
    private VBox imageContainer;

    @FXML
    private ListView<EndItem> currentEndItemsList, availableEndItemList;

    @FXML
    private TabPane tabPane;

    @FXML
    private Tab supplyTab, medicalTab, administrativeTab;

    @FXML
    private ComboBox<Rank> rankComboBox;

    @FXML
    private TextField firstNameField, lastNameField, subscriberNumberField, areaCodeField,
            middleInitialField, exchangeCodeField, searchAvailableItemsTextField;

    @FXML
    private HBox saveButtonBar;

    @FXML
    private ImageView operatorImageView;

    @FXML
    private ComboBox<MOS> mosComboBox;

    @FXML
    private Button filterButton, searchButton;

    /**
     * Property book for itemdata.
     */
    private final PropertyBook pb;

    /**
     * The property field that holds a single object property.
     */
    private final ObjectProperty<Operator> operator;

    private final FilteredList<EndItem> availableList, currentList;

    /**
     * Key -> Available list
     * Value -> List of enditems assigned to current operator
     */
    private Predicate<EndItem> operatorPredicate;

    /**
     * Creates a Controller for the Operator View.
     *
     * @param pb PropertyBook to create.
     */
    public OperatorController(PropertyBook pb) {
        this.pb = pb;
        operator = new SimpleObjectProperty<>(null);
        operatorPredicate = DEFAULT_CURRENT_PREDICATE;
        availableList = new FilteredList<EndItem>(this.pb.getEndItems(), DEFAULT_AVAILABLE_PREDICATE);
        currentList = new FilteredList<EndItem>(this.pb.getEndItems(), operatorPredicate);
    }

    /**
     * @return The Operator that is current shown.
     */
    public Operator getOperator() {
        return operator.get();
    }

    /**
     * @return The Operator property.
     */
    public ObjectProperty<Operator> operatorProperty() {
        return operator;
    }

    /**
     * Set the operator of this controller to populate the view.
     *
     * @param operator Operator to set this at.
     */
    public void setOperator(Operator operator) {
        this.operator.set(operator);
    }

    @FXML
    void onPrintHandReceipt(ActionEvent event) {
        // TODO Print the Hand Receipt
    }

    @FXML
    void onSearchAvailableItems(ActionEvent event) {
        refreshLists();
    }

    @FXML
    void onSaveNewOperator(ActionEvent event) {
        // TODO Create a new Operator
        String firstName = firstNameField.getText();
        String lastName = lastNameField.getText();
        String middleName = middleInitialField.getText();
        MOS mos = mosComboBox.getValue();
        Rank rank = rankComboBox.getValue();

        try {
            Operator operator = OperatorManager.getInstance().add(firstName, middleName, lastName, rank, mos);
            setOperator(operator);
        } catch (ParseException e) {
            showException(null, "Unable to Create and save a new operator", e);
        }
    }

    @FXML
    protected void initialize() {
        assert printHandReceiptButton != null : "fx:id=\"printHandReceiptButton\" was not injected: check your FXML file 'Operator.fxml'.";
        assert searchButton != null : "fx:id=\"searchButton\" was not injected: check your FXML file 'Operator.fxml'.";
        assert contactInfoPane != null : "fx:id=\"contactInfoPane\" was not injected: check your FXML file 'Operator.fxml'.";
        assert imageContainer != null : "fx:id=\"imageContainer\" was not injected: check your FXML file 'Operator.fxml'.";
        assert administrativeTab != null : "fx:id=\"administrativeTab\" was not injected: check your FXML file 'Operator.fxml'.";
        assert medicalTab != null : "fx:id=\"medicalTab\" was not injected: check your FXML file 'Operator.fxml'.";
        assert searchAvailableItemsTextField != null : "fx:id=\"searchAvailableItemsTextField\" was not injected: check your FXML file 'Operator.fxml'.";
        assert currentEndItemsList != null : "fx:id=\"currentEndItemsList\" was not injected: check your FXML file 'Operator.fxml'.";
        assert lastNameField != null : "fx:id=\"lastNameField\" was not injected: check your FXML file 'Operator.fxml'.";
        assert supplyTab != null : "fx:id=\"supplyTab\" was not injected: check your FXML file 'Operator.fxml'.";
        assert firstNameField != null : "fx:id=\"firstNameField\" was not injected: check your FXML file 'Operator.fxml'.";
        assert rankComboBox != null : "fx:id=\"rankComboBox\" was not injected: check your FXML file 'Operator.fxml'.";
        assert subscriberNumberField != null : "fx:id=\"subscriberNumberField\" was not injected: check your FXML file 'Operator.fxml'.";
        assert areaCodeField != null : "fx:id=\"areaCodeField\" was not injected: check your FXML file 'Operator.fxml'.";
        assert availableEndItemList != null : "fx:id=\"availableEndItemList\" was not injected: check your FXML file 'Operator.fxml'.";
        assert middleInitialField != null : "fx:id=\"middleInitialField\" was not injected: check your FXML file 'Operator.fxml'.";
        assert saveButtonBar != null : "fx:id=\"saveButtonBar\" was not injected: check your FXML file 'Operator.fxml'.";
        assert operatorImageView != null : "fx:id=\"operatorImageView\" was not injected: check your FXML file 'Operator.fxml'.";
        assert exchangeCodeField != null : "fx:id=\"exchangeCodeField\" was not injected: check your FXML file 'Operator.fxml'.";
        assert filterButton != null : "fx:id=\"filterButton\" was not injected: check your FXML file 'Operator.fxml'.";
        assert saveNewOperatorButton != null : "fx:id=\"saveNewOperatorButton\" was not injected: check your FXML file 'Operator.fxml'.";
        assert tabPane != null : "fx:id=\"tabPane\" was not injected: check your FXML file 'Operator.fxml'.";
        assert mosComboBox != null : "fx:id=\"mosComboBox\" was not injected: check your FXML file 'Operator.fxml'.";
        super.initialize();
    }

    @Override
    protected void setUpUI() {

        filterButton.setOnAction(e -> showNotYetImplemented(null));
        printHandReceiptButton.setOnAction(e -> showNotYetImplemented(null));

        // Initially hide the button bar
        saveButtonBar.visibleProperty().bindBidirectional(saveButtonBar.managedProperty());
        saveButtonBar.visibleProperty().bind(operator.isNull());

        // Bind the button to the operator not be null.
        BooleanBinding disable = operator.isNotNull();
        disable = disable.or(firstNameField.textProperty().isEmpty());
        disable = disable.or(lastNameField.textProperty().isEmpty());
        saveNewOperatorButton.disableProperty().bind(disable);

        // When no operator is set then hide the supporting views
        tabPane.visibleProperty().bindBidirectional(tabPane.managedProperty());
        contactInfoPane.visibleProperty().bindBidirectional(contactInfoPane.managedProperty());
        tabPane.visibleProperty().bindBidirectional(contactInfoPane.visibleProperty());

        // When the button bar is showing then
        tabPane.visibleProperty().bind(saveButtonBar.visibleProperty().not());
        tabPane.getSelectionModel().select(2);

        // Set the value of the ranks
        rankComboBox.setItems(Rank.RANKS);
        // Set the value of the mos's
        mosComboBox.setItems(MOS.MOSES);

        // Set Up drap and drop
        setupDragAndDrop();

        operator.addListener(new ChangeListener<Operator>() {
            @Override
            public void changed(ObservableValue<? extends Operator> observableValue,
                                Operator oldVal, Operator newVal) {
                if (oldVal != null) { // Remove the old bindings
                    firstNameField.textProperty().unbindBidirectional(oldVal.firstNameProperty());
                    middleInitialField.textProperty().unbindBidirectional(oldVal.middleNameProperty());
                    lastNameField.textProperty().unbindBidirectional(oldVal.lastNameProperty());
                    rankComboBox.valueProperty().unbindBidirectional(oldVal.rankProperty());
                    mosComboBox.valueProperty().unbindBidirectional(oldVal.MOSProperty());
                    // Unbind the old phonenumber change.
                    oldVal.phoneNumberProperty().unbind();
                    areaCodeField.setText("");
                    exchangeCodeField.setText("");
                    subscriberNumberField.setText("");

                }

                // Set the operator view read for
                if (newVal == null) {
                    // Clear out all the text properties
                    firstNameField.setText("");
                    middleInitialField.setText("");
                    lastNameField.setText("");
                    rankComboBox.getSelectionModel().select(null);
                    mosComboBox.getSelectionModel().select(null);

                    operatorPredicate = DEFAULT_CURRENT_PREDICATE;
                    refreshLists();
                    return;
                }

                // Update with the new values.
                firstNameField.textProperty().bindBidirectional(newVal.firstNameProperty());
                middleInitialField.textProperty().bindBidirectional(newVal.middleNameProperty());
                lastNameField.textProperty().bindBidirectional(newVal.lastNameProperty());
                rankComboBox.valueProperty().bindBidirectional(newVal.rankProperty());
                mosComboBox.valueProperty().bindBidirectional(newVal.MOSProperty());
                String phoneNumber = newVal.getPhoneNumber();

                // Set the phone number to the appropiate value
                if (phoneNumber != null && !phoneNumber.isEmpty()) {

                    // Find the area code.
                    int endOfAreaCode =  Math.min(3, phoneNumber.length());
                    areaCodeField.setText(phoneNumber.substring(0, endOfAreaCode));
                    phoneNumber = phoneNumber.substring(endOfAreaCode);

                    // Find the exchange code in the current string
                    int exchangeNumber = Math.min(3, phoneNumber.length());
                    exchangeCodeField.setText(phoneNumber.substring(0, exchangeNumber));
                    phoneNumber = phoneNumber.substring(exchangeNumber);

                    // Find the last 4 suscriber field.
                    subscriberNumberField.setText(phoneNumber);
                }

                // Any changes to the phone number should be recipricated
                newVal.phoneNumberProperty().bind(areaCodeField.textProperty().concat(
                        exchangeCodeField.textProperty().concat(
                                subscriberNumberField.textProperty())));

                // TODO Every time the operator is set update the predicates on the list view.
                operatorPredicate = item -> {
                    SignedOutRecord record = item.getCurrentSignedOutRecord();
                    return record != null && record.getIndividual().equals(newVal);
                };
                refreshLists();
            }
        });

        // Set up how to present the list items.
        currentEndItemsList.setCellFactory(view -> new EndItemListCell(currentEndItemsList.widthProperty()));
        availableEndItemList.setCellFactory(view -> new EndItemListCell(availableEndItemList.widthProperty()));

        // Initial set the data to present in the view.
        availableEndItemList.setItems(availableList);
        currentEndItemsList.setItems(currentList);

        // Restrict the text values to only number in the phone number area.
        RestrictiveTextListener areaCodeRestriction = new NumberTextChangeListener(exchangeCodeField.textProperty(), 3);
        areaCodeField.textProperty().addListener(areaCodeRestriction);

        RestrictiveTextListener exchangeRestriction = new NumberTextChangeListener(exchangeCodeField.textProperty(), 3);
        exchangeCodeField.textProperty().addListener(exchangeRestriction);

        RestrictiveTextListener subscriberRestriction = new NumberTextChangeListener(exchangeCodeField.textProperty(), 4);
        subscriberNumberField.textProperty().addListener(subscriberRestriction);

        refreshLists();

        // Tool tips
        searchAvailableItemsTextField.setTooltip(new Tooltip("Type a little bit of the nomenclature, LIN, NSN in order to " +
                "search within this property book."));
        StringExpression searchExpression = new SimpleStringProperty("Search for \"").concat(
                searchAvailableItemsTextField.textProperty()).concat("\"");
        Tooltip searchButtonTip = new Tooltip();
        searchButtonTip.textProperty().bind(searchExpression);
        searchButton.setTooltip(searchButtonTip);
        rankComboBox.setTooltip(new Tooltip("Select the rank of this Operator"));
        mosComboBox.setTooltip(new Tooltip("Select the MOS identifier for this Operator"));
    }

    private void refreshLists() {
        availableList.setPredicate(REFRESH_PREDICATE);
        currentList.setPredicate(REFRESH_PREDICATE);
        availableList.setPredicate(getAvailablePredicate());
        currentList.setPredicate(operatorPredicate);
    }

    private static final String SPLITTER = "        split         ";

    private void setupDragAndDrop() {

        // Set the items in each view.
        availableEndItemList.setOnDragDetected(e -> {
            Dragboard dragboard = availableEndItemList.startDragAndDrop(TransferMode.MOVE);
            ClipboardContent content = new ClipboardContent();
            EndItem item = availableEndItemList.getSelectionModel().getSelectedItem();
            content.putString(item.getNSN() + SPLITTER + item.getLIN() + SPLITTER + item.getName());
            dragboard.setContent(content);
        });
        currentEndItemsList.setOnDragDetected(e -> {
            Dragboard dragboard = currentEndItemsList.startDragAndDrop(TransferMode.MOVE);
            ClipboardContent content = new ClipboardContent();
            EndItem item = currentEndItemsList.getSelectionModel().getSelectedItem();
            content.putString(item.getNSN() + SPLITTER + item.getLIN() + SPLITTER + item.getName());
            dragboard.setContent(content);
        });

        currentEndItemsList.setOnDragEntered(e -> {
            currentEndItemsList.setBlendMode(BlendMode.DIFFERENCE);
        });
        currentEndItemsList.setOnDragExited(e -> {
            currentEndItemsList.setBlendMode(null);
        });
        availableEndItemList.setOnDragEntered(e -> {
            availableEndItemList.setBlendMode(BlendMode.DIFFERENCE);
        });
        availableEndItemList.setOnDragExited(e -> {
            availableEndItemList.setBlendMode(null);
        });


        currentEndItemsList.setOnDragOver(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent dragEvent) {
                dragEvent.acceptTransferModes(TransferMode.MOVE);
            }
        });
        availableEndItemList.setOnDragOver(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent dragEvent) {
                dragEvent.acceptTransferModes(TransferMode.MOVE);
            }
        });


        currentEndItemsList.setOnDragDropped(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent dragEvent) {
                String[] line = dragEvent.getDragboard().getString().split(SPLITTER);
                if (line.length != 3) {
                    dragEvent.setDropCompleted(false);
                    return;
                }
                String nsn = line[0];
                String lin = line[1];
                String name = line[2];

                // Grab the end item.
                EndItem foundItem = pb.getEndItems().stream().filter(item -> item.getName().equals(name)
                        && item.getNSN().equals(nsn) && item.getLIN().equals(lin)).findFirst().orElse(null);
                if (foundItem == null) {
                    dragEvent.setDropCompleted(false);
                    return;
                }

                foundItem.setCurrentSignedOutRecord(new SignedOutRecord(operator.get()));
                refreshLists();
//                availableList.remove(item);
//                currentList.add(toRemove);
                dragEvent.setDropCompleted(true);
            }
        });
        availableEndItemList.setOnDragDropped(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent dragEvent) {
                String[] line = dragEvent.getDragboard().getString().split(SPLITTER);
                if (line.length != 3) {
                    dragEvent.setDropCompleted(false);
                    return;
                }
                String nsn = line[0];
                String lin = line[1];
                String name = line[2];

                // Grab the end item off the other list.
                EndItem foundItem = pb.getEndItems().stream().filter(item -> item.getName().equals(name)
                        && item.getNSN().equals(nsn) && item.getLIN().equals(lin)).findFirst().orElse(null);
                if (foundItem == null) {
                    dragEvent.setDropCompleted(false);
                    return;
                }

                SignedOutRecord record = foundItem.getCurrentSignedOutRecord();
                if (record == null) return;

                // Set the current signed in time.
                foundItem.setCurrentSignedOutRecord(null);
                record.setDateSignedIn(LocalDate.now());
                refreshLists();
                dragEvent.setDropCompleted(true);
            }
        });


    }

    /**
     * How to show an end item list cell.
     */
    private static class EndItemListCell extends ListCell<EndItem> {

        private final ReadOnlyDoubleProperty width;

        EndItemListCell(ReadOnlyDoubleProperty widthProperty) {
            if (widthProperty == null)
                throw new NullPointerException();
            width = widthProperty;
        }

        @Override
        protected void updateItem(EndItem item, boolean empty) {
            super.updateItem(item, empty);
            if (item != null) {
                EndItemListItemController controller = new EndItemListItemController(item);
                EndItemListItemView view = new EndItemListItemView(controller);
                // Set the view to show to the user.
                setGraphic(view);
                view.prefWidthProperty().bind(width);
            } else {
                setGraphic(null);
            }
        }
    }

    private Predicate<EndItem> getAvailablePredicate() {
        return enditem -> {

            // If it is already signed out then it cannot possibly be available
            if (enditem.getCurrentSignedOutRecord() != null) return false;

            // Then match against the main properties of the item.
            final String match = searchAvailableItemsTextField.getText().toLowerCase().trim();
            List<String> properties = new ArrayList<>();
            properties.add(enditem.getNSN().toLowerCase());
            properties.add(enditem.getLIN().toLowerCase());
            properties.add(enditem.getName().toLowerCase());
            enditem.getData().getNames().stream().forEach(nomen -> properties.add(nomen.toLowerCase()));
            return properties.stream().anyMatch(property -> property.contains(match));
        };
    }

}
