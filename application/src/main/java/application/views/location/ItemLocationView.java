package application.views.location;

import application.controllers.items.location.ItemLocationController;
import application.util.loader.StylesheetLoader;
import application.util.loader.ViewLoader;
import javafx.scene.layout.HBox;

import java.io.FileNotFoundException;

/**
 * Created by mhotan on 3/8/14.
 */
public class ItemLocationView extends HBox {

    public ItemLocationView(ItemLocationController controller) {
        ViewLoader.loadView("ItemLocation.fxml", this, controller);
        try {
            StylesheetLoader.load(this, "base.css", "location.css");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

}
