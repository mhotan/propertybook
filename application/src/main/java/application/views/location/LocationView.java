package application.views.location;

import application.controllers.items.location.LocationController;
import application.util.loader.StylesheetLoader;
import application.util.loader.ViewLoader;
import javafx.scene.layout.VBox;

import java.io.FileNotFoundException;

/**
 * Created by mhotan on 3/24/14.
 */
public class LocationView extends VBox {

    public LocationView(LocationController controller) {
        ViewLoader.loadView("Location.fxml", this, controller);

        try {
            StylesheetLoader.load(this, "base.css", "location.css");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

}
