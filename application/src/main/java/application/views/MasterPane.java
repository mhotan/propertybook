package application.views;

import application.controllers.main.master.MasterPaneController;
import application.util.loader.StylesheetLoader;
import application.util.loader.ViewLoader;
import javafx.scene.layout.BorderPane;

import java.io.FileNotFoundException;

/**
 * Created by mhotan on 3/19/14.
 */
public class MasterPane extends BorderPane {

    public MasterPane(MasterPaneController controller) {
        ViewLoader.loadView("MasterPane.fxml", this, controller);

        try {
            StylesheetLoader.load(this);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

    }

}
