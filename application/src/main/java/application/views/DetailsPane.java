package application.views;

import application.controllers.main.details.DetailsPaneController;
import application.util.loader.StylesheetLoader;
import application.util.loader.ViewLoader;
import javafx.scene.layout.VBox;

import java.io.FileNotFoundException;

/**
 * Created by mhotan on 3/20/14.
 */
public class DetailsPane extends VBox {

    public DetailsPane(DetailsPaneController controller) {
        ViewLoader.loadView("DetailsPane.fxml", this, controller);

        try {
            StylesheetLoader.load(this, "menu.css");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

}
