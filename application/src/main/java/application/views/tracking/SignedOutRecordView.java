package application.views.tracking;

import application.controllers.tracking.SignedOutRecordController;
import application.util.loader.StylesheetLoader;
import application.util.loader.ViewLoader;
import javafx.scene.layout.VBox;

import java.io.FileNotFoundException;

/**
 * Created by mhotan on 3/24/14.
 */
public class SignedOutRecordView extends VBox {

    public SignedOutRecordView(SignedOutRecordController controller) {
        ViewLoader.loadView("SignedOutRecord.fxml", this, controller);

        try {
            StylesheetLoader.load(this, "signed-out-record.css");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

}
