package application.views.images;

import application.controllers.image.ImagePortfolioController;
import application.util.loader.StylesheetLoader;
import application.util.loader.ViewLoader;
import javafx.scene.layout.VBox;

import java.io.FileNotFoundException;

/**
 * Created by mhotan on 3/20/14.
 */
public class ImagePortfolio extends VBox {

     public ImagePortfolio(ImagePortfolioController controller) {
         ViewLoader.loadView("ImagePortfolio.fxml", this, controller);

         try {
             StylesheetLoader.load(this, "base.css", "image-portfolio.css");
         } catch (FileNotFoundException e) {
             throw new RuntimeException(e);
         }
     }

}
