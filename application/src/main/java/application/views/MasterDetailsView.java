package application.views;

import application.controllers.main.MasterDetailsController;
import application.util.loader.StylesheetLoader;
import javafx.geometry.Side;
import org.controlsfx.control.MasterDetailPane;

import java.io.FileNotFoundException;

/**
 * Created by mhotan on 3/7/14.
 */
public class MasterDetailsView extends MasterDetailPane {

    /**
     * Creates a Menu and Detail view.
     *
     * @param controller Controller for this view.
     */
    public MasterDetailsView(MasterDetailsController controller) {
        super();
        setAnimated(true);
        setDetailSide(Side.LEFT);
        controller.setView(this);

        try {
            StylesheetLoader.load(this);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

}
