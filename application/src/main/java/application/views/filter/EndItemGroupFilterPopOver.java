package application.views.filter;

import application.controllers.filter.EndItemGroupFilterController;
import application.controllers.filter.FilterChangedListener;
import com.hotan.army.supply.data.EndItemGroup;

/**
 * Created by mhotan on 3/20/14.
 */
public class EndItemGroupFilterPopOver extends FilterPopOver {

    public EndItemGroupFilterPopOver(FilterChangedListener<EndItemGroup> listener, ArrowLocation location) {
        super(location);
        if (listener == null)
            throw new NullPointerException("FilterChangedListener cannot be null");

        EndItemGroupFilterController controller = new EndItemGroupFilterController(listener);
        EndItemFilterView view = new EndItemFilterView(controller);
        setContentNode(view);
    }
}
