package application.views.filter;

import application.controllers.filter.EndItemGroupFilterController;
import application.util.loader.StylesheetLoader;
import application.util.loader.ViewLoader;
import javafx.scene.layout.VBox;

import java.io.FileNotFoundException;

/**
 * Created by mhotan on 3/20/14.
 */
public class EndItemFilterView extends VBox {

    public EndItemFilterView(EndItemGroupFilterController contoller) {
        ViewLoader.loadView("EndItemGroupFilter.fxml", this, contoller);

        try {
            StylesheetLoader.load(this, "base.css", "enditemgroup-filter.css");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

}
