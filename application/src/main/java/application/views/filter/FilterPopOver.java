package application.views.filter;

import org.controlsfx.control.PopOver;

/**
 * Created by mhotan on 3/20/14.
 */
public class FilterPopOver extends PopOver {

    private static final double DEFAULT_ARROW_SIZE = 20;
    private static final double DEFAULT_ARROW_INDENT = 30;
    private static final double DEFAULT_CORNER_RADIUS = 10;

    public FilterPopOver(ArrowLocation location) {
        super();
        setArrowLocation(location);
        setArrowSize(DEFAULT_ARROW_SIZE);
        setArrowIndent(DEFAULT_ARROW_INDENT);
        setCornerRadius(DEFAULT_CORNER_RADIUS);
    }

}
