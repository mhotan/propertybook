package application.views.login;

import application.storage.FileAdapter;

/**
 * Created by mhotan_dev on 1/13/14.
 */
public interface LogInCallback {

    public void onLoginSuccess(FileAdapter adapter);

    public void onLoginFailed(String error);

    public void onLoginCancelled();

}
