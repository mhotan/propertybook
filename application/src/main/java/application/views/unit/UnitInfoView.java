package application.views.unit;

import application.controllers.unit.UnitInfoController;
import application.util.loader.StylesheetLoader;
import application.util.loader.ViewLoader;
import javafx.scene.layout.BorderPane;

import java.io.FileNotFoundException;

/**
 * Created by mhotan on 3/10/14.
 */
public class UnitInfoView extends BorderPane {

    /**
     * Creates a view that represents all the unit info for a property book.
     *
     * @param controller Controller class for this view.
     */
    public UnitInfoView(UnitInfoController controller) {
        ViewLoader.loadView("UnitInfo.fxml", this, controller);
        try {
            StylesheetLoader.load(this);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

}
