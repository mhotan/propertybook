package application.views.items;

import application.controllers.inspection.DeficienciesController;
import application.util.loader.StylesheetLoader;
import application.util.loader.ViewLoader;
import javafx.scene.layout.VBox;

import java.io.FileNotFoundException;

/**
 * Created by mhotan on 3/8/14.
 */
public class DeficienciesView extends VBox {

    public DeficienciesView(DeficienciesController controller) {
        ViewLoader.loadView("IssueTracker.fxml", this, controller);

        try {
            StylesheetLoader.load(this, "base.css", "issue-tracker.css");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

}
