package application.views.items;

import application.controllers.items.EndItemListItemController;
import application.util.loader.StylesheetLoader;
import application.util.loader.ViewLoader;
import javafx.scene.layout.HBox;

import java.io.FileNotFoundException;

/**
 * Created by mhotan on 3/21/14.
 */
public class EndItemListItemView extends HBox {

    public EndItemListItemView(EndItemListItemController controller) {
        ViewLoader.loadView("EndItemListCell.fxml", this, controller);

        try {
            StylesheetLoader.load(this, "base.css", "enditem-list-element.css");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

}
