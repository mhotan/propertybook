package application.views.items;

import application.controllers.items.EndItemDetailsController;
import application.util.loader.StylesheetLoader;
import application.util.loader.ViewLoader;
import javafx.scene.layout.VBox;

import java.io.FileNotFoundException;

/**
 * Created by mhotan on 3/8/14.
 */
public class EndItemDetailsView extends VBox {

    public EndItemDetailsView(EndItemDetailsController controller) {
        ViewLoader.loadView("EndItemDetails.fxml", this, controller);

        // load style sheets
        try {
            StylesheetLoader.load(this, "base.css", "enditem-detail.css");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

}
