package application.views.items;

import application.controllers.items.EndItemSearchController;
import application.util.loader.StylesheetLoader;
import application.util.loader.ViewLoader;
import javafx.scene.layout.VBox;

import java.io.FileNotFoundException;

/**
 * View of all the details of a particular end item.
 *
 * @author Michael Hotan, michael.hotan@gmail.com
 */
public class EndItemSearchView extends VBox {

    public EndItemSearchView(EndItemSearchController controller) {
        ViewLoader.loadView("EndItemSearch.fxml", this, controller);
        try {
            StylesheetLoader.load(this, "base.css", "enditem-detail.css");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
