package application.views.items;

import application.controllers.items.components.ComponentsController;
import application.util.loader.StylesheetLoader;
import application.util.loader.ViewLoader;
import javafx.scene.layout.VBox;

import java.io.FileNotFoundException;

/**
 * Created by mhotan on 3/8/14.
 */
public class ComponentsView extends VBox {

    public ComponentsView(ComponentsController controller) {
        ViewLoader.loadView("Components.fxml", this, controller);

        try {
            StylesheetLoader.load(this, "base.css", "components.css");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

}
