package application.views.intro;

import application.controllers.main.intro.IntroController;
import application.util.loader.ViewLoader;
import application.util.loader.StylesheetLoader;
import javafx.scene.layout.BorderPane;

import java.io.FileNotFoundException;


/**
 * Introduction view for the user.
 *
 * @author Michael Hotan, michael.hotan@gmail.com
 */
public class IntroView extends BorderPane {

    /**
     * View that shows the introduction
     *
     * @param controller Controller class to handle proceeding
     */
    public IntroView(IntroController controller) {
        ViewLoader.loadView("Intro.fxml", this, controller);

        try {
            StylesheetLoader.load(this, "base.css", "intro.css");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e.getMessage());
        }

    }

}
