package application.views.operator;

import application.controllers.operator.OperatorController;
import application.util.loader.StylesheetLoader;
import application.util.loader.ViewLoader;
import javafx.scene.layout.BorderPane;

import java.io.FileNotFoundException;

/**
 * Created by mhotan on 3/10/14.
 */
public class OperatorView extends BorderPane {

    public OperatorView(OperatorController controller) {
        ViewLoader.loadView("Operator.fxml", this, controller);

        try {
            StylesheetLoader.load(this, "operator.css");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

}
