package application.views;

import application.controllers.main.MainController;
import application.util.loader.ViewLoader;
import application.util.loader.StylesheetLoader;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;

import java.io.FileNotFoundException;

/**
 * Simple container view that has a menu bar and a place holder for dynamic itemdata.
 * The menu bar will be persistent across all views.
 *
 * @author Michael Hotan, michael.hotan@gmail.com
 */
public class MainView extends BorderPane {

    /**
     * Creates a single instance of the view binding the controller.
     *
     * @param controller Controller to bind to this.
     */
    public MainView(MainController controller) {
        // Use the wrapping view loader class for this.
        ViewLoader.loadView("Main.fxml", this, controller);

        try {
            StylesheetLoader.load(this);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * Sets the content of this view.
     *
     * @param view View to show in the main content.
     */
    public void setContent(Parent view) {
        setCenter(view);
    }

}
