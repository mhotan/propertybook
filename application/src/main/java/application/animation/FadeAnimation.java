package application.animation;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import javafx.util.Duration;


/**
 * Created by mhotan on 3/24/14.
 */
public class FadeAnimation {

    public static final int DEFAULT_FADE_TIME = 1000;

    private FadeAnimation() {}

//    /**
//     *
//     * @param view
//     * @param handler
//     */
//    public static void fadeOut(Node view, int fadeTime, EventHandler<ActionEvent> handler) {
//        fadeTime = Math.max(1, fadeTime);
//        if (view == null)
//            throw new NullPointerException("View cannot be null");
//        if (handler == null)
//            throw new NullPointerException("Event Handler cannot be null");
//
//        final DoubleProperty opacity = view.opacityProperty();
//        KeyFrame initialFrame = new KeyFrame(Duration.ZERO, new KeyValue(opacity, 1.0));
//        KeyFrame finalFrame = new KeyFrame(new Duration(fadeTime), handler, new KeyValue(opacity, 0.0));
//        Timeline fade = new Timeline(initialFrame, finalFrame);
//        fade.play();
//    }
//
//    public static void fadeOut(Node view, EventHandler<ActionEvent> handler) {
//        fadeOut(view, DEFAULT_FADE_TIME, handler);
//    }
//
//    /**
//     * Adds the view to container using a fade animation.  If the container contains the view
//     * no animation is done.
//     *
//     * @param container Container to add the view to.
//     * @param view The View to fade into the container.
//     * @param fadeTime The time it takes to fade it.
//     */
//    public static void fadeIn(Pane container, Node view, int fadeTime) {
//        fadeTime = Math.max(1, fadeTime);
//        if (view == null)
//            throw new NullPointerException("View cannot be null");
//        if (container == null)
//            throw new NullPointerException("Container cannot be null");
//
//        if (container.getChildren().contains(view)) return;
//
//        // Make the view clear
//        final DoubleProperty opacity = view.opacityProperty();
//        view.setOpacity(0.0);
//
//        // Add the view to the container
//        container.getChildren().add(0, view);
//
//        // Fade the view in.
//        KeyFrame initialFrame = new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0));
//        KeyFrame finalFrame = new KeyFrame(new Duration(fadeTime), new KeyValue(opacity, 1.0));
//        Timeline fade = new Timeline(initialFrame, finalFrame);
//        fade.play();
//    }
//
//    /**
//     * Adds the view to container using a fade animation.  If the container contains the view
//     * no animation is done.  Uses default time DEFAULT_FADE_TIME
//     *
//     * @param container Container to add the view to.
//     * @param view The View to fade into the container.
//     */
//    public static void fadeIn(Pane container, Node view) {
//        fadeIn(container, view, DEFAULT_FADE_TIME);
//    }

    public static void setView(BorderPane container, Node view) {
        // If the container already has the view and it is showing.
        if (container.getCenter() != null && container.getCenter().equals(view)) return;

        final DoubleProperty opacity = container.opacityProperty();

        // If the pane currently has a view then
        if (!container.getChildren().isEmpty()) {

            KeyFrame fadeOutInitFrame = new KeyFrame(Duration.ZERO, new KeyValue(opacity, 1.0));
            KeyFrame fadeOutFinalFrame = new KeyFrame(new Duration(DEFAULT_FADE_TIME), event -> {

                // Remove all the screens.
                container.setCenter(view);

                KeyFrame fadeInInitFrame = new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0));
                KeyFrame fadeInFinalFrame = new KeyFrame(Duration.ZERO, new KeyValue(opacity, 1.0));
                Timeline fadeIn = new Timeline(fadeInInitFrame, fadeInFinalFrame);
                fadeIn.play();
            }, new KeyValue(opacity, 0.0));

            Timeline fadeOut = new Timeline(fadeOutInitFrame, fadeOutFinalFrame);
            fadeOut.play();
        } else { // If container is empty then just simply fade in.
            container.setOpacity(0.0);
            container.setCenter(view);

            KeyFrame fadeInInitFrame = new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0));
            KeyFrame fadeInFinalFrame = new KeyFrame(Duration.ZERO, new KeyValue(opacity, 1.0));
            Timeline fadeIn = new Timeline(fadeInInitFrame, fadeInFinalFrame);
            fadeIn.play();
        }
    }
}
