package application;

import application.controllers.main.MainController;
import application.storage.FileAdapter;
import application.views.MainView;
import application.views.login.LogInCallback;
import com.hotan.army.supply.data.parse.PropertyBookParse;
import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import org.parse4j.Parse;

/**
 * Created by mhotan_dev on 1/12/14.
 */
public class Main extends Application implements LogInCallback {



    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        PropertyBookParse.registerClasses();
        Parse.initialize(Constants.PARSE_APPLICATION_ID, Constants.PARSE_REST_ID);

        // Fit the stage to the screen.
        fitToScreen(stage);

//        stage.setScene(new Scene(new TestView()));
        // Main Controller.
        MainController controller = new MainController();
        MainView mainView = new MainView(controller);

        // Set the stage using the main view.
        stage.setScene(new Scene(mainView));

        // Show the application.
        stage.show();
    }



    @Override
    public void onLoginSuccess(FileAdapter adapter) {

    }

    @Override
    public void onLoginFailed(String error) {

    }

    @Override
    public void onLoginCancelled() {

    }

    /**
     * Fit the stage to screen.
     *
     * @param stage Stage to fit to the screen.
     */
    private void fitToScreen(Stage stage) {
        // Set the size of the primary stage.
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // Set the window to be the size of the screen.
        stage.setX(bounds.getMinX());
        stage.setY(bounds.getMinY());
        stage.setWidth(bounds.getWidth());
        stage.setHeight(bounds.getHeight());
    }
}
