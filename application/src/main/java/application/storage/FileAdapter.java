package application.storage;

import com.hotan.army.supply.data.EndItemGroup;

import java.io.File;
import java.io.IOException;

/**
 * Created by mhotan_dev on 1/14/14.
 */
public interface FileAdapter {

    /**
     *
     *
     * @param group The group of items that
     * @param image Image to use.
     * @throws java.io.IOException
     */
    public void setLocationImage(EndItemGroup group, File image) throws IOException;

    /**
     *
     * @param group The group of end items.
     * @param image Image to use.
     * @throws IOException
     */
    public void setSerialNumberImage(EndItemGroup group, File image) throws IOException;

    public void setBIILayoutImage(EndItemGroup group, File image) throws IOException;

}
