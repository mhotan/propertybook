package application.util.listeners;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 * Change listener for making sure text meets a length and character requirement.
 *
 * Created by mhotan on 3/20/14.
 */
public class RestrictiveTextListener implements ChangeListener<String> {

    private IntegerProperty maxLength = new SimpleIntegerProperty(this, "maxLength", -1);
    private StringProperty restrict = new SimpleStringProperty(this, "restrict");

    private boolean ignore;

    private final StringProperty field;

    public RestrictiveTextListener(StringProperty field) {
        this.field = field;
    }

    public int getMaxLength() {
        return maxLength.get();
    }

    public IntegerProperty maxLengthProperty() {
        return maxLength;
    }

    public void setMaxLength(int maxLength) {
        this.maxLength.set(maxLength);
    }

    public String getRestrict() {
        return restrict.get();
    }

    public StringProperty restrictProperty() {
        return restrict;
    }

    public void setRestrict(String restrict) {
        this.restrict.set(restrict);
    }

    @Override
    public void changed(ObservableValue<? extends String> observableValue, String s, String s1) {
        if (ignore || s1 == null) return;
        if (maxLength.get() > -1 && s1.length() > maxLength.get()) {
            ignore = true;
            field.set(s1.substring(0, maxLength.get()));
            ignore = false;
        }

        if (restrict.get() != null && !restrict.get().equals("") && !s1.matches(restrict.get() + "*")) {
            ignore = true;
            field.set(s);
            ignore = false;
        }
    }

}
