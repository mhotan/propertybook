package application.util.listeners;

import javafx.beans.property.StringProperty;

/**
 * Created by mhotan on 3/20/14.
 */
public class NumberTextChangeListener extends RestrictiveTextListener {

    private static final String NUMBER_REGEX = "[0-9]";

    public NumberTextChangeListener(StringProperty field, int maxLength) {
        super(field);
        super.setRestrict(NUMBER_REGEX);
        super.setMaxLength(Math.max(-1, maxLength));
    }
}
