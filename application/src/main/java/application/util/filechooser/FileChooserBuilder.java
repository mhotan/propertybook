package application.util.filechooser;

import javafx.stage.FileChooser;

import java.io.File;

/**
 * Created by mhotan on 3/20/14.
 */
public class FileChooserBuilder {

    private FileChooserBuilder() {}

    public static FileChooser getImageFileChooser(String title) {
        if (title == null || title.isEmpty())
            title = "Choose an Image";
        FileChooser chooser = new FileChooser();
        chooser.setTitle(title);
        chooser.setInitialDirectory(new File(System.getProperty("user.home")));
        chooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.*"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png"));
        return chooser;
    }

    public static FileChooser getXLSFileChooser(String title) {
        if (title == null || title.isEmpty())
            title = "Choose an XLS Document";
        FileChooser chooser = new FileChooser();
        chooser.setTitle(title);
        chooser.setInitialDirectory(new File(System.getProperty("user.home")));
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("XLS", "*.xls"));
        return chooser;
    }

}
