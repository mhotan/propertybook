package application.util.cells;

import com.hotan.army.supply.data.personnel.MOS;
import com.hotan.army.supply.data.personnel.Operator;
import com.hotan.army.supply.data.personnel.Rank;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

/**
 * Created by mhotan on 3/21/14.
 */
public class OperatorListCellFactory implements Callback<ListView<Operator>, ListCell<Operator>> {

    @Override
    public ListCell<Operator> call(ListView<Operator> operatorListView) {
        return new OperatorListCell();
    }

    private static class OperatorListCell extends ListCell<Operator> {

        @Override
        protected void updateItem(Operator operator, boolean empty) {
            super.updateItem(operator, empty);
            if (operator != null) {
                ChangeListener<String> nameChange = new ChangeListener<String>() {
                    @Override
                    public void changed(ObservableValue<? extends String> observableValue, String s, String s2) {
                        updateText(operator);
                    }
                };
                operator.firstNameProperty().addListener(nameChange);
                operator.lastNameProperty().addListener(nameChange);
                operator.rankProperty().addListener(new ChangeListener<Rank>() {
                    @Override
                    public void changed(ObservableValue<? extends Rank> observableValue, Rank rank, Rank rank2) {
                        updateText(operator);
                    }
                });
                operator.MOSProperty().addListener(new ChangeListener<MOS>() {
                    @Override
                    public void changed(ObservableValue<? extends MOS> observableValue, MOS mos, MOS mos2) {
                        updateText(operator);
                    }
                });
                updateText(operator);
            } else {
                setText(null);
            }
        }

        private void updateText(Operator operator) {
            setText(processOperator(operator));
        }
    }

    public static String processOperator(Operator operator) {
        if (operator == null) return "";
        String rank = operator.getRank() != null ? operator.getRank().toString(): "";
        String mos = operator.getMOS() != null ? "(" + operator.getMOS().toString() + ")": "";
        return (rank + " " + operator.getFirstName() + " " + operator.getLastName() + " " + mos).trim();
    }
}
