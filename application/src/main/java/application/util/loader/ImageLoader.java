package application.util.loader;

import javafx.scene.image.Image;

/**
 * Class used to load static images in resource folder.
 *
 * Created by mhotan on 3/8/14.
 */
public class ImageLoader {

    private ImageLoader() {}

    public static Image load(String name) {
        return new Image(ImageLoader.class.getClassLoader().
                getResourceAsStream("images/" + name));
    }

    public static Image getNoImageAvailableImage() {
        if (NO_IMAGE_AVAILABLE == null)
            NO_IMAGE_AVAILABLE = load("no-image.png");
        return NO_IMAGE_AVAILABLE;
    }

    private static Image NO_IMAGE_AVAILABLE;

}
