package application.util.loader;

import javafx.scene.Parent;

import java.io.FileNotFoundException;
import java.net.URL;
import java.util.logging.Logger;

/**
 * Created by mhotan on 3/6/14.
 */
public class StylesheetLoader {

    private static final Logger LOG = Logger.getLogger(StylesheetLoader.class.getSimpleName());

    /**
     * Cannot instantiate.
     */
    private StylesheetLoader() {}

    private static final String[] BASE_CSS_FILES = {"base.css"};

    /**
     * Loads all the style sheets for this view.
     *
     * @param view View to load style sheets on.
     * @param styleSheetNames Names of style sheets to load
     * @throws FileNotFoundException
     */
    public static void load(Parent view, String... styleSheetNames) throws FileNotFoundException {
        for (String baseStyleSheet: BASE_CSS_FILES) {
            load(view, baseStyleSheet);
        }
        for (String styleSheetName: styleSheetNames) {
            load(view, styleSheetName);
        }
    }

    /**
     * Loads a style sheet on this view.
     *
     * @param view View to load stylesheet in.
     * @param styleSheetName Name of stylesheet to load.
     * @throws java.io.FileNotFoundException Unable to find stylesheet.
     */
    private static void load(Parent view, String styleSheetName) throws FileNotFoundException {
        URL url = StylesheetLoader.class.getClassLoader().getResource("stylesheets/" + styleSheetName);
        if (url == null)
            throw new FileNotFoundException("Unable to find stylesheet " + styleSheetName);
        String urlExtForm = url.toExternalForm();
        if (view.getStylesheets().contains(urlExtForm)) return;
        view.getStylesheets().add(urlExtForm);
    }

}
