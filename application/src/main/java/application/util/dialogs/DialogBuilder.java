package application.util.dialogs;

import javafx.beans.property.*;
import javafx.scene.Node;
import org.controlsfx.dialog.Dialogs;

/**
 * Created by mhotan on 3/19/14.
 */
public class DialogBuilder {

    private final BooleanProperty lightWeight, nativeTitleBar, showMasthead;

    private final ObjectProperty<Node> owner;

    private final StringProperty title, masthead, message;

    public DialogBuilder() {
        this.lightWeight = new SimpleBooleanProperty(false);
        this.nativeTitleBar = new SimpleBooleanProperty(false);
        this.owner = new SimpleObjectProperty(null);
        this.title = new SimpleStringProperty("");
        this.masthead = new SimpleStringProperty("");
        this.message = new SimpleStringProperty("");
        this.showMasthead = new SimpleBooleanProperty();
        this.showMasthead.bind(masthead.isEmpty().not());
    }

    public Dialogs build() {
        Dialogs d =Dialogs.create().
                title(getTitle()).
                message(getMessage()).
                masthead(getShowMasthead() ? getMasthead() : null).
                owner(getOwner());
        if (getLightWeight())
            d.lightweight();
        if (getNativeTitleBar())
            d.nativeTitleBar();
        return d;
    }

    private String getMasthead() {
        return masthead.get();
    }

    public StringProperty mastheadProperty() {
        return masthead;
    }

    public void setMasthead(String masthead) {
        this.masthead.set(processString(masthead));
    }

    public boolean getShowMasthead() {
        return showMasthead.get();
    }

    public BooleanProperty showMastheadProperty() {
        return showMasthead;
    }

    public boolean getLightWeight() {
        return lightWeight.get();
    }

    public BooleanProperty lightWeightProperty() {
        return lightWeight;
    }

    public void setLightWeight(boolean lightWeight) {
        this.lightWeight.set(lightWeight);
    }

    public boolean getNativeTitleBar() {
        return nativeTitleBar.get();
    }

    public BooleanProperty nativeTitleBarProperty() {
        return nativeTitleBar;
    }

    public void setNativeTitleBar(boolean nativeTitleBar) {
        this.nativeTitleBar.set(nativeTitleBar);
    }

    public Node getOwner() {
        return owner.get();
    }

    public ObjectProperty<Node> ownerProperty() {
        return owner;
    }

    public void setOwner(Node owner) {
        this.owner.set(owner);
    }

    public String getTitle() {
        return title.get();
    }

    public StringProperty titleProperty() {
        return title;
    }

    public void setTitle(String title) {
        this.title.set(processString(title));
    }

    public String getMessage() {
        return message.get();
    }

    public StringProperty messageProperty() {
        return message;
    }

    public void setMessage(String message) {
        this.message.set(processString(message));
    }

    private static String processString(String message) {
        if (message == null) return "";
        return message.trim();
    }
}
