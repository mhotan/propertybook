package application.custom.controls;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.TextField;

import java.text.NumberFormat;
import java.text.ParseException;

/**
 * Textfield implementation that accepts formatted number and stores them in a
 * BigDecimal property The user input is formatted when the focus is lost or the
 * user hits RETURN.
 *
 * @author Thomas Bolz
 */
public abstract class NumberTextField<T extends Number> extends TextField {

    private final NumberFormat nf;
    private ObjectProperty<T> number = new SimpleObjectProperty<>();

    public final T getNumber() {
        return number.get();
    }

    public final void setNumber(T value) {
        number.set(value);
    }

    public ObjectProperty<T> numberProperty() {
        return number;
    }

//    public NumberTextField() {
//        this(Numbe);
//    }

    public NumberTextField(T value) {
        this(value, NumberFormat.getInstance());
        initHandlers();
    }

    public NumberTextField(T value, NumberFormat nf) {
        super();
        this.nf = nf;
        initHandlers();
        setNumber(value);
    }

    private void initHandlers() {

        // try to parse when focus is lost or RETURN is hit
        setOnAction(event -> parseAndFormatInput());

        // Watch for the loss of focus then set the value.
        focusedProperty().addListener((ov, oldVal, newVal) -> {
            if (!newVal.booleanValue())
                parseAndFormatInput();
        });

        // Set text in field if BigDecimal property is changed from outside.
        numberProperty().addListener((ov, oldVal, newVal) -> setText(nf.format(newVal)));
    }

    /**
     * Tries to parse the user input to a number according to the provided
     * NumberFormat
     */
    private void parseAndFormatInput() {
        try {
            String input = getText();
            if (input == null || input.length() == 0) {
                return;
            }
            Number parsedNumber = nf.parse(input);
            setNumber(fromString(parsedNumber.toString()));
            selectAll();
        } catch (ParseException ex) {
            // If parsing fails keep old number
            setText(nf.format(number.get()));
        }
    }

    /**
     * Creates a Number from a specific string.
     *
     * @param parsedNumber String representation of the number
     * @return The number value from the parsed string.
     * @throws java.text.ParseException unable to create a number from parsed string.
     */
    protected abstract T fromString(String parsedNumber) throws ParseException;
}
