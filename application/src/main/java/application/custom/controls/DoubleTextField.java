package application.custom.controls;

import java.text.ParseException;

/**
 * Created by mhotan on 3/24/14.
 */
public class DoubleTextField extends NumberTextField<Double> {

    public DoubleTextField() {
        super(0.0);
    }

    @Override
    protected Double fromString(String parsedNumber) throws ParseException {
        return new Double(parsedNumber);
    }
}
